[agile_hr.gif]: http://luomingui.gitee.io/agilehr/wwwroot/agile_hr.gif "agile hr"
![agile_hr.gif]
#### 介绍
Agile HR人力资源系统该系统提供了对 人员管理、人员调动、薪酬管理、代表事项、流程中心、基础设施、学校信息、系统配置、系统结构、图标管理 、健康检查、统一身份认证平台、消息提醒等模块。
![输入图片说明](http://luomingui.gitee.io/agilehr/wwwroot/hr/login.png "在这里输入图片标题")
![输入图片说明](http://luomingui.gitee.io/agilehr/wwwroot/hr/index.png "在这里输入图片标题")

#### 已有模块

01.  人员管理
02.  人员调动
03.  薪酬管理
04.  学校信息
05.  系统配置
06.  代表事项
07.  流程中心
08.  基础设施
09.  系统结构
10.  图标管理 
11.  健康检查
12.  统一身份认证平台
13.  消息提醒

#### 待办事项

01.  考勤管理
02.  绩效考核
03.  报表中心
04.  自定义表单
05.  定时任务、导入导出
06.  对接企业微信
07.  公共数据交换平台
08.  统一服务门户管理平台
09.  自定义查询条件 自动补齐插件

#### 技术栈

01.  前端 AdminTLE+ bootstrap + charts + ztree + gooflow + framework-ui
02.  后端 .net core3.1 + SignalR + Dapper + WeiXin + IdentityServer
03.  ico http://fontawesome.io https://www.thinkcmf.com/font/font_awesome/icons.html
04.  grid http://www.guriddo.net/demo/guriddojs/ https://blog.mn886.net/jqGrid/

#### 开发规范

1. 实体类文件名规范： 类名
2. 服务类文件名规范： 类名+Service
3. 请求类文件名规范： 类名+Req
4. 响应类文件名规范： 类名+Resq
5. 安装类文件名规范： 类名+Startup
6. 模块项目文件规范： Agile.Service+英文模块名

#### 技术要求

1. 模块化、组件式开发模式；
2. 系统应具有良好的扩展性与二次开发能力；
3. 内置标准的工作流引擎和常用的工作流程；
4. 有统一的权限控制机制；
5. 与其他应用统一认证、统一授权（SS0）；
6. 支持云计算平台。



#### 项目配置props
```
<TreatWarningsAsErrors>true</TreatWarningsAsErrors>
<NoWarn>$(NoWarn);CS1591;CS1572;CS1573;CS1570;CS0618;CA2100;AD0001;</NoWarn>
<GenerateDocumentationFile>true</GenerateDocumentationFile>
<GeneratePackageOnBuild>true</GeneratePackageOnBuild>
<RunAnalyzersDuringLiveAnalysis>false</RunAnalyzersDuringLiveAnalysis>
<GeneratePackageOnBuild>false</GeneratePackageOnBuild>

 warning RAZORSDK1007: Reference assembly Agile.Data.dll 
could not be found. This is typically caused by build errors in referenced projects.
```



帮助文档: https://gitee.com/luomingui/AgileHR/wikis
BUG提交: https://gitee.com/luomingui/AgileHR/issues 
问答社区: https://gitee.com/luomingui/AgileHR/discussions 











#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
