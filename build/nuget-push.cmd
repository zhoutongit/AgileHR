@echo off
echo ================[如果出现找不到路径，请检查批处理的环境变量]================
setlocal enabledelayedexpansion
cd ..

set ApiKey=luomingui
set SourceUrl= http://120.203.30.149:7765/
set "NupkgPath=%cd%\_packages\*.nupkg"
set "NupgetPhan=%cd%\Build\nuget.exe"
echo ================%cd%
echo ================%SourceUrl%
echo ================%NupkgPath%
echo ================%NupgetPhan%
::上传包
%NupgetPhan% push %NupkgPath% -Source %SourceUrl% -ApiKey %ApiKey%

echo 按任意键退出
pause

