﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    Run();
                    Console.ResetColor();
                }
            }
            catch (Exception ex)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.BackgroundColor = ConsoleColor.White;
                Console.ResetColor();
                Run();
            }
        }

        public static void Run()
        {
            Console.Title = "Agile 控制台程序 ";
            Console.ForegroundColor = ConsoleColor.Gray;
         
            Console.WriteLine("从下列列表中选择一个选项:");
            Dictionary<int,object> pairs = new Dictionary<int,object>();
            var types = GetAllChildClass(typeof(IStrategy));
            var instances = types.Select(startupTask => (IStrategy)Activator.CreateInstance(startupTask))
               .OrderBy(startupTask => startupTask.Sid);
            foreach(var strategy in instances) {
                Console.WriteLine("    " + strategy.Sid + "：" + strategy.Name);
                pairs.Add(strategy.Sid,strategy);
            }
            Console.Write("Your option? ");
            Stopwatch _sw = new Stopwatch();
            string input = Console.ReadLine();
            if(!string.IsNullOrWhiteSpace(input)) {
                _sw.Start();
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine(" ");
                int key = Utils.GetObjTranNull<int>(input);
                if(pairs.ContainsKey(key)) {
                    IStrategy strategy = (IStrategy)pairs[key];
                    strategy.Run(null);
                }
            }
            _sw.Stop();
            TimeSpan ts2 = _sw.Elapsed;
            int seconds = Utils.GetObjTranNull<int>(ts2.TotalSeconds % 60);
            int minutes = Utils.GetObjTranNull<int>((ts2.TotalSeconds / 60) % 60);
            int hours = Utils.GetObjTranNull<int>(ts2.TotalSeconds / 3600);
            Console.WriteLine("\n总共花费=" + string.Format("{0}:{1}:{2}",hours,minutes,seconds) + "\n");
            Console.WriteLine(" ");
        }

        public static Type[] GetAllChildClass(Type baseType) {
            var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(a => a.GetTypes().Where(t => !t.IsAbstract && t.GetInterfaces().Contains(baseType)))
            .ToArray();
            return types;
        }
    }
}
