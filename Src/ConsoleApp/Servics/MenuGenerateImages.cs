﻿using System;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Agile.Service;
using Agile.Service.Rbac;

namespace ConsoleApp {
    /// <summary>
    /// 根据菜单生成图片
    /// </summary>
    public class MenuGenerateImages:Strategy {
        public override int Sid { get; set; } = 5;
        public override string Name { get; set; } = "根据菜单生成图片";
        private static readonly IMenuElementService menuElementDal = new MenuElementService();
        private static readonly IMenuService menuDal = new MenuService(new MenuElementService());
        private static string baseUrl = "http://localhost:8085";//先登录
        public override void RunCode(object data) {
            var menus = menuDal.FindAll("Name NOT IN('系统结构','健康检查','图标管理') AND ParentID<>'00000000-0000-0000-0000-000000000000'");
            int tablecount = menus.Count();
            int index = 1;
            foreach(var menu in menus) {

                string menuid = menu.ID.ToString();
                string menuname = menu.Name;
                string menuurlmvc = menu.Url;

                string fileName = BaseDirectory + "/Temp/" + menuname + ".jpg";
                string url = baseUrl + menuurlmvc;

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("****" + index + "/" + tablecount + "***" + menuname + "******" + url + "*********");
                index++;
                DateTime current = DateTime.Now.AddMinutes(1);
                while(current > DateTime.Now) {
                    HtmlOrImages orImages = new HtmlOrImages(url,fileName,menuname);
                    orImages.GetImg();
                    index++;
                    break;
                }
            }
        }
    }
}
