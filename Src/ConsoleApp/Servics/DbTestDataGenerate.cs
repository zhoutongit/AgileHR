﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ConsoleApp {
    /// <summary>
    /// 根据数据库表生成测试数据
    /// </summary>
    public class DbTestDataGenerate:Strategy {
        public override int Sid { get; set; } = 2;
        public override string Name { get; set; } = "根据数据库表生成测试数据";
        public override void RunCode(object data) {
            Console.WriteLine("开始生成表测试数据");
            Console.WriteLine("=====================================================");
            var dba = DbFactory.DbHelper();
            var tables = dba.GetTables();//.Where(p => p.TableName.ToLower() == "base_area");
            int i = 0;
            foreach(var item in tables) {
                try {
                    int record = dba.ExecuteScalar<int>("select count(*) from " + item.TableName + " ");
                    if(record == 0) {
                        if(dba.TestDataGenerate(item.TableName)) {
                            i++;
                            if(i % 2 == 0) {
                                Console.ForegroundColor = ConsoleColor.White;
                            } else {
                                Console.ForegroundColor = ConsoleColor.Blue;
                            }
                            Console.WriteLine(item.TableName + "(" + item.TableNameRemark + ")表测试数据生成完成");
                        }
                    }
                } catch(Exception ex) {
                    Logger.LogError(item.TableName,ex);
                    continue;
                }
            }
            Console.WriteLine("=====================================================");
            Console.WriteLine("表测试数据生成完成");
        }
    }
}
