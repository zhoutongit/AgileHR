﻿using System;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Agile.Service;
using Agile.Service.Rbac;

namespace ConsoleApp {
    /// <summary>
    /// 导出菜单数据
    /// </summary>
    public class ExportMenuData:Strategy {
        public override int Sid { get; set; } = 4;
        public override string Name { get; set; } = "导出菜单数据";
        private static readonly IMenuElementService menuElementDal = new MenuElementService();
        private static readonly IMenuService menuDal = new MenuService(new MenuElementService());
        public override void RunCode(object data) {
            
            var menus = menuDal.FindAll("Name NOT IN('系统结构','健康检查','图标管理') AND ParentID<>'00000000-0000-0000-0000-000000000000'");
            foreach(var menu in menus) {

            }
        }
    }
}
