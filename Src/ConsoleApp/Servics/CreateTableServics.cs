﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ConsoleApp {
    /// <summary>
    /// 根据实体类生成数据库表
    /// </summary>
    public class CreateTableServics:Strategy {
        public override int Sid { get; set; } = 1;
        public override string Name { get; set; } = "根据实体类生成数据库表";
        public override void RunCode(object data) {
            ArrayList arrayList = new ArrayList();
            arrayList.Add("Agile.Service.dll");
            arrayList.Add("Agile.Service.Flow.dll");
            arrayList.Add("Agile.Service.Personnel.dll");
            arrayList.Add("Agile.Service.Rbac.dll");
            arrayList.Add("Agile.Service.Reward.dll");
            arrayList.Add("Agile.Service.School.dll");
            arrayList.Add("Agile.Service.Survey.dll");
            arrayList.Add("Agile.Service.Assess.dll");
            ArrayList arrayListNo = new ArrayList();
            arrayListNo.Add("FlowVerificationResp");
            arrayListNo.Add("FrmLeaveReq");
            arrayListNo.Add("ApplicationReq");
            arrayListNo.Add("RoleReq");
            arrayListNo.Add("  ");
            arrayListNo.Add("  ");
            arrayListNo.Add("  ");
            
            var root = Directory.GetCurrentDirectory();

            try {
                var dba = DbFactory.DbHelper("Data Source=127.0.0.1;Database=master;User ID=sa;Password=123456;MultipleActiveResultSets=true;Connect Timeout=30;Min Pool Size=16;Max Pool Size=100;");
                dba.ExecuteNonQuery("if exists (select * from sys.databases where name = 'Agile8')drop database [Agile8];CREATE DATABASE [Agile8];");

                var dp = DbFactory.DbHelper();
                for(int i = 0;i < arrayList.Count;i++) {
                    if(i % 2 == 0) {
                        Console.ForegroundColor = ConsoleColor.White;
                    } else {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    Console.WriteLine("正在生成" + arrayList[i].ToString() + "数据表");
                    string path = root + "/" + arrayList[i].ToString();
                    Assembly assembly = Assembly.LoadFile(path);
                    var types = EntityHelper.GetEntityTypes(assembly);
                    foreach(Type type in types) {
                        if(!arrayListNo.Contains(type.Name)) {
                            dp.CreateTable(type);
                            Console.WriteLine("生成" + type.Name + "数据库表...");
                        }
                    }
                }
            } catch(Exception ex) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("生成数据库表错误：" + ex.Message);
            }

            try {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("..............生成测试数据开始..............");
                //生成测试数据
                var types = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(a => a.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(ITestData))))
                        .ToArray();

                var instances = types.Select(startupTask => (ITestData)Activator.CreateInstance(startupTask))
               .OrderBy(startupTask => startupTask.DisplayOrder);
                int i = 0;
                foreach(var task in instances) {
                    task.Run();
                    i++;
                    if(i % 2 == 0) {
                        Console.ForegroundColor = ConsoleColor.White;
                    } else {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    Console.WriteLine("正在生成" + task.ToString() + "测试数据..............");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("..............生成测试数据结束..............");
            } catch(Exception ex) {
                Logger.LogError("生成数据库测试数据错误：",ex);
                Console.WriteLine("生成数据库测试数据错误：" + ex.ToString());
            }
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }

    }
}
