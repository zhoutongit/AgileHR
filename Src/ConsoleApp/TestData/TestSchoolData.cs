﻿using System;
using System.Collections.Generic;
using System.Text;

using Agile.Service.School;

namespace ConsoleApp
{
    public class TestSchoolData : ITestData
    {
        public int DisplayOrder => 3;
        public void Run()
        {
            ISchoolmajorService schoolmajorService = new SchoolmajorService();
            ISchoolacadyeartermService schoolacadyeartermService = new SchoolacadyeartermService();
            ISchoolclassesService schoolclassesService = new SchoolclassesService();
            ISchoolClassRoomService schoolClassRoomService = new SchoolClassRoomService();
            ISchoolfunctionareaService schoolfunctionareaService = new SchoolfunctionareaService();
            ISchoolschoolareaService schoolschoolareaService = new SchoolschoolareaService();
            ISchoolteachingbuildingService schoolteachingbuildingService = new SchoolteachingbuildingService();

            var 瑶湖校区 = schoolschoolareaService.AddOrUpdate(new SchoolArea { Name = "瑶湖校区", Address = "" });
            var 青山湖校区 = schoolschoolareaService.AddOrUpdate(new SchoolArea { Name = "青山湖校区", Address = "" });

            //教学楼
            var 教学楼 = schoolteachingbuildingService.AddOrUpdate(new TeachingBuilding { SchoolAreaId = 瑶湖校区.ID, Name = "教学1楼" });
            schoolteachingbuildingService.AddOrUpdate(new TeachingBuilding { SchoolAreaId = 瑶湖校区.ID, Name = "教学2楼" });
            schoolteachingbuildingService.AddOrUpdate(new TeachingBuilding { SchoolAreaId = 瑶湖校区.ID, Name = "宿舍1楼" });
            schoolteachingbuildingService.AddOrUpdate(new TeachingBuilding { SchoolAreaId = 瑶湖校区.ID, Name = "宿舍2楼" });
            schoolteachingbuildingService.AddOrUpdate(new TeachingBuilding { SchoolAreaId = 青山湖校区.ID, Name = "宿舍2楼" });
            //教学功能
            var 普通教室 = schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "普通教室" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "计算机房" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "投影仪教师" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "办公室" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "杂物室" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "休息室" });
            schoolfunctionareaService.AddOrUpdate(new FunctionArea { SchoolAreaId = 瑶湖校区.ID, Name = "休息室" });

            //教师信息
            schoolClassRoomService.AddOrUpdate(new ClassRoom
            {
                SchoolAreaId = 瑶湖校区.ID,
                TeachingBuildingId = 教学楼.ID,
                FunctionAreaId = 普通教室.ID,
                Name = "101室",
                Seat = 60,
                TestSeat = 60,
                GotoClassSeat = 60
            });
            schoolClassRoomService.AddOrUpdate(new ClassRoom
            {
                SchoolAreaId = 瑶湖校区.ID,
                TeachingBuildingId = 教学楼.ID,
                FunctionAreaId = 普通教室.ID,
                Name = "102室",
                Seat = 60,
                TestSeat = 60,
                GotoClassSeat = 60
            });
            schoolClassRoomService.AddOrUpdate(new ClassRoom
            {
                SchoolAreaId = 瑶湖校区.ID,
                TeachingBuildingId = 教学楼.ID,
                FunctionAreaId = 普通教室.ID,
                Name = "103室",
                Seat = 60,
                TestSeat = 60,
                GotoClassSeat = 60
            });

            //学年学期
            schoolacadyeartermService.AddOrUpdate(new AcadYearTerm { Name = "2020上半年", ShortName = "2020", Mark = true, StartWeek = 1, EndWeek = 16 });
            schoolacadyeartermService.AddOrUpdate(new AcadYearTerm { Name = "2020下半年", ShortName = "2020", Mark = true, StartWeek = 1, EndWeek = 16 });
            //专业信息
            var 专业信息 = schoolmajorService.AddOrUpdate(new Major { Name = "学前教育", NameShort = "学前教育" });
            schoolmajorService.AddOrUpdate(new Major { Name = "广告设计", NameShort = "广告设计" });
            schoolmajorService.AddOrUpdate(new Major { Name = "建筑设计", NameShort = "建筑设计" });
            schoolmajorService.AddOrUpdate(new Major { Name = "服装设计", NameShort = "服装设计" });
            //班级信息
            schoolclassesService.AddOrUpdate(new Classes
            {
                SchoolAreaId = 瑶湖校区.ID,
                TeachingBuilding = 教学楼.ID,
                ClassRoom = 普通教室.ID,
                Major = 专业信息.ID,
                Name = "20级学前教育3班",
                Colleges = "学前教育分院",
                EnrollmentYear = "2020",
                NumberPeople = 112,
                CurrNumberPeople = 112,
                CurrNumberBoyPeople = 51,
                CurrNumberGirlPeople = 61,


            });



        }
    }
}
