﻿using System;
using System.Collections.Generic;
using System.Text;

using Agile.Service.Rbac;
using Agile.Service.Flow;

namespace ConsoleApp
{
    public class TestFlowData : ITestData
    {
        public readonly IDivModelfieldService divModelfieldService = new DivModelfieldService();
        public readonly IDivModelService divModelService = new DivModelService(new DivModelfieldService(), new MenuService(new MenuElementService()));
        

        public int DisplayOrder => 4;
        public void Run()
        {
            var 人事管理 = divModelService.AddOrUpdate(new DivModel { ParentID = Guid.Empty.ToString(), Name = "人事管理" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 人事管理.ID, Name = "转正申请" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 人事管理.ID, Name = "入职申请" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 人事管理.ID, Name = "离职申请" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 人事管理.ID, Name = "调岗申请" });
            var 合同管理 = divModelService.AddOrUpdate(new DivModel { ParentID = Guid.Empty.ToString(), Name = "合同管理" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 合同管理.ID, Name = "人事代理" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 合同管理.ID, Name = "在编合同" });
            var 考勤管理 = divModelService.AddOrUpdate(new DivModel { ParentID = Guid.Empty.ToString(), Name = "考勤管理" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 考勤管理.ID, Name = "加班申请" });
            divModelService.AddOrUpdate(new DivModel { ParentID = 考勤管理.ID, Name = "出差外出申请" });
            var 请假申请 = divModelService.AddOrUpdate(new DivModel { ParentID = 考勤管理.ID, Name = "请假申请" });

            divModelfieldService.AddOrUpdate(new DivModelfield
            {
                DivModelID = 请假申请.ID,
                Name = "标题",
                Field = "title",
                FieldType = "text",
                DefaultValue = ""
            });
            divModelfieldService.AddOrUpdate(new DivModelfield
            {
                DivModelID = 请假申请.ID,
                Name = "开始时间",
                Field = "begintime",
                FieldType = "datetime",
                DefaultValue = ""
            });
            divModelfieldService.AddOrUpdate(new DivModelfield
            {
                DivModelID = 请假申请.ID,
                Name = "结束时间",
                Field = "endtime",
                FieldType = "datetime",
                DefaultValue = ""
            });
            divModelfieldService.AddOrUpdate(new DivModelfield
            {
                DivModelID = 请假申请.ID,
                Name = "备注",
                Field = "remarks",
                FieldType = "textarea",
                DefaultValue = ""
            });
            divModelfieldService.AddOrUpdate(new DivModelfield
            {
                DivModelID = 请假申请.ID,
                Name = "申请人",
                Field = "UserID",
                FieldType = "text",
                DefaultValue = ""
            });
        }
    }
}
