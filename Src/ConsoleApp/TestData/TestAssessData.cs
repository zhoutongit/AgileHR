﻿using System;
using System.Collections.Generic;
using System.Text;
using Agile.Service;
using Agile.Service.Assess;
using Agile.Service.Rbac;
using Agile.Service.School;

namespace ConsoleApp {
    public class TestAssessData:ITestData {
        public int DisplayOrder => 7;
        public readonly IPerfProgramCategoryService perfProgramCategoryService = new PerfProgramCategoryService();
        public readonly IPerfProgramService perfProgramService = new PerfProgramService();
        public readonly IPerfKPITypeService perfKPITypeService = new PerfKPITypeService();
        public readonly IPerfKPIsService perfKPIsService = new PerfKPIsService();
        public readonly IPerfScoreModelService perfScoreModelService = new PerfScoreModelService();
        public readonly IUserService userService = new UserService(new RelevanceService());
        public readonly IPerfObjectService perfObjectService = new PerfObjectService();

        public void Run() {

            var root = perfProgramCategoryService.AddOrUpdate(new PerfProgramCategory {
                ParentID = Guid.Empty.ToString(),
                Name = DateTime.Now.Year.ToString() + "年方案",
                Remarks = "",
                OrderBy = 0,
                AddTime = DateTime.Now
            });
            var 员工考核 = perfProgramCategoryService.AddOrUpdate(new PerfProgramCategory {
                ParentID = root.ID,
                Name = "员工考核",
                Remarks = "",
                OrderBy = 1,
                AddTime = DateTime.Now
            });
            var 部门考核 = perfProgramCategoryService.AddOrUpdate(new PerfProgramCategory {
                ParentID = root.ID,
                Name = "部门考核",
                Remarks = "",
                OrderBy = 2,
                AddTime = DateTime.Now
            });

            var 考核方案 = perfProgramService.AddOrUpdate(new PerfProgram {
                PerfProgramCategoryID = 部门考核.ID,
                PrmName = DateTime.Now.Year.ToString() + "第二季度考核方案",
                PrmYm = DateTime.Now.ToString("yyyy-MM"),
                ObjectType = "rbac_user",
                MinScore = 0,
                MaxScore = 100,
                Status = 0,
                Remarks = DateTime.Now.Year.ToString() + "第二季度考核方案",
            });
            var 团队建设 = perfKPITypeService.AddOrUpdate(new PerfKPIType {
                PerfProgramID = 考核方案.ID,
                Name = "团队建设",
                Weighting = 30.00,
                Remarks = "",
                ParentID = Guid.Empty.ToString(),
                OrderBy = 1,
                AddTime = DateTime.Now
            });
            var 工作业绩 = perfKPITypeService.AddOrUpdate(new PerfKPIType {
                PerfProgramID = 考核方案.ID,
                Name = "工作业绩",
                Weighting = 30.00,
                Remarks = "",
                ParentID = Guid.Empty.ToString(),
                OrderBy = 1,
                AddTime = DateTime.Now
            });
            var 工作态度 = perfKPITypeService.AddOrUpdate(new PerfKPIType {
                PerfProgramID = 考核方案.ID,
                Name = "工作态度",
                Weighting = 30.00,
                Remarks = "",
                ParentID = Guid.Empty.ToString(),
                OrderBy = 1,
                AddTime = DateTime.Now
            });
            var 工作能力 = perfKPITypeService.AddOrUpdate(new PerfKPIType {
                PerfProgramID = 考核方案.ID,
                Name = "工作能力",
                Weighting = 30.00,
                Remarks = "",
                ParentID = Guid.Empty.ToString(),
                OrderBy = 1,
                AddTime = DateTime.Now
            });
            var 管理提升 = perfKPITypeService.AddOrUpdate(new PerfKPIType {
                PerfProgramID = 考核方案.ID,
                Name = "管理提升",
                Weighting = 30.00,
                Remarks = "",
                ParentID = Guid.Empty.ToString(),
                OrderBy = 1,
                AddTime = DateTime.Now
            });

            var 分档打分 = perfScoreModelService.AddOrUpdate(new PerfScoreModel {
                MinScore = 0,
                MaxScore = 100,
                IsChoose = 0,
                SmName = "分档打分",
                ChooseSource = ""
            });
            var 选择评分 = perfScoreModelService.AddOrUpdate(new PerfScoreModel {
                MinScore = 0,
                MaxScore = 100,
                IsChoose = 1,
                SmName = "选择评分",
                ChooseSource = "{100:优秀,80:良好,60:一般}"
            });
            perfKPIsService.AddOrUpdate(new PerfKPIs {
                PerfProgramID = 考核方案.ID,
                PerfKPITypeID = 团队建设.ID,
                KpiName = "增强团队凝聚力",
                Target = "",
                Remarks = "1、团建次数 2、团队和谐 3、团队聚餐",
                Weights = 50.00,
                KpiProperty = "Quantitative",
                PerfScoreModelID = 分档打分.ID,
                Completeness = ""
            });
            perfKPIsService.AddOrUpdate(new PerfKPIs {
                PerfProgramID = 考核方案.ID,
                PerfKPITypeID = 工作业绩.ID,
                KpiName = "团队业绩",
                Target = "",
                Remarks = "1、团队成绩突出 2、团队获奖次数 3、团队干劲十足",
                Weights = 50.00,
                KpiProperty = "Quantitative",
                PerfScoreModelID = 选择评分.ID,
                Completeness = ""
            });
            perfKPIsService.AddOrUpdate(new PerfKPIs {
                PerfProgramID = 考核方案.ID,
                PerfKPITypeID = 工作态度.ID,
                KpiName = "大家认可",
                Target = "",
                Remarks = "1、能胜任 2、大家佩服",
                Weights = 50.00,
                KpiProperty = "Quantitative",
                PerfScoreModelID = 选择评分.ID,
                Completeness = ""
            });
            perfKPIsService.AddOrUpdate(new PerfKPIs {
                PerfProgramID = 考核方案.ID,
                PerfKPITypeID = 工作能力.ID,
                KpiName = "互帮互助",
                Target = "",
                Remarks = "1、乐于助人 2、能力大家认可 3、帮助后进",
                Weights = 50.00,
                KpiProperty = "Quantitative",
                PerfScoreModelID = 选择评分.ID,
                Completeness = ""
            });
            perfKPIsService.AddOrUpdate(new PerfKPIs {
                PerfProgramID = 考核方案.ID,
                PerfKPITypeID = 管理提升.ID,
                KpiName = "管理团队日常",
                Target = "",
                Remarks = "1、兢兢业业 2、团队凝聚力提升",
                Weights = 50.00,
                KpiProperty = "Quantitative",
                PerfScoreModelID = 选择评分.ID,
                Completeness = ""
            });

            var users = userService.GetUsers("");
            foreach(var item in users) {
                perfObjectService.AddOrUpdate(new PerfObject {
                    AddTime = DateTime.Now,
                    ObjCode = item.Account,
                    ObjName = item.Name,
                    PerfProgramID = 考核方案.ID,
                });
            }
        }
    }
}
