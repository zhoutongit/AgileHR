﻿using System;
using System.Collections.Generic;

using Agile.Service;

namespace ConsoleApp {
    public class TestCommonData : ITestData {
        public int DisplayOrder => 1;
        ItemsService items = new ItemsService();
        IItemsDetailService itemsDetail = new ItemsDetailService();
        IScheduleTaskService taskService = new ScheduleTaskService();
        public void Run() {
            var settingService = new SettingService();

            ApplicationSettings applicationSettings = new ApplicationSettings();
            applicationSettings.DefaultTitle = "人力资源管理系统";
            applicationSettings.DefaultMetaKeywords = "AgileHR";
            applicationSettings.DefaultMetaDescription = "人力资源管理系统";
            applicationSettings.SiteClosed = false;
            applicationSettings.DefaultStoreTheme = "default";

            settingService.SaveSetting(applicationSettings);

            InitItem();

            AddTask();
        }
        private void InitItem() {
            string root = AddItemType(Guid.Empty.ToString(), "字典列表");

            string TYGL = AddItemType(root, "通用管理");
            string ROLETYPE = AddItemType(TYGL, "角色类型");
            AddItemData(ROLETYPE, "系统角色");
            AddItemData(ROLETYPE, "业务角色");
            AddItemData(ROLETYPE, "其他角色");
            string Flow = AddItemType(TYGL,"流程类型");
            AddItemData(Flow,"人事业务");
            AddItemData(Flow,"招聘业务");
            AddItemData(Flow,"考勤业务");
            AddItemData(Flow,"考核业务");
            string YHLX = AddItemType(TYGL, "用户类型");
            AddItemData(YHLX, "系统用户");
            AddItemData(YHLX, "老师");
            AddItemData(YHLX, "学生");
            string ZJXY = AddItemType(TYGL, "宗教信仰");
            AddItemData(ZJXY, "佛教");
            AddItemData(ZJXY, "道教");
            AddItemData(ZJXY, "天主教");
            AddItemData(ZJXY, "基督教");
            AddItemData(ZJXY, "喇嘛教");
            AddItemData(ZJXY, "伊斯兰教");
            AddItemData(ZJXY, "东正教");
            string JZKT = AddItemType(TYGL, "健康状况");
            AddItemData(JZKT, "健康或良好");
            AddItemData(JZKT, "一般或较弱");
            AddItemData(JZKT, "有慢性病");
            AddItemData(JZKT, "残疾");
            string ZJLX = AddItemType(TYGL, "证件类型");
            AddItemData(ZJLX, "居民身份证 ");
            AddItemData(ZJLX, "军官证 ");
            AddItemData(ZJLX, "士兵证 ");
            AddItemData(ZJLX, "文职干部证 ");
            AddItemData(ZJLX, "部队离退休证 ");
            AddItemData(ZJLX, "港澳台身份证件 ");
            AddItemData(ZJLX, "华侨身份证 ");
            AddItemData(ZJLX, "外籍护照 ");
            string HYZT = AddItemType(TYGL, "婚姻状态");
            AddItemData(HYZT, "未婚 ");
            AddItemData(HYZT, "已婚 ");
            AddItemData(HYZT, "再婚 ");
            AddItemData(HYZT, "复婚 ");
            AddItemData(HYZT, "丧偶 ");
            AddItemData(HYZT, "离婚 ");
            string ZZMM = AddItemType(TYGL, "政治面貌");
            AddItemData(ZZMM, "中共党员");
            AddItemData(ZZMM, "预备党员");
            AddItemData(ZZMM, "共青团员");
            AddItemData(ZZMM, "民革党员");
            AddItemData(ZZMM, "民盟盟员");
            AddItemData(ZZMM, "民建会员");
            AddItemData(ZZMM,"民进会员");   
            AddItemData(ZZMM, "农工党党员");
            AddItemData(ZZMM, "致公党党员");
            AddItemData(ZZMM, "九三学社社员");
            AddItemData(ZZMM, "台盟盟员");
            AddItemData(ZZMM, "群众");
            string TZGG = AddItemType(TYGL,"通知类型");
            AddItemData(TZGG,"通知公告");
            AddItemData(TZGG,"人才招聘");
            AddItemData(TZGG,"管理制度");

            string XXGL = AddItemType(root, "学校管理");
            string BXLX = AddItemType(XXGL, "办学类型");
            AddItemData(BXLX, "学前教育");
            AddItemData(BXLX, "初等教育");
            AddItemData(BXLX, "中等教育");
            AddItemData(BXLX, "高等教育");
            AddItemData(BXLX, "特殊教育");
            AddItemData(BXLX, "其他教育");
            string DWBB = AddItemType(XXGL, "单位办别");
            AddItemData(DWBB, "直属校办");
            AddItemData(DWBB, "中外合作办");
            AddItemData(DWBB, "校企合办");
            AddItemData(DWBB, "民办");
            AddItemData(DWBB, "其他");
            string DWLB = AddItemType(XXGL, "单位类别");
            AddItemData(DWLB, "教学院系");
            AddItemData(DWLB, "科研机构");
            AddItemData(DWLB, "公共服务");
            AddItemData(DWLB, "党务部门");
            AddItemData(DWLB, "行政机构");
            AddItemData(DWLB, "附属单位");
            AddItemData(DWLB, "后勤部门");
            AddItemData(DWLB, "校办产业");
            AddItemData(DWLB, "其他");
            string XXXZ = AddItemType(XXGL, "学校性质");
            AddItemData(XXXZ, "综合大学");
            AddItemData(XXXZ, "理工院校");
            AddItemData(XXXZ, "农业院校");
            AddItemData(XXXZ, "林业院校");
            AddItemData(XXXZ, "医药院校");
            AddItemData(XXXZ, "师范院校");
            AddItemData(XXXZ, "语文院校");
            AddItemData(XXXZ, "财经院校");
            AddItemData(XXXZ, "政法院校");
            AddItemData(XXXZ, "体育院校");
            AddItemData(XXXZ, "艺术院校");
            AddItemData(XXXZ, "民族院校");
            string JSGL = AddItemType(root, "教师管理");
            string BZLB = AddItemType(JSGL, "编制类别");
            AddItemData(BZLB, "教学类");
            AddItemData(BZLB, "科研类");
            AddItemData(BZLB, "行政类");
            AddItemData(BZLB, "教辅类");
            AddItemData(BZLB, "校办企业类");
            AddItemData(BZLB, "附设机构类");
            AddItemData(BZLB, "工勤类");
            AddItemData(BZLB, "其他类");
            string GXJZGLY = AddItemType(JSGL,"教职工来源");
            AddItemData(GXJZGLY, "录用应届毕业生");
            AddItemData(GXJZGLY, "军队转业复员");
            AddItemData(GXJZGLY, "调入");
            AddItemData(GXJZGLY, "引进人才");
            AddItemData(GXJZGLY, "社会招聘");
            AddItemData(GXJZGLY, "其他进校人员");
            string JZGLB = AddItemType(JSGL,"教职工类别");
            AddItemData(JZGLB,"校本部教职工");
            AddItemData(JZGLB,"专任教师");
            AddItemData(JZGLB,"行政人员");
            AddItemData(JZGLB,"教辅人员");
            AddItemData(JZGLB,"工勤人员");
            AddItemData(JZGLB,"科研机构人员");
            AddItemData(JZGLB,"校办企业职工");

            string JXGL = AddItemType(root, "教学管理");
            // string XSGL = AddItemType(root, "XSGL", "学生管理");

        }
        private string AddItemType(string pid, string name) {
            var model = items.AddOrUpdate(new Items {
                ParentID = pid,
                EnCode = Pinyin.GetInitials(name.Trim()),
                Name = name.Trim(),
                IsTree = true,
                Description = "",
                OrderBy = 1
            });
            return model.ID;
        }
        private string AddItemData(string typeId, string name) {
            var model = itemsDetail.AddOrUpdate(new ItemsDetail {
                ItemId = typeId,
                EnCode = Pinyin.GetInitials(name.Trim()),
                Name = name.Trim(),
                OrderBy = 1
            });
            return model.ID;
        }
        public void AddTask() {
            taskService.AddOrUpdate(new ScheduleTask {
                Name = "Clear log",
                Enabled = true,
                Seconds = 3600,
                Type = "Agile.Service.ClearLogTask, Agile.Service",
                StopOnError = true
            });
        }
    }
}
