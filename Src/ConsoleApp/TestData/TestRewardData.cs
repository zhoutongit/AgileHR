﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Reflection;

using Agile.Service;
using Agile.Service.Rbac;
using Agile.Service.Reward;

namespace ConsoleApp
{
    /// <summary>
    ///  https://www.cnblogs.com/wghao/archive/2009/05/16/1458104.html
    /// </summary>
    public class TestRewardData : ITestData
    {
        public int DisplayOrder => 5;
        ISalaryColumnService salaryColumnService = new SalaryColumnService();
        public void Run()
        {
            var root = salaryColumnService.AddOrUpdate(new SalaryColumn
            {
                ParentID = Guid.Empty.ToString(),
                Name = "在职人员",
                OperateExpression = "",
                OrderBy = 0,
                IfType = 1,
                SalaryType = 0,
                Remark = "",
            });
            AddSalaryColumn(root, "岗位工资", 1);
            AddSalaryColumn(root, "薪级", 2);
            AddSalaryColumn(root, "薪级工资", 3);
            AddSalaryColumn(root, "基础绩效", 4);
            AddSalaryColumn(root, "奖励绩效I", 5);
            AddSalaryColumn(root, "特殊津贴", 6);
            AddSalaryColumn(root, "养老保险", 7);
            AddSalaryColumn(root, "职业年金", 8);
            AddSalaryColumn(root, "医疗保险", 9);
            AddSalaryColumn(root, "失业保险", 10);
            AddSalaryColumn(root, "公积金", 11);
            AddSalaryColumn(root, "其他", 12);

            var root1 = salaryColumnService.AddOrUpdate(new SalaryColumn
            {
                ParentID = Guid.Empty.ToString(),
                Name = "退休人员",
                OperateExpression = "",
                OrderBy = 1,
                IfType = 1,
                SalaryType = 0,
                Remark = "",
            });
            AddSalaryColumn(root1, "其他", 1);
        }
        public void AddSalaryColumn(SalaryColumn root, string name, int order)
        {
            salaryColumnService.AddOrUpdate(new SalaryColumn
            {
                ParentID = root.ID,
                Name = name,
                OperateExpression = "",
                OrderBy = order,
                IfType = 0,
                SalaryType = SalaryType.Fixed,
                Remark = "",
            });
        }
    }
}
