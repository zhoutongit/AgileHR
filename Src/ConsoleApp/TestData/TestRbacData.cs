﻿using System;

using Agile.Service;
using Agile.Service.Rbac;

namespace ConsoleApp {
    public class TestRbacData:ITestData {
        public int DisplayOrder => 2;

        public readonly IMenuElementService menuElementDal = new MenuElementService();
        public readonly IMenuService menuDal = new MenuService(new MenuElementService());
        public readonly IOrganizeService organizeServiceDal = new OrganizeService();
        public readonly IRelevanceService relevanceService = new RelevanceService();
        public readonly INoticemodService noticemodService = new NoticemodService();

        public readonly IRoleService roleService = new RoleService(new RelevanceService());
        public readonly IUserService userService = new UserService(new RelevanceService());
        public readonly IApplicationService applicationService = new ApplicationService(new RelevanceService());
        public void Run() {

            AddApption();

            TestMenuElementData();
            TestOrgData();
            TestRoleData();
            TestUserData();
            TestNoticemod();
        }
        public void AddApption() {

        }
        private void TestMenuElementData() {

            var 平台 = applicationService.AddOrUpdate(new Application {
                Name = "平台",
            });
            var 人事 = applicationService.AddOrUpdate(new Application {
                Name = "人事",
            });
            var OA = applicationService.AddOrUpdate(new Application {
                Name = "OA",
            });
            var 学工 = applicationService.AddOrUpdate(new Application {
                Name = "学工",
            });
            var 教务 = applicationService.AddOrUpdate(new Application {
                Name = "教务",
            });

            /*
             
▪ 人事档案
▪ 组织架构
▪ 合同管理 *
▪ 薪酬管理 *
▪ 社保管理
▪ 绩效管理
▪ 考勤管理 *
▪ 培训管理 *
▪ 招聘管理 *
▪ 招聘门户
▪ 报表中心
▪ 预警功能
▪ 系统管理 *
▪ 热门功能
             */

            string FlowItem = AddMenu(false,Guid.Empty.ToString(),"代办事项","/FlowItem",0,平台.ID,"fa fa-list-alt");
            AddMenu(true,FlowItem,"待处理流程","/flow/flowinstances/wait",1,平台.ID);
            AddMenu(true,FlowItem,"我的流程","/flow/flowInstances/index",2,平台.ID);
            AddMenu(true,FlowItem,"已处理流程","/flow/flowinstances/disposed",3,平台.ID);
            AddMenu(true,FlowItem,"我的评论","/Sys/Comment/Index",4,平台.ID);
            AddMenu(true,FlowItem,"我的收藏","/Sys/Favorite/Index",5,平台.ID);
            AddMenu(true,FlowItem,"我的绩效","/Assess/PerfExaminer/Index",6,平台.ID);

            string Notice = AddMenu(false,Guid.Empty.ToString(),"通知公告","/Notice",1,平台.ID,"fa fa-commenting");
            AddMenu(true,Notice,"发布通知","/Sys/Noticemod/Index",1,平台.ID);

            string Job = AddMenu(false,Guid.Empty.ToString(),"招聘管理","/Job",2,人事.ID,"fa fa-hand-rock-o");
            AddMenu(true,Job,"招聘计划","/Job",1,平台.ID);
            AddMenu(true,Job,"简历审批","/Job",2,平台.ID);
            AddMenu(true,Job,"成绩录入","/Job",3,平台.ID);
            AddMenu(true,Job,"简历库","/Job",4,平台.ID);

            string Personnel = AddMenu(false,Guid.Empty.ToString(),"人员管理","/Personnel",3,人事.ID,"fa fa-users");
            AddMenu(true,Personnel,"人员花名册"," /Personnel/Roster/Index",0,平台.ID);
            AddMenu(true,Personnel,"教育经历"," /Personnel/EducationResume/Index",1,平台.ID);
            AddMenu(true,Personnel,"培训经历","/Personnel/TrainResume/Index",2,平台.ID);
            AddMenu(true,Personnel,"项目经验","/Personnel/ProjectResume/Index",3,平台.ID);
            AddMenu(true,Personnel,"工作经验","/Personnel/WorkResume/Index",4,平台.ID);
            AddMenu(true,Personnel,"家庭成员"," /Personnel/Socialrelations/Index",5,平台.ID);

            string PersonnelChange = AddMenu(false,Guid.Empty.ToString(),"变动管理","/ChangeControl",4,人事.ID,"fa fa-map-signs");
            AddMenu(true,PersonnelChange,"部门调动","/ChangeControl/OrgTransfer/Index",1,平台.ID);

            string contract = AddMenu(false,Guid.Empty.ToString(),"合同管理","/contract",4,人事.ID,"fa fa-map-signs");
            AddMenu(true,contract,"合同模板","/contract/OrgTransfer/Index",1,平台.ID);
            AddMenu(true,contract,"合同签订","/contract/OrgTransfer/Index",2,平台.ID);
            AddMenu(true,contract,"合同台账","/contract/OrgTransfer/Index",3,平台.ID);
            AddMenu(true,contract,"到期统计","/contract/OrgTransfer/Index",4,平台.ID);

            string Attendance = AddMenu(false,Guid.Empty.ToString(),"考勤管理","/Attendance",5,人事.ID,"fa fa-tachometer");
            AddMenu(true,Attendance,"请假销假","/Attendance/leave/Index",1,平台.ID);
            AddMenu(true,Attendance,"考勤周报","/Attendance/week/Index",2,平台.ID);
            AddMenu(true,Attendance,"考勤月报","/Attendance/month/Index",3,平台.ID);
            AddMenu(true,Attendance,"考勤年报","/Attendance/year/Index",4,平台.ID);

            string kpi = AddMenu(false,Guid.Empty.ToString(),"考核管理","/Assess",6,人事.ID,"fa fa-heartbeat");
            AddMenu(true,kpi,"考核方案分类","/Assess/PerfProgramCategory/Index",1,平台.ID);
            AddMenu(true,kpi,"考核方案","/Assess/PerfProgram/Index",2,平台.ID);
            AddMenu(true,kpi,"考核对象","/Assess/PerfObject/Index",3,平台.ID);
            AddMenu(true,kpi,"考核指标","/Assess/PerfKPIs/Index",4,平台.ID);
            AddMenu(true,kpi,"考核指标类型","/Assess/PerfKPIType/Index",5,平台.ID);
            AddMenu(true,kpi,"考核打分","/Assess/PerfScore/Index",6,平台.ID);
            AddMenu(true,kpi,"打分方式","/Assess/PerfScoreModel/Index",7,平台.ID);

            //调查问卷
            string tcwj = AddMenu(false,Guid.Empty.ToString(),"调查问卷","/Assess",6,人事.ID,"fa fa-heartbeat");
            AddMenu(true,tcwj,"矩阵标题","/Survey/SurArrayTitle/Index",1,人事.ID);
            AddMenu(true,tcwj,"收集设置","/Survey/SurFilter/Index",2,人事.ID);
            AddMenu(true,tcwj,"问卷问题","/Survey/SurQuestion/Index",3,人事.ID);
            AddMenu(true,tcwj,"问题选项","/Survey/SurQuestionChoice/Index",4,人事.ID);
            AddMenu(true,tcwj,"调查问卷","/Survey/SurQuestionNaire/Index",5,人事.ID);
            AddMenu(true,tcwj,"报表过滤条件","/Survey/SurReportFilter/Index",6,人事.ID);
            AddMenu(true,tcwj,"用户问卷情况","/Survey/SurResponseList/Index",7,人事.ID);
            AddMenu(true,tcwj,"调查结果","/Survey/SurResult/Index",7);

            string Reward = AddMenu(false,Guid.Empty.ToString(),"薪酬管理","/Reward",7,人事.ID,"fa fa-credit-card");
            AddMenu(true,Reward,"工资公式","/Reward/SalaryColumn/Index",1,人事.ID);
            AddMenu(true,Reward,"工资表","/Reward/Salary/Index",2,人事.ID);

            string FlowDesign = AddMenu(false,Guid.Empty.ToString(),"流程中心","/Flow",8,平台.ID,"fa fa-openid");
            AddMenu(true,FlowDesign,"表单设计","/flow/DivModel/Index",1,平台.ID);
            AddMenu(true,FlowDesign,"流程设计","/flow/flowSchemes/Index",2,平台.ID);

            string School = AddMenu(false,Guid.Empty.ToString(),"学校信息","/School",11,人事.ID,"fa fa-graduation-cap");
            AddMenu(true,School,"校区信息","/School/SchoolArea/Index",1,人事.ID);
            AddMenu(true,School,"教学楼","/School/TeachingBuilding/Index",2,人事.ID);
            AddMenu(true,School,"教学功能区","/School/FunctionArea/Index",3,人事.ID);
            AddMenu(true,School,"教室信息","/School/ClassRoom/Index",4,人事.ID);
            AddMenu(true,School,"班级信息","/School/Classes/Index",5,人事.ID);
            AddMenu(true,School,"学年学期","/School/AcadYearTerm/Index",6,人事.ID);
            AddMenu(true,School,"专业信息","/School/Major/Index",7,人事.ID);
            AddMenu(true,School,"组织机构","/rbac/organize/Index",8,人事.ID);
            AddMenu(true,School,"岗位设置","/rbac/Duty/Index",9,人事.ID);
            AddMenu(true,School,"数据字典","/rbac/ItemsData/Index",10,人事.ID);
            AddMenu(true,School,"区域管理","/Sys/Area/Index",11,人事.ID);

            string Basics = AddMenu(false,Guid.Empty.ToString(),"基础设施","/Basics",12,平台.ID,"fa fa-asterisk");
            AddMenu(true,Basics,"用户管理","/rbac/user/Index",1);
            AddMenu(true,Basics,"角色管理","/rbac/role/Index",2);
            AddMenu(true,Basics,"菜单管理","/rbac/menu/Index",3);
            AddMenu(true,Basics,"计划任务","/Sys/ScheduleTask/Index",4);
            AddMenu(true,Basics,"系统日志","/Sys/SysLog/Index",5);
            AddMenu(true,Basics,"在线用户","/rbac/UserOnline/Index",6);
            AddMenu(true,Basics,"友情链接","/Sys/Friendlink/Index",7);

            string Setting = AddMenu(false,Guid.Empty.ToString(),"系统管理","/Setting",13,平台.ID,"fa fa-cog");
            AddMenu(true,Setting,"系统配置","/rbac/Setting/Index",2);
            AddMenu(true,Setting,"应用管理","/Sys/Application/Index",6);
            AddMenu(true,Setting,"系统结构","/Sys/DatabaseTable/Index",7);
            AddMenu(true,Setting,"图标管理","/Sys/Icon/Index",8);
            AddMenu(true,Setting,"健康检查","/Sys/Healths/Index",9);
        }
        private string AddMenu(bool but,string pid,string name,string url,int order,string appId = "",string icon = "fa fa-file-text-o") {
            var model = menuDal.AddOrUpdate(new Menu {
                ParentID = pid,
                Code = Pinyin.GetInitials(name.Trim()),
                Name = name.Trim(),
                Url = url,
                IsSys = true,
                Status = 0,
                Icon = icon,
                OrderBy = order,
                AddTime = DateTime.Now
            });
            string moduleId = model.ID;
            if(!string.IsNullOrWhiteSpace(appId)) {
                relevanceService.Assign(new AssignReq { firstId = appId,secIds = new[] { moduleId },type = Define.APPLICATIONMODULE });
            }
            if(but) {
                menuElementDal.AddOrUpdate(new MenuElement { ModuleId = moduleId,DomId = "NF-add",Name = "新建",Script = "btn_add()",OrderBy = 1 });
                menuElementDal.AddOrUpdate(new MenuElement { ModuleId = moduleId,DomId = "NF-delete",Name = "删除",Script = "btn_delete()",OrderBy = 2 });
                menuElementDal.AddOrUpdate(new MenuElement { ModuleId = moduleId,DomId = "NF-edit",Name = "修改",Script = "btn_edit()",OrderBy = 3 });
                menuElementDal.AddOrUpdate(new MenuElement { ModuleId = moduleId,DomId = "NF-Details",Name = "查看",Script = "btn_details()",OrderBy = 4 });
            }
            return moduleId;
        }
        private void TestOrgData() {
            string root = AddOrganize(Guid.Empty.ToString(),"技术学院",1);
            string dqjg = AddOrganize(root,"党群机构",1);
            AddOrganize(dqjg,"院领导",1);
            AddOrganize(dqjg,"党政办公室",2);
            AddOrganize(dqjg,"组织人事部",3);
            AddOrganize(dqjg,"宣传统战部",4);
            AddOrganize(dqjg,"纪委办",5);
            AddOrganize(dqjg,"工会",6);
            AddOrganize(dqjg,"团委",7);
            string xzjg = AddOrganize(root,"行政机构",2);
            AddOrganize(xzjg,"教务处",1);
            AddOrganize(xzjg,"学工处",2);
            AddOrganize(xzjg,"后勤处",3);
            AddOrganize(xzjg,"计财处",4);
            AddOrganize(xzjg,"招就处",5);
            AddOrganize(xzjg,"保卫处",6);
            AddOrganize(xzjg,"科研处",7);
            string jxjg = AddOrganize(root,"教学机构",3);
            AddOrganize(jxjg,"机电工程分院",1);
            AddOrganize(jxjg,"艺术分院",2);
            AddOrganize(jxjg,"学前教育分院",3);
            AddOrganize(jxjg,"经济管理分院",4);
            AddOrganize(jxjg,"旅游分院",5);
            AddOrganize(jxjg,"信息工程分院",6);
            AddOrganize(jxjg,"汽车工程分院",7);
            AddOrganize(jxjg,"军体部",8);
            AddOrganize(jxjg,"思政部",9);
            AddOrganize(jxjg,"继续教育分院",10);
            AddOrganize(jxjg,"经管分院",11);
            string jfjg = AddOrganize(root,"教辅机构",4);
            AddOrganize(jfjg,"信息中心",1);
            AddOrganize(jfjg,"图书馆",2);
            AddOrganize(jfjg,"宣传部",3);
            AddOrganize(jfjg,"实训产业处",4);
            AddOrganize(jfjg,"对外交流与合作中心",6);
            AddOrganize(jfjg,"教学质量管理与评价中心",7);

        }
        public string AddOrganize(string pid,string name,int order) {
            var model = organizeServiceDal.AddOrUpdate(new Organize {
                ParentID = pid,
                EndCode = Pinyin.GetInitials(name.Trim()),
                Name = name.Trim(),
                Icon = "",
                OrderBy = order,
                Status = 0,
                TenantId = "",
                AddTime = DateTime.Now,
                IsDelete = 0
            });
            return model.ID;
        }
        private void TestRoleData() {
            //常用角色
            AddRole("超级管理员",DicData("角色类型"),1);
            AddRole("系统管理员",DicData("角色类型"),1);
            AddRole("系统配置员",DicData("角色类型"),1);
            AddRole("系统开发人员",DicData("角色类型"),1);
            AddRole("系统操作员",DicData("角色类型"),1);
            AddRole("访客人员",DicData("角色类型"),1);
            AddRole("测试人员",DicData("角色类型"),1);
            //常用岗位
            AddRole("主席","",2);
            AddRole("副书记","",2);
            AddRole("馆长","",2);
            AddRole("副主任","",2);
            AddRole("主任","",2);
            AddRole("副处长","",2);
            AddRole("处长","",2);
            AddRole("副部长","",2);
            AddRole("部长","",2);
            AddRole("支部书记","",2);
            AddRole("副院长","",2);
            AddRole("院长","",2);
            AddRole("干事","",2);
        }
        public void AddRole(string name,string type,int category) {
            roleService.AddOrUpdate(new Role {
                Name = name.Trim(),
                EnCode = Pinyin.GetInitials(name.Trim()),
                Type = type,
                Category = category,
                AddTime = DateTime.Now
            });
        }
        private void TestUserData() {
            for(int i = 0;i < 100;i++) {
                string orgId = userService.DBA.ExecuteScalar<string>("SELECT TOP 1 ID FROM rbac_organize WHERE ParentID!='00000000-0000-0000-0000-000000000000' ORDER BY NEWID()");
                string roleId = userService.DBA.ExecuteScalar<string>("SELECT TOP 1 ID FROM dbo.rbac_role WHERE Category=1 ORDER BY NEWID()");
                string[] idCardInfo = IDCardInfoHelper.GetIdCardInfo("362430198508160639");
                var birthDate = idCardInfo[1].ToDateTime();
                var user = userService.AddOrUpdate(new User {
                    Account = "luomingui" + i,
                    Name = "罗敏贵" + i,
                    Email = "luomingui@163.com",
                    Password = "123456".ToMD5String(),
                    IdcardType = "身份证",
                    Idcard = "362430198508160639",
                    Mobile = "13320163492",
                    Sex = 1,
                    Marry = DicData("婚姻状态"),
                    PoliticalID = DicData("政治面貌"),
                    Health = DicData("健康状况"),
                    Faith = DicData("宗教信仰"),
                    Nationality = "中国",
                    BirthDate = birthDate,
                    NativePlace = idCardInfo[0],
                    Address = idCardInfo[0],
                    Zodiac = IDCardInfoHelper.GetZodiac(birthDate),
                    Constellation = IDCardInfoHelper.GetConstellation(birthDate),
                    Zipcode = "000000",
                    NationalID = "汉族",
                });

                relevanceService.AssignOrgUsers(new AssignOrgUsers { OrgId = orgId,UserIds = new string[] { user.ID },IsDelete = false });

                relevanceService.AssignRoleUsers(new AssignRoleUsers { RoleId = roleId,UserIds = new string[] { user.ID },IsDelete = false });

            }
        }
        public string DicData(string dicName) {
            string keyValue = userService.DBA.ExecuteScalar<string>(@"SELECT TOP 1 bid.ID FROM [dbo].[base_itemsdetail] bid 
INNER JOIN [dbo].[base_items] bi ON bid.ItemId=bi.ID WHERE bi.Name = '" + dicName + "' ORDER BY NEWID()");
            return keyValue;
        }
        public void TestNoticemod() {
            for(int i = 0;i < 100;i++) {
                noticemodService.AddOrUpdate(new Noticemod { Title = "南昌滕王阁等中国历史文化名楼为“星星的孩子”点亮蓝灯",Content = "南昌滕王阁等中国历史文化名楼为“星星的孩子”点亮蓝灯",CurryType = DicData("通知类型"),AddUserID = "admin",EditUserID = "admin" });
            }
        }
    }
}
