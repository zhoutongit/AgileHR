﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    public interface ITestData
    {
        void Run();
        int DisplayOrder { get;}
    }
}
