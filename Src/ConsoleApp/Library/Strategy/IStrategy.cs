﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp {
    public interface IStrategy {
        int Sid { get; set; }
        string Name { get; set; }
        void Run(object data);
    }
    public class Context {
        private IStrategy _strategy;
        public void SetStrategy(IStrategy communication) {
            this._strategy = communication;
        }
        public void Send(object data) {
            this._strategy.Run(data);
        }
    }
    public abstract class Strategy:IStrategy {
        public abstract int Sid { set; get; }
        public abstract string Name { set; get; }
        public string BaseDirectory {
            get {
                return AppDomain.CurrentDomain.BaseDirectory + "Content";
            }
        }
        public void Run(object data) {
            Console.WriteLine("*******"+ Name + "开始***************");
            RunCode(data);
            Console.WriteLine("*******" + Name + "结束***************");
        }
        public abstract void RunCode(object data);
    }
}
