﻿//https://blog.csdn.net/olifchou/article/details/78740362
/**
 * 解释一下：
1、page.viewportSize是定义浏览器的宽和高；
2、page.clipRect就是在指定截图区域的宽和高以及距浏览器左边和上边的距离，
代码中的bb.top与bb.left代表的就是浏览器的边缘，
建议指定截图区域距离上边距离的时候以bb.top+50 这种形式来指定，左边亦如此；
3、window.setTimeout是用来指定浏览器加载时间的。
————————————————
 * */
var page = require('webpage').create(), system = require('system'), address, output, size;

if (system.args.length < 3 || system.args.length > 5) {
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    //定义宽高
    page.viewportSize = {
        width: 1366,
        height: 768
    };

    phantom.addCookie({
        "domain": "localhost",
        "expires": "Fri, 01 Jan 2038 00:00:00 GMT",
        "expiry": 2145916800,
        "httponly": false,
        "name": "HDJ.Session",
        "path": "/",
        "secure": false,
        "value": "CfDJ8G56iXVwSupLu542Wrj%2BaPw0c0ZmDTTxd3Nn3KH1o7MZ2K09QaQnpgFmfGT9902hjTFDsYLtwg1ClYkw6dLRnZepY12QIZamBetINa5OZOroBCq%2BY9a%2FvSZWNEBhmjNQQE6UE8UyQ1hfgG5l1w3RUV7kq0ms81yLqdoG%2Fab%2F%2BtU1"
    });

    page.open(address, function (status) {
        if (address.indexOf("Login") != -1) {
            window.setTimeout(function () {
                page.evaluate(function () {
                    $("#userName")[0].value = 'admin'
                    $("#passWord")[0].value = 'hdj_admin'
                    $("#login_button").click();
                });
            }, 1000);
        } else {
            //截图
            var bb = page.evaluate(function () {
                document.body.bgColor = 'white';
                return document.getElementsByTagName('html')[0].getBoundingClientRect();
            });
            page.clipRect = {
                top: bb.top,
                left: bb.left,
                width: bb.width,
                height: bb.height
            };
            window.setTimeout(function () {
                page.render(output);
                page.close();
                console.log('渲染成功...');
                phantom.exit();
            }, 1000);
        }
    });
}
