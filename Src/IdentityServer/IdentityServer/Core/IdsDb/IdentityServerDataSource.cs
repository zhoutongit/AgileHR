﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using IdentityServer4.Models;
using IdentityServer4.Test;

namespace IdentityServer
{
    public class IdentityServerDataSource
    {
        public static IEnumerable<ApiScope> ApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("scope1","scope1")
            };
        }
        public static IEnumerable<ApiResource> ApiResources()
        {
            return new[]
            {
                new ApiResource
                {
                    Name = "api1",
                    DisplayName = "My Api1",
                    Scopes = { "scope1" }
                }
            };
        }
        public static async Task<IEnumerable<Client>> Clients()
        {
            List<Client> list = new List<Client>();
            list.Add(
                new Client
                {
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets = { new Secret("secret".Sha256()) }
                }
            );

            ICAS_ClientService cAS_Client = new CAS_ClientService();
            var clients = cAS_Client.GetByAll();
            foreach (var client in clients)
            {
                ICollection<string> AllowedGrantTypes = client.AllowedGrantTypes.Split(",");
                Client model = new Client
                {
                    ClientId = client.ClientId,
                    AllowedGrantTypes = AllowedGrantTypes,
                    ClientSecrets = { new Secret(client.ClientSecrets.Sha256()) },
                    AllowedScopes = { "scope1" }
                };
                list.Add(model);
            }
            return await Task.FromResult(list);


        }
        /// <summary>
        /// Users
        /// </summary>
        /// <returns></returns>
        public static async Task<IEnumerable<TestUser>> Users()
        {
            List<TestUser> list = new List<TestUser>();
            list.Add(new TestUser { SubjectId = "admin", Username = "admin", Password = "admin" });

            ICAS_UserService cAS_User = new CAS_UserService();
            var users = cAS_User.GetByAll();
            foreach (var user in users)
            {
                TestUser model = new TestUser();
                model.SubjectId = user.SubjectId;
                model.Username = user.Username;
                model.Password = user.Password;
                list.Add(model);
            }
            return await Task.FromResult(list);
        }
    }
}
