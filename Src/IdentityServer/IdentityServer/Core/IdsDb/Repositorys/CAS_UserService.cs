﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace IdentityServer
{
    public interface ICAS_UserService : IRepository<CAS_User>
    {

    }
    public partial class CAS_UserService : DapperRepository<CAS_User>, ICAS_UserService
    {

    }
}