﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace IdentityServer
{
    public interface ICAS_ClientService : IRepository<CAS_Client>
    {

    }
    public partial class CAS_ClientService : DapperRepository<CAS_Client>, ICAS_ClientService
    {

    }
}