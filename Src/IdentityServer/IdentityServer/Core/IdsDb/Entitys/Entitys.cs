﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

using IdentityServer4.Models;

namespace IdentityServer
{
    /// <summary>
    /// 客户
    /// </summary>
    [Table("CAS_Client")]
    [Description("客户")]
    public class CAS_Client : Entity
    {
        public CAS_Client()
        {
            AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials.ToJson();
            ClientSecrets = "secret".Sha256();
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        [Description("客户编号")]
        public string ClientId { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        [Description("客户名称")]
        public string ClientName { get; set; }
        /// <summary>
        /// 授权类型
        /// </summary>
        [Description("授权类型")]
        public string AllowedGrantTypes { get; set; }
        /// <summary>
        /// //如果不需要显示否同意授权 页面 这里就设置为false
        /// </summary>
        [Description("显示授权页面")]
        public bool RequireConsent { get; set; }
        /// <summary>
        /// 客户秘钥
        /// </summary>
        [Description("客户秘钥")]
        public string ClientSecrets { get; set; }
        /// <summary>
        /// 客户描述
        /// </summary>
        [Description("客户描述")]
        public string Description { get; set; }
        /// <summary>
        /// 客户地址
        /// </summary>
        [Description("客户地址")]
        public string ClientUri { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public string LogoUri { get; set; }
        /// <summary>
        /// 注销登录后返回的客户端地址
        /// </summary>
        [Description("退出跳转地址")]
        public string PostLogoutRedirectUris { get; set; }
        /// <summary>
        /// 登录成功后返回的客户端地址
        /// </summary>
        [Description("登录跳转地址")]
        public string RedirectUris { get; set; }
    }
    /// <summary>
    /// 用户
    /// </summary>
    [Table("CAS_User")]
    [Description("用户")]
    public class CAS_User : Entity
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Description("账号")]
        public string SubjectId { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [Description("姓名")]
        public string Username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Description("密码")]
        public string Password { get; set; }
        /// <summary>
        /// 提供者名称
        /// </summary>
        [Description("提供者名称")]
        public string ProviderName { get; set; }
        /// <summary>
        /// 提供者
        /// </summary>
        [Description("提供者")]
        public string ProviderSubjectId { get; set; }
        /// <summary>
        /// 活跃
        /// </summary>
        [Description("活跃")]
        public bool IsActive { get; set; }
    }
}
