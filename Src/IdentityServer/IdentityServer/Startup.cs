using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Agile;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IdentityServer
{
    public class Startup
    {
        public const string CorsName = "IdentityServer";
        #region Fields
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }
        #endregion
        public void ConfigureServices(IServiceCollection services)
        {
            // ����cookie����
            services.Configure<CookiePolicyOptions>(options =>
            {
                //https://docs.microsoft.com/zh-cn/aspnet/core/security/samesite?view=aspnetcore-3.1&viewFallbackFrom=aspnetcore-3
                options.MinimumSameSitePolicy = SameSiteMode.Lax;
            });
            services.AddAgileCore(_configuration, _webHostEnvironment);
            services.AddControllersWithViews();
            services.AddIdentityServer()
            .AddDeveloperSigningCredential()
            .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
            .AddClientStore<ClientStore>()
            .AddInMemoryApiScopes(IdentityServerDataSource.ApiScopes())
            .AddInMemoryApiResources(IdentityServerDataSource.ApiResources());

            services.AddCors(op => {
                op.AddPolicy(CorsName, set => {
                    set.SetIsOriginAllowed(origin => true).AllowAnyHeader()
                    .AllowAnyMethod().AllowCredentials();
                });
            });
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseRouting();
            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthorization();
            app.UseCors(CorsName);
            app.UseAgileCore();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                   name: "default",
                   pattern: "{controller=Home}/{action=Index}/{id?}"
               );
            });
        }
    }
}
