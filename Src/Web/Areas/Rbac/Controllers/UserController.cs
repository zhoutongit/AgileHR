﻿using System;
using System.Collections.Generic;
using Agile.Service;
using Agile.Service.Rbac;

using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Web.Areas.Rbac.Controllers {
    [Area("rbac")]
    public class UserController:BaseController<User> {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IRelevanceService _relevanceService;
        private readonly ApplicationSettings _applicationSettings;
        public UserController(IUserService userService,
            IRoleService roleService,
            IRelevanceService relevanceService,ApplicationSettings applicationSettings) {
            _userService = userService;
            _roleService = roleService;
            _relevanceService = relevanceService;
            _applicationSettings = applicationSettings;
        }

        [HttpPost]
        [HttpGet]
        public override IActionResult GetJGridPageDataJson(Paging paging) {
            string sqlwhere = " 1=1 ";

            if(!string.IsNullOrWhiteSpace(paging.keyword)) {
                JObject jo = JObject.Parse(paging.keyword);//吧json字符串转化为json对象
                string key = jo["key"] == null ? "" : jo["key"].ToString();
                string OrgId = jo["OrgId"] == null ? "" : jo["OrgId"].ToString();

                var ids = _relevanceService.FindAll(Define.USERORG,false,OrgId);
                if(!string.IsNullOrWhiteSpace(key)) {
                    sqlwhere += $" AND (Account like ('%{key}%') " +
                                $" OR Name like ('%{key}%') " +
                                $" OR Idcard like ('%{key}%') " +
                                $" OR Mobile like ('%{key}%')) ";
                }
                if(ids != null && ids.Count > 0) {
                    sqlwhere += $" and id in ({ids.ToSql(false)}) ";
                }

                paging.page = 1;
            }
            paging.sqlwhere = sqlwhere;
            JQridPageData pageData = Repository.FindAll(paging);
            return Content(pageData.ToJson());
        }

        #region UserRole
        [HttpGet]
        public IActionResult UserRole() {
            var list = _roleService.GetRoles();
            return View(list);
        }
        [HttpPost]
        public IActionResult UserRole(string userids,string[] roleIds) {
            try {
                if(ModelState.IsValid == false) {
                    return Error();
                }
                string[] _userids = userids.Split(",");
                for(int i = 0;i < roleIds.Length;i++) {
                    string keyValue = roleIds[i];
                    _relevanceService.UnAssign(new AssignReq { type = Define.USERROLE,firstId = keyValue });
                    _relevanceService.AssignRoleUsers(new AssignRoleUsers {
                        RoleId = keyValue,
                        UserIds = _userids,
                        IsDelete = true

                    });
                }
                return this.Success();
            } catch(Exception ex) {
                Logger.LogError(this.GetType().Name,ex);
                return this.Error();
            }
        }
        public List<User> GetUsers(string orgId) {
            return _userService.GetUsers(orgId);
        }
        #endregion

        #region MyRegion
        [HttpPost]
        public IActionResult ResetPassword(string keyValue) {
            if(string.IsNullOrWhiteSpace(keyValue)) return Error("参数keyValue不能为空！");
            _userService.ResetPassword(keyValue,_applicationSettings.AdminPwd);
            return this.Success();
        }
        #endregion
    }
}