﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Agile.Service;
using Agile.Service.Rbac;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers {
    [Area("rbac")]
    public class RoleController:BaseController<Role> {
        private readonly IRoleService _roleService;
        private readonly IMenuService _menuService;
        private readonly IMenuElementService _menuelementService;
        private readonly IRelevanceService _relevanceService;
        public RoleController(IRoleService roleService,
            IMenuService menuService,
            IMenuElementService menuelementService,
            IRelevanceService relevanceService) {
            _roleService = roleService;
            _menuService = menuService;
            _menuelementService = menuelementService;
            _relevanceService = relevanceService;
        }
        [HttpPost]
        public override IActionResult Switch(string keyId) {
            _roleService.Switch(keyId);
            return Success("操作成功。");
        }
        public override IActionResult GetGridJson(string keyword) {
            var data = _roleService.GetRoles(keyword);
            return Content(data.ToJson());
        }
        [HttpPost]
        public override IActionResult DeleteForm(string keyValue) {
            _relevanceService.UnAssign(new AssignReq { type = Define.ROLEMODULE,firstId = keyValue });
            _relevanceService.UnAssign(new AssignReq { type = Define.ROLEELEMENT,firstId = keyValue });
            _relevanceService.UnAssign(new AssignReq { type = Define.ROLEORG,firstId = keyValue });
            return base.DeleteForm(keyValue);
        }
        [HttpPost]
        public IActionResult SaveRole(RoleReq roleReq) {
            if(roleReq == null) return Error("提交失败！");
            roleReq.Category = 1;
            Result result = _roleService.SubmitForm(roleReq);
            return Content(result.ToJson());
        }
        public IActionResult GetMenusTree(string roleId) {
            var treeList = _roleService.GetMenuElementTree(roleId);
            return Content(treeList.zTreeViewJson());
        }
        public IActionResult GetBranTree(string roleId) {
            var treeList = _roleService.GetBranTree(roleId);
            return Content(treeList.zTreeViewJson());
        }
    }
}