﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers {
    [Area("rbac")]
    public class UserOnlineController:  BaseController<UserOnline> {
        private readonly IUserOnlineService _useronlineService;
        public UserOnlineController(IUserOnlineService useronlineService) {
            _useronlineService = useronlineService;
        }
    }
}
