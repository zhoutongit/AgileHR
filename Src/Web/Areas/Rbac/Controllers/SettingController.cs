﻿using System;
using System.Linq;
using System.Collections.Generic;
using Agile.Service;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Areas.Rbac.Controllers {
    /// <summary>
    /// 系统设置
    /// </summary>
    [Area("rbac")]
    public class SettingController:BaseController<Setting> {
        private readonly ISettingService _settingService;
        private readonly IApplicationService _applicationService;
        public SettingController(ISettingService settingService,
            IApplicationService applicationService) {
            _settingService = settingService;
            _applicationService = applicationService;
        }

        #region AppSettings
        public override IActionResult Index() {
            var set = _settingService.LoadSetting<ApplicationSettings>();
            if(set == null) set = new ApplicationSettings();

            return View(new SettingModel { AppSettings = set,AppSystem = _applicationService.FindAll().ToList() });
        }
        [HttpPost]
        public virtual IActionResult AppSettings(ApplicationSettings model) {
            _settingService.SaveSetting(model);
            return RedirectToAction("Index");
        }
        #endregion
    }
  
}
