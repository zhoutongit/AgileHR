﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agile.Service;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers
{
    /// <summary>
    /// 选项主表
    /// </summary>
    [Area("rbac")]
    public class ItemsTypeController : BaseController<Items>
    {
        private readonly IItemsService _itemsService;
        public ItemsTypeController(IItemsService itemsService)
        {
            _itemsService = itemsService;
        }
        [HttpGet]
        public ActionResult GetTreeSelectJson()
        {
            var data = _itemsService.FindAll();
            var treeList = new List<TreeSelectModel>();
            foreach (Items item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.ID;
                treeModel.text = item.Name;
                treeModel.parentId = item.ParentID;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
    }
}
