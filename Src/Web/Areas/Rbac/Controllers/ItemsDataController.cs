﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agile.Service;
using Agile.Service.Rbac;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Area("rbac")]
    public class ItemsDataController : BaseController<ItemsDetail>
    {
        private readonly IItemsService _itemsService;
        private readonly IItemsDetailService _itemsdetailService;
        public ItemsDataController(IItemsService itemsService,IItemsDetailService itemsdetailService)
        {
            _itemsService = itemsService;
            _itemsdetailService = itemsdetailService;
        }
        public IActionResult GetSelectJson(string enCode) 
        {
            var data = _itemsdetailService.GetItemList(enCode);
            List<object> list = new List<object>();
            list.Add(new { id = "",text = "==请选择==" });
            foreach (ItemsDetail item in data)
            {
                list.Add(new { id = item.ID, text = item.Name });
            }
            return Content(list.ToJson());
        }
        [HttpGet]
        public virtual IActionResult GetItemDetailGridJson(string itemId, string keyword, string sidx = "OrderBy", string sord = "desc", int page = 1, int rows = 15)
        {
            System.Text.StringBuilder strWhere = new System.Text.StringBuilder();
            strWhere.Append(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(itemId))
            {
                strWhere.Append(" AND ItemId='"+ itemId + "' ");
            }
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                strWhere.Append(" AND Name like '%" + keyword + "%' ");
            }
            Paging paging = new Paging();
            paging.field = "*";
            paging.sidx = sidx;
            paging.sord = sord;
            paging.page = page;
            paging.rows = rows;
            paging.sqlwhere = strWhere.ToString();
            JQridPageData pageData = _itemsdetailService.FindAll(paging);
            return Content(pageData.ToJson());
        }
        [HttpPost]
        public override IActionResult SubmitForm(ItemsDetail moduleEntity)
        {
            moduleEntity.EnCode = Pinyin.GetInitials(moduleEntity.Name.Trim());
            return base.SubmitForm(moduleEntity);
        }
        [HttpGet]
        public IActionResult BatchAdd()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SubmitBatchAddForm(string ItemId,string Names)
        {
            if(string.IsNullOrWhiteSpace(ItemId)) {
                return Error("字典保存失败。");
            }
            string[] ContentLines = Names.Split(new string[] { "\n" },StringSplitOptions.RemoveEmptyEntries);
            if(ContentLines.Length > 0) {
                for(int i = 0;i < ContentLines.Length;i++) {
                    string str = ContentLines[i];
                    ItemsDetail detail = new ItemsDetail();
                    detail.ItemId = ItemId;
                    detail.EnCode = Pinyin.GetInitials(str);
                    detail.SimpleSpelling = detail.EnCode;
                    detail.Name = str;
                    detail.ParentID = "";
                    detail.Description = "";
                    detail.OrderBy = 0;
                    detail.Status = 0;
                    detail.AddTime = DateTime.Now;
                    _itemsdetailService.AddOrUpdate(detail);
                }
                return Success("字典保存成功。");
            } else {
                return Error("字典保存失败。");
            }
        }
    }
}
