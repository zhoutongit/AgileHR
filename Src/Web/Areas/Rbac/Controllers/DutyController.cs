﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agile.Service;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers
{
    /// <summary>
    /// 岗位管理
    /// </summary>
    [Area("rbac")]
    public class DutyController : BaseController<Role>
    {
        private readonly IRoleService _roleService;
        public DutyController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        public override IActionResult GetGridJson(string keyword)
        {
            string where = " Category=2 ";
            if (!string.IsNullOrEmpty(keyword))
            {
                where = " and (Name='" + keyword + "' or EnCode='"+ keyword + "')";
            }
            var data = _roleService.FindAll(where);
            return Content(data.ToJson());
        }
        public override IActionResult SubmitForm(Role moduleEntity)
        {
            moduleEntity.Category = 2;
            return base.SubmitForm(moduleEntity);
        }
    }
}
