﻿using System;
using System.Collections.Generic;
using System.Linq;

using Agile.Service.Rbac;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers
{
    [Area("rbac")]
    public class OrganizeController : BaseController<Organize>
    {
        private readonly IOrganizeService _organizeService;
        public OrganizeController(IOrganizeService organizeService)
        {
            _organizeService = organizeService;
        }
        [HttpGet]
        public IActionResult GetTreeSelectJson()
        {
            var data = _organizeService.FindAll();
            var treeList = new List<TreeSelectModel>();
            foreach (Organize item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.ID;
                treeModel.text = item.Name;
                treeModel.parentId = item.ParentID;
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        [HttpGet]
        public override IActionResult GetTreeJson()
        {
            var data = _organizeService.FindAll().ToList();
            var treeList = new List<TreeViewModel>();
            for (int i = 0; i < data.Count; i++)
            {
                Organize item = data[i];
                TreeViewModel tree = new TreeViewModel();
                bool hasChildren = data.Count(t => t.ParentID == item.ID) == 0 ? false : true;
                tree.id = item.ID;
                tree.text = item.Name;
                tree.value = item.ID;
                tree.parentId = item.ParentID;
                tree.isexpand = i <= 1 ? true : false;
                tree.complete = true;
                tree.checkstate = i == 0 ? 1 : 0;
                tree.hasChildren = hasChildren;
                treeList.Add(tree);
            }
            return Content(treeList.TreeViewJson());
        }
    }
}