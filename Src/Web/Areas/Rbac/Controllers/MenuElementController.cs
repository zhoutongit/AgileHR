﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Rbac.Controllers
{
    [Area("rbac")]
    public class MenuElementController : BaseController<MenuElement>
    {
        private readonly IMenuElementService _menuelementService;
        public MenuElementController(IMenuElementService menuelementService)
        {
            _menuelementService = menuelementService;
        }

        public IActionResult GetListByModuleId(string moduleId)
        {
            var data = _menuelementService.GetListByModuleId(moduleId);
            return Content(data.ToJson());
        }

        public IActionResult CloneButton(string moduleId)
        {
            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Add", Icon= "fa fa-plus", Name = "新建", Script = "btn_add()", OrderBy = 1 });
            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Delete", Icon = "fa fa-trash-o", Name = "删除", Script = "btn_delete()", OrderBy = 2 });
            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Edit", Icon = "fa fa-pencil-square-o", Name = "修改", Script = "btn_edit()", OrderBy = 3 });
            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Details", Icon = "fa fa-search-plus", Name = "查看", Script = "btn_details()", OrderBy = 4 });

            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Imort", Icon = "glyphicon glyphicon-open", Name = "导入", Script = "btn_Imort()", OrderBy = 5 });
            _menuelementService.AddOrUpdate(new MenuElement { ModuleId = moduleId, DomId = "NF-Export", Icon = "fa fa-file-excel-o", Name = "导出", Script = "btn_Export()", OrderBy = 6 });

            return Success("操作成功");
        }
    }
}