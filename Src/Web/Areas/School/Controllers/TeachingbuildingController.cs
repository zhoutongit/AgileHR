﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using Agile.Service.School;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Agile.Controllers
{
    ///<summary>
    ///教学楼
    ///</summary>
    [Area("School")]
    public class TeachingBuildingController : BaseController<TeachingBuilding>
    {
        private readonly ISchoolteachingbuildingService _teachingbuildingService;
        public TeachingBuildingController(ISchoolteachingbuildingService teachingbuildingService)
        {
            _teachingbuildingService = teachingbuildingService;
        }

    }
}