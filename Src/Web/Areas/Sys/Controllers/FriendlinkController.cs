﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Agile.Service;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///友情链接表
    ///</summary>
    [Area("Sys")]
    public class FriendlinkController : BaseController<Friendlink>
    {
        private readonly IFriendlinkService _friendlinkService;
        public FriendlinkController(IFriendlinkService friendlinkService)
        {
            _friendlinkService = friendlinkService;
        }

    }
}
