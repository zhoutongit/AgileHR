﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

using Agile.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///文件
    ///</summary>
    [Area("Sys")]
    public class UploadFileController : BaseController<UploadFile>
    {
        private readonly IUploadFileService _uploadfileService;
        public UploadFileController(IUploadFileService uploadfileService)
        {
            _uploadfileService = uploadfileService;
        }

        /// <summary>
        ///  批量上传文件接口
        /// </summary>
        /// <param name="files"></param>
        /// <returns>服务器存储的文件信息</returns>
        [HttpPost]
        public Result<IList<UploadFile>> Upload(IFormFileCollection files)
        {
            var result = new Result<IList<UploadFile>>();
            try
            {
                result.Data = _uploadfileService.BatchAddFile(files);
                result.Message = "批量上传文件成功";
                result.StatusCode = 200;
            }
            catch (Exception ex)
            {
                result.StatusCode = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }
    }
}
