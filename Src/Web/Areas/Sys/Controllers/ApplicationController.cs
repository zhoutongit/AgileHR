﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Agile.Service;

namespace Web.Areas.Sys.Controllers {
    ///<summary>
    ///应用
    ///</summary>
    [Area("Sys")]
    public class ApplicationController:BaseController<Application> {
        private readonly IApplicationService _applicationService;
        public ApplicationController(IApplicationService applicationService) {
            _applicationService = applicationService;
        }
        [HttpPost]
        public IActionResult SaveApplication(ApplicationReq applicationReq) {
            if(applicationReq == null) return Error("提交失败！");
            Result result = _applicationService.SaveApplication(applicationReq);
            return Content(result.ToJson());
        }
        public IActionResult GetMenusTree(string appId) {
            var treeList = _applicationService.GetMenuElementTree(appId);
            return Content(treeList.zTreeViewJson());
        }
        public IActionResult GetBranTree(string appId) {
            var treeList = _applicationService.GetBranTree(appId);
            return Content(treeList.zTreeViewJson());
        }
    }
}
