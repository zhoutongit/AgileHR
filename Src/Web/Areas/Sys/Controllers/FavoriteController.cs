﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Agile.Service;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///收藏表
    ///</summary>
    [Area("Sys")]
    public class FavoriteController : BaseController<Favorite>
    {
        private readonly IFavoriteService _favoriteService;
        public FavoriteController(IFavoriteService favoriteService)
        {
            _favoriteService = favoriteService;
        }

    }
}
