﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Agile.Service;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///通知公告
    ///</summary>
    [Area("Sys")]
    public class NoticemodController: BaseController<Noticemod>
    {
        private readonly INoticemodService _noticemodService;
        public NoticemodController(INoticemodService noticemodService) {
            _noticemodService = noticemodService;
        }

    }
}
