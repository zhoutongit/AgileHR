﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///系统日志
    ///</summary>
    [Area("Sys")]
    public class SysLogController : BaseController<SysLog>
    {
        private readonly ISysLogService _syslogService;
        public SysLogController(ISysLogService syslogService)
        {
            _syslogService = syslogService;
        }

    }
}
