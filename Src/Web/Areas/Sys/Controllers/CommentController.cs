﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Agile.Service;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///评论
    ///</summary>
    [Area("Sys")]
    public class CommentController : BaseController<Comment>
    {
        private readonly ICommentService _commentService;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

    }
}
