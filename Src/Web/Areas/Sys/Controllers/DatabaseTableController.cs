﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    /// <summary>
    /// 描 述：数据表管理
    /// </summary>
    [Area("Sys")]
    public class DatabaseTableController : BaseController
    {
        public DatabaseTableController()
        {

        }
        public IActionResult GetList(string tableName)
        {
            var list = DbFactory.DbHelper().GetTables();
            if (!string.IsNullOrEmpty(tableName))
            {
                list = list.FindAll(t => t.TableName == tableName);
            }
            return Content(list.ToJson());
        }
        public IActionResult GetFieldList(string tableName)
        {
            var table = DbFactory.DbHelper().GetTableColumns(tableName);
            return Content(table.ToJson());
        }
    }
}
