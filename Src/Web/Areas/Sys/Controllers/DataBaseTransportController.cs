﻿using System;
using System.Data;
using Agile.Service;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers {
    /// <summary>
    /// 数据库同步
    /// </summary>
    [Area("Sys")]
    public class DataBaseTransportController:Controller {
        public IActionResult Index() {
            return View();
        }
        public IActionResult GetDataBase(string server,string username,string password) {
            string conn = DataBaseTransportDAL.GetConnString(server,username,password,"master");
            DataTable data = new DataBaseTransportDAL().GetDataBase(conn);
            return Json(data.ToObject<TreeSelectModel>());
        }
        public IActionResult GetTable(string server,string username,string password,string database) {
            string conn = DataBaseTransportDAL.GetConnString(server,username,password,database);
            DataTable data = new DataBaseTransportDAL().GetTableList(conn);
            return Json(data.ToObject<TreeSelectModel>());
        }
        public IActionResult GetColumn(string server,string username,string password,string database,string table) {
            string conn = DataBaseTransportDAL.GetConnString(server,username,password,database);
            DataTable data = new DataBaseTransportDAL().GetTerm(conn,table);
            return Json(data.ToObject<TreeSelectModel>());
        }
        public IActionResult InsertInto(string sql,string server,string username,string password,string database) {
            string conn = DataBaseTransportDAL.GetConnString(server,username,password,database);
            int flag = new DataBaseTransportDAL().GetExcuteNonQuery(sql,conn);
            return Json(new { flag });
        }
        public IActionResult UpdateTable(string sql,string server,string username,string password,string database) {
            string conn = DataBaseTransportDAL.GetConnString(server,username,password,database);
            int flag = new DataBaseTransportDAL().GetExcuteNonQuery(sql,conn);
            return Json(new { flag });
        }
    }
}