﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.HealthChecks;
using Newtonsoft.Json;

namespace Web.Areas.Sys.Controllers
{
    /// <summary>
    /// 健康检查控制器
    /// </summary>
    [Area("Sys")]
    public class HealthsController: Controller
    {
        private readonly IHealthCheckService _healthCheck;
        public HealthsController(IHealthCheckService healthCheck)
        {
            _healthCheck = healthCheck;
        }
        public async Task<IActionResult> Index()
        {
            string result;
            if (await _healthCheck.CheckHealthAsync())
            {
                result = "healthy";
            }
            else
            {
                result = "unhealthy!";
            }
            ViewData["AppStatus"] = result;

            return View();
        }
        public IActionResult GetGridJson()
        {
            return Content(_healthCheck.CheckResults.CheckResults.ToJson());
        }
    }
}
