﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    /// <summary>
    /// 字体图标查看
    /// </summary>
    [Area("Sys")]
    public class IconController : BaseController
    {
        #region 视图功能
        /// <summary>
        /// 手机图标查看
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AppIndex()
        {
            return View();
        }
        #endregion
    }
}
