﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Agile.Service;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Sys.Controllers
{
    ///<summary>
    ///区域
    ///</summary>
    [Area("Sys")]
    public class AreaController : BaseController<Area>
    {
        private readonly IAreaService _areaService;
        public AreaController(IAreaService areaService)
        {
            _areaService = areaService;
        }

    }
}
