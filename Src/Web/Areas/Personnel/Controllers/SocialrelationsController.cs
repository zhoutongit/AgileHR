﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Agile.Service.Personnel;

namespace Web.Areas.Agile.Controllers
{
    ///<summary>
    ///家庭成员
    ///</summary>
    [Area("Personnel")]
    public class SocialrelationsController : BaseController<Socialrelations>
    {
        private readonly ISocialrelationsService _socialrelationsService;
        public SocialrelationsController(ISocialrelationsService socialrelationsService)
        {
            _socialrelationsService = socialrelationsService;
        }

    }
}