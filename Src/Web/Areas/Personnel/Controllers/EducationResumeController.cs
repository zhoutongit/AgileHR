﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using Agile.Service.Personnel;

namespace Web.Areas.Personnel.Controllers
{
    ///<summary>
    ///教育经历
    ///</summary>
    [Area("Personnel")]
    public class EducationResumeController : BaseController<EducationResume>
    {
        private readonly IEducationResumeService _EducationResumeService;
        public EducationResumeController(IEducationResumeService EducationResumeService)
        {
            _EducationResumeService = EducationResumeService;
        }

    }
}