﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Agile.Service.Flow;
using System.Text;
using Agile.Service.Rbac;

namespace Web.Areas.Flow.Controllers {
    ///<summary>
    ///模型管理
    ///</summary>
    [Area("flow")]
    public class DivModelController:BaseController<DivModel> {
        private readonly IDivModelService _divmodelService;
        private readonly IMenuService _menuService;
        private readonly IMenuElementService _menuElementService;
        public DivModelController(IDivModelService divmodelService,
            IMenuService menuService,
            IMenuElementService menuElementService) {
            _divmodelService = divmodelService;
            _menuService = menuService;
            _menuElementService = menuElementService;
        }
        [HttpGet]
        public IActionResult GetDivModelTreeJson() {
            var data = _divmodelService.FindAll("ParentID='" + Guid.Empty.ToString() + "'").ToList();
            var treeList = new List<TreeViewModel>();
            for(int i = 0;i < data.Count;i++) {
                DivModel item = data[i];
                //bool hasChildren = _divmodelService.FindAll("ParentID='" + item.ID + "'").Count == 0 ? false : true;
                TreeViewModel treeModel = new TreeViewModel();
                treeModel.id = item.ID;
                treeModel.text = item.Name;
                treeModel.parentId = item.ParentID;
                treeModel.isexpand = true;
                treeModel.complete = true;
                treeModel.checkstate = i == 1 ? 1 : 0;
                treeModel.hasChildren = false;
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeViewJson());
        }
        [HttpGet]
        [HttpPost]
        public virtual IActionResult GetDivModelSubGridJson(string parentID,string keyword,string field = "*",string sidx = "OrderBy",string sord = "desc",int page = 1,int rows = 15) {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" 1=1 ");
            if(!string.IsNullOrWhiteSpace(parentID)) {
                strWhere.AppendLine(" AND (ParentID='" + parentID + "' OR ID='"+ parentID + "') ");
            }
            if(!string.IsNullOrWhiteSpace(keyword)) {
                strWhere.AppendLine(" AND (Name like'%" + keyword + "%') ");
            }
            Paging paging = new Paging();
            paging.field = field;
            paging.sidx = sidx;
            paging.sord = sord;
            paging.page = page;
            paging.rows = rows;
            paging.sqlwhere = strWhere.ToString();
            JQridPageData pageData = Repository.FindAll(paging);
            return Content(pageData.ToJson());
        }
        [HttpPost]
        public override IActionResult SubmitForm(DivModel divModel) {
            if(!string.IsNullOrWhiteSpace(divModel.ModuleId)) {
                var menu = new Menu();
                menu.ParentID = divModel.ModuleId;
                menu.Name = divModel.Name;
                menu.Code = Pinyin.GetInitials(divModel.Name.Trim());
                menu.Url = "/flow/DivModelInstances/DivModelDataIndex?divModelID=" + divModel.ID;
                menu.IsSys = false;
                menu.Icon = "fa fa-file-text-o";
                var model = _divmodelService.Find(divModel.ID);
                if(model != null && !string.IsNullOrWhiteSpace(model.ModuleId))
                    _menuService.DeleteAll(model.ModuleId);
                _menuService.Add(menu);
                _menuElementService.Add(AddBut(menu.ID));
                divModel.ModuleId = menu.ID;
            }
            _divmodelService.AddOrUpdate(divModel);
            return Success();
        }
        public List<MenuElement> AddBut(string menuID) {
            List<MenuElement> moduleButtonList = new List<MenuElement>();
            MenuElement addButtonEntity = new MenuElement();
            addButtonEntity.ModuleId = menuID;
            addButtonEntity.DomId = "NF-add";
            addButtonEntity.Name = "新增";
            moduleButtonList.Add(addButtonEntity);
            MenuElement editButtonEntity = new MenuElement();
            editButtonEntity.ModuleId = menuID;
            editButtonEntity.DomId = "NF-edit";
            editButtonEntity.Name = "编辑";
            moduleButtonList.Add(editButtonEntity);
            MenuElement deleteButtonEntity = new MenuElement();
            deleteButtonEntity.ModuleId = menuID;
            deleteButtonEntity.DomId = "NF-delete";
            deleteButtonEntity.Name = "删除";
            moduleButtonList.Add(deleteButtonEntity);
            return moduleButtonList;
        }
        public override IActionResult DeleteForm(string keyValue) {
            if(string.IsNullOrWhiteSpace(keyValue))
                return Error("参数keyValue不能为空！");
            _divmodelService.DeleteAll(keyValue);
            return Success("删除成功。");
        }

        public IActionResult GetSelectForm() {
            var data = Repository.FindAll("ModuleId IS NOT NULL");
            var treeList = new List<TreeSelectModel>();
            treeList.Add(new TreeSelectModel { id = "",text = "==请选择==",parentId = Guid.Empty.ToString(),data = "" });
            foreach(DivModel item in data) {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.ID;
                treeModel.text = item.Name;
                treeModel.parentId = Guid.Empty.ToString();
                treeModel.data = item;
                treeList.Add(treeModel);
            }
            return Json(treeList);
        }
    }
}