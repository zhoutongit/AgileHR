﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

using Agile.Service.Flow;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Flow.Controllers {
    ///<summary>
    ///模型实例
    ///</summary>
    [Area("flow")]
    public class DivModelInstancesController:BaseController {
        private readonly IDivModelService _divmodelService;
        private readonly IDivModelfieldService _divModelfieldService;
        public DivModelInstancesController(IDivModelService divmodelService,
            IDivModelfieldService divModelfieldService) {
            _divmodelService = divmodelService;
            _divModelfieldService = divModelfieldService;
        }
        public List<DivModelfield> GetDivModelfields(string divModelID,string keyword = "") {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" 1=1 ");
            if(!string.IsNullOrWhiteSpace(divModelID)) {
                strWhere.AppendLine(" AND DivModelID='" + divModelID + "' ");
            }
            if(!string.IsNullOrWhiteSpace(keyword)) {
                strWhere.AppendLine(" AND (Name like'%" + keyword + "%') ");
            }
            var dmfList = _divModelfieldService.FindAll(strWhere.ToString()).ToList();
            return dmfList;
        }
        public IActionResult DivModelDataIndex(string divModelID,string keyword = "") {
            var dmfList = GetDivModelfields(divModelID,keyword);
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("[");
            foreach(DivModelfield dmf in dmfList) {
                builder.AppendLine("{ label: '" + dmf.Name + "', name: '" + dmf.Field + "', align: 'left' },");
            }
            builder.AppendLine("]");
            ViewBag.Girdhead = builder.ToString();
            return View();
        }
        public IActionResult DivModelForm(string divModelID,string keyword = "") {
            var dmfList = GetDivModelfields(divModelID,keyword);
            ViewBag.divModelID = divModelID;
            ViewBag.keyword = keyword;
            return View(dmfList);
        }
        public IActionResult GetGridJson(string divModelID,string keyword,string sidx,string sord,int page = 1,int rows = 15) {


            return Content("");
        }
    }
}
