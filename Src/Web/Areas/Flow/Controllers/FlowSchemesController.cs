﻿using System;
using System.Collections.Generic;
using System.Linq;
using Agile.Service;
using Agile.Service.Flow;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Flow.Controllers
{
    [Area("flow")]
    public class FlowSchemesController : BaseController<FlowScheme>
    {
        private readonly IFlowSchemeService _flowSchemeService;
        public FlowSchemesController(IFlowSchemeService flowSchemeService)
        {
            _flowSchemeService = flowSchemeService;
        }

        #region 视图功能
        public IActionResult Design()
        {
            return View();
        }
        public IActionResult Preview()
        {
            return View();
        }
        public IActionResult NodeInfo()
        {
            return View();
        }
        #endregion

        #region 数据处理 GetGridPageJson
        [HttpPost]
        public override IActionResult SubmitForm(FlowScheme moduleEntity)
        {
            return base.SubmitForm(moduleEntity);
        }
        #endregion
    }
}
