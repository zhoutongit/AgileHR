﻿using System;
using System.Linq;

using Agile.Service;
using Agile.Service.Flow;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Flow.Controllers
{
    ///<summary>
    ///工作流流程实例表
    ///</summary>
    [Area("flow")]
    public class FlowInstancesController : BaseController<FlowInstance>
    {
        private readonly IFlowInstanceService _flowinstanceService;
        public FlowInstancesController(IFlowInstanceService flowinstanceService)
        {
            _flowinstanceService = flowinstanceService;
        }

        #region 视图功能
        /// <summary>
        /// 待处理的流程
        /// </summary>
        public IActionResult Wait()
        {
            return View();
        }
        /// <summary>
        /// 已完成的流程
        /// </summary>
        public IActionResult Disposed()
        {
            return View();
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit()
        {
            return View();
        }
        public IActionResult Verification()
        {
            return View();
        }     
        #endregion

        public IActionResult FindAll([FromQuery] QueryFlowInstanceListReq request)
        {
            request.userid = loginInfo.AccountId;
           var list=  _flowinstanceService.FindAll(request);
            return Content(list.ToJson());
        }
        public IActionResult FindAll(string keyValue)
        {
            try
            {
                var flowinstance = _flowinstanceService.FindAll(p => p.ID == keyValue).ToList();
                var result = flowinstance.MapTo<FlowVerificationResp>();
                return Success("获取成功", result);
            }
            catch (Exception ex)
            {
                return Error(ex.InnerException?.Message ?? ex.Message);
            }
        }
        //操作历史
        public IActionResult QueryHistories(string flowInstanceId)
        {
            try
            {
                var result= _flowinstanceService.QueryHistories(flowInstanceId);
                return Success("获取一个流程实例的操作历史记录", result);
            }
            catch (Exception ex)
            {
                return Error(ex.InnerException?.Message ?? ex.Message);
            }
        }
        /// <summary>
        /// 审批流程
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Verification(VerificationReq request)
        {
            try
            {
                _flowinstanceService.Verification(request);
                return Success();
            }
            catch (Exception ex)
            {
                return Error(ex.InnerException?.Message ?? ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Delete(string[] ids)
        {
            try
            {
                string idi = string.Join(",", ids);
                _flowinstanceService.Delete(idi);
                return Success();
            }
            catch (Exception ex)
            {
                return Error(ex.InnerException?.Message ?? ex.Message);
            }
        }
    }
}
