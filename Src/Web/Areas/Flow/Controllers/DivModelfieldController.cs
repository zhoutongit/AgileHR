﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Agile.Service.Flow;
using System.Text;

namespace Web.Areas.Flow.Controllers
{
    ///<summary>
    ///模型字段
    ///</summary>
    [Area("flow")]
    public class DivModelfieldController : BaseController<DivModelfield>
    {
        private readonly IDivModelfieldService _divModelfieldService;
        public DivModelfieldController(IDivModelfieldService divModelfieldService)
        {
            _divModelfieldService = divModelfieldService;
        }
        [HttpGet]
        [HttpPost]
        public virtual IActionResult GetDivModelFieldGridJson(string divModelID, string keyword, string field = "*", string sidx = "OrderBy", string sord = "desc", int page = 1, int rows = 15)
        {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(divModelID))
            {
                strWhere.AppendLine(" AND DivModelID='" + divModelID + "' ");
            }
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                strWhere.AppendLine(" AND (Name like'%" + keyword + "%') ");
            }
            Paging paging = new Paging();
            paging.field = field;
            paging.sidx = sidx;
            paging.sord = sord;
            paging.page = page;
            paging.rows = rows;
            paging.sqlwhere = strWhere.ToString();
            JQridPageData pageData = Repository.FindAll(paging);
            return Content(pageData.ToJson());
        }

    }
}