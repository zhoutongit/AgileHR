﻿
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using Agile.Service;
using Agile.Service.Reward;

using Microsoft.AspNetCore.Mvc;
using Agile.Service.Rbac;

namespace Web.Areas.Reward.Controllers
{
    /// <summary>
    /// 工资表
    /// </summary>
    [Area("reward")]
    public class SalaryController : BaseController<Salary>
    {
        private readonly IUserService _userService;
        private readonly ISalaryService _salaryService;
        private readonly ISalaryColumnService _salaryColumnService;
        public SalaryController(IUserService userService, ISalaryService salaryService, ISalaryColumnService salaryColumnService)
        {
            _userService = userService;
            _salaryService = salaryService;
            _salaryColumnService = salaryColumnService;
        }
        public override IActionResult Index()
        {
            var list = _salaryColumnService.FindAll(" IfType=1 ");
            return View(list);
        }
        public IActionResult GetDateSelectJson(string type)
        {
            List<KeyValue> data = new List<KeyValue>();
            if (type == "year")
            {
                int year = DateTime.Now.Year;
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year - 3), Value = Utils.GetObjTranNull<string>(year - 3) });
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year - 2), Value = Utils.GetObjTranNull<string>(year - 2) });
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year - 1), Value = Utils.GetObjTranNull<string>(year - 1) });
                data.Add(new KeyValue { Key = year.ToString(), Value = year.ToString() });
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year + 1), Value = Utils.GetObjTranNull<string>(year + 1) });
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year + 2), Value = Utils.GetObjTranNull<string>(year + 2) });
                data.Add(new KeyValue { Key = Utils.GetObjTranNull<string>(year + 3), Value = Utils.GetObjTranNull<string>(year + 3) });
            }
            else if (type == "month")
            {
                for (int i = 1; i < 13; i++)
                {
                    string value = i.ToString();
                    if (i < 10) value = "0" + i.ToString();
                    data.Add(new KeyValue { Key = value, Value = value });
                }
            }
            var treeList = new List<TreeSelectModel>();
            foreach (KeyValue item in data)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.Value.ToString();
                treeModel.text = item.Key;
                treeModel.parentId = Guid.Empty.ToString();
                treeList.Add(treeModel);
            }
            return Content(treeList.TreeSelectJson());
        }
        public IActionResult GetTableJson(string yemo, string wageType)
        {
            var list = _salaryColumnService.FindAll(p => p.ParentID == wageType).Select(p => p.Name).ToArray();
            return Json(list.ToSql(true));
        }
        public IActionResult GetSalaryTreeGridJson(string keyword, string yemo, string orgId, string wageType,
            string sidx = "OrderBy", string sord = "desc", int page = 1, int rows = 15)
        {
            var table = _salaryService.GetPayroll(keyword, orgId, yemo, wageType);
            string retJson = Paging.GetjqGridJson(table.ToStrJson(), 1, table.Rows.Count);
            return Content(retJson);
        }
        //生成工资条
        [HttpPost]
        public IActionResult GenerateSalary(string yemo, string wageType)
        {
            _salaryService.GenerateSalary(yemo, wageType);
            return Success();
        }
    }
}
