﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Agile.Service;
using Agile.Service.Reward;

using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Reward.Controllers
{
    /// <summary>
    /// 工资公式
    /// </summary>
    [Area("reward")]
    public class SalaryColumnController : BaseController<SalaryColumn>
    {
        private readonly ISalaryColumnService _salaryColumnService;
        public SalaryColumnController(ISalaryColumnService salaryColumnService)
        {
            _salaryColumnService = salaryColumnService;
        }
        public IActionResult GetSalaryColumnJson()
        {
            var list = _salaryColumnService.FindAll(" IfType=1 ");

            return Content(list.ToJson());
        }
        public IActionResult GetBranchGridJson(string keyword, string treeword, int rows, int page, string sidx, string sord)
        {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" 1=1 and IfType<>1 ");
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                strWhere.Append("and [name] like '%" + keyword.Trim() + "%' ");
            }
            if (!string.IsNullOrWhiteSpace(treeword))
            {
                strWhere.Append("and [ParentID]= '" + treeword.Trim() + "' ");
            }

            Paging paging = new Paging();
            paging.field = "*";
            paging.sidx = sidx;
            paging.sord = sord;
            paging.page = page;
            paging.rows = rows;
            paging.sqlwhere = strWhere.ToString();
            JQridPageData pageData = _salaryColumnService.FindAll(paging);
            return Content(pageData.ToJson());
        }

        #region 工资册
        [HttpGet]
        public IActionResult GzlList()
        {
            return View();
        }
        [HttpPost]
        public IActionResult GzlGridJson(Paging paging)
        {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" 1=1 and IfType=1 ");
            if (!string.IsNullOrWhiteSpace(paging.keyword))
            {
                strWhere.Append("and [name] like '%" + paging.keyword.Trim() + "%' ");
            }
            paging.field = "*";
            paging.sqlwhere = strWhere.ToString();
            JQridPageData pageData = _salaryColumnService.FindAll(paging);
            return Content(pageData.ToJson());
        }
        #endregion

    }
}
