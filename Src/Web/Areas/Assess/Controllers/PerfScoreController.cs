﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using Agile.Service.Assess;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Assess.Controllers {
    ///<summary>
    ///考核打分
    ///</summary>
    [Area("Assess")]
    public class PerfScoreController:BaseController<PerfScore> {
        private readonly IPerfScoreService _PerfScoreService;
        public PerfScoreController(IPerfScoreService PerfScoreService) {
            _PerfScoreService = PerfScoreService;
        }

    }
}
