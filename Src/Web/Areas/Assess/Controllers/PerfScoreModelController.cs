﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using Agile.Service.Assess;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Assess.Controllers {
    ///<summary>
    ///打分方式
    ///</summary>
    [Area("Assess")]
    public class PerfScoreModelController:BaseController<PerfScoreModel> {
        private readonly IPerfScoreModelService _PerfScoreModelService;
        public PerfScoreModelController(IPerfScoreModelService PerfScoreModelService) {
            _PerfScoreModelService = PerfScoreModelService;
        }

    }
}
