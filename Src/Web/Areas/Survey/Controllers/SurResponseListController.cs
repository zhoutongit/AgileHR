﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using Agile.Service.Survey;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Survey.Controllers {
    ///<summary>
    ///用户问卷情况
    ///</summary>
    [Area("Survey")]
    public class SurResponseListController : BaseController<SurResponseList>
    {
        private readonly ISurResponseListService _SurResponseListService;
        public SurResponseListController(ISurResponseListService SurResponseListService)
        {
            _SurResponseListService = SurResponseListService;
        }
      
    }
}
