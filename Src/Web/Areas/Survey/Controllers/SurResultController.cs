﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using Agile.Service.Survey;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Assess.Controllers {
    ///<summary>
    ///调查结果
    ///</summary>
    [Area("Survey")]
    public class SurResultController : BaseController<SurResult>
    {
        private readonly ISurResultService _SurResultService;
        public SurResultController(ISurResultService SurResultService)
        {
            _SurResultService = SurResultService;
        }
      
    }
}
