﻿//no-tabs solution
$(document).ready(function () {
    $(".panel.collapsible-panel >.panel-heading").click(WrapAndSaveBlockData);
});
function WrapAndSaveBlockData() {
    $(this).parents(".panel").find(">.panel-container").slideToggle();
    $("#ajaxBusy span").addClass("no-ajax-loader");
    var icon = $(this).find("i.toggle-icon");
    if ($(this).hasClass("opened")) {
        icon.removeClass("fa-minus");
        icon.addClass("fa-plus");
    } else {
        icon.addClass("fa-minus");
        icon.removeClass("fa-plus");
    }
    $(this).toggleClass("opened");
}

//index头部查询区域高度
function seachHeight() {
    var winH = $(window).height();
    $("#requireHide").click(function () {
        var requireH = $(".search-require").height();
        var contentH = $(".content-zl").height();
        $(".search-require").slideToggle(0);
        $(this).toggleClass("glyphicon-chevron-down");
        if ($(".search-require").css("display") == "none") {
            var contentH2 = contentH + requireH;
            $(".content-zl").height(contentH2);
            $("#gridList").setGridHeight(contentH2 - 130);
        } else {
            var contentH3 = contentH - requireH;
            $(".content-zl").height(contentH3);
            $("#gridList").setGridHeight(contentH3 - 130);
        }
    });

    //var winHeight = $(window).height();
    var winWidth = $(window).width();
    if (winWidth > 1050) {
        $(".search-require>.container-fluid>.row>div").removeClass("col-md-6");
        $(".search-require>.container-fluid>.row>div").addClass("col-md-4");
    } else if (900 < winWidth < 1050) {
        $(".search-require>.container-fluid>.row>div").removeClass("col-md-4");
        $(".search-require>.container-fluid>.row>div").addClass("col-md-6");
    }


    var searchH = $(".search-area").height();
    $(".content-zl").height(winH - searchH - 35);

}
/**
 * 弹出外键对应的数据字典表格
 * @param mID 返回值绑定的VUE控件
 * @param mName 返回名称的VUE控件
 * @param tableName 数据字典对应的数据库表
 *  * @param params [可选] 对象参数包括
 *  	[可选] 返回值选取的字段，参数设置{fields:{ID:'UID',NAME:'UNAME', IDTITLE:'ID', NAMETITLE:'名称'}} 默认{fields:{ID:'ID',NAME:'NAME'}}
 *  	[可选] 单选or多选，参数设置{radio:true} or {checkbox:true} 默认单选
 *  	[可选] 搜索条件 conditions  {radio:true,conditions:{FID:1,FNAME:'新闻公告'}}
 *  			使用whereStr直接拼接SQL查询条件，将不再接受其他条件 如{conditions:{whereStr:' 1=1 and (UNAME like \'%'+localStorage.UN+'%\''+' or GROUP_NAMES like \'%'+localStorage.UN+'%\')'}}
 *  	[可选] 直接使用sql语句进行查询
 *  			selectTable('stu_exam_place', 'stu_exam_place', 'DD_GROUP', {checkbox:true, search:'exam_place',  fields:{ID:'exam_place', NAME:'exam_place', NAMETITLE:'考场'}, sql: 'select distinct exam_place from dd_candidate'})
 *  	[可选] 其他表格要显示的字段 columns {radio:true,conditions:{FID:1,FNAME:'新闻公告'},columns:{FNAME:'父分类名称'}}
 *  	[可选] 窗口标题 title {radio:true,conditions:{FID:1,FNAME:'新闻公告'},columns:{FNAME:'父分类名称'},title:'操作'}
 *  	[可选] 搜索框搜索字段 { search: 'NAME'}
 *  	[可选] 排序 {  sort:'grade', order:'asc' }
 *  	[可选] 分页条数 {  pageSize:10 }
 *  	[可选] 多行的index { rowIndex: 'index'}
 *     
<div class="input-group" style="width:99%;">
    <input type="hidden" id="p_lxbmnum" name="p_lxbmnum" autocomplete="off" class="form-control">
    <input type="text" id="p_lxbm" name="p_lxbm" class="form-control">
    <div class="input-group-btn">
        <a href="#" onclick="selectTable('p_lxbmnum', 'p_lxbm', 'Base_Branch', { conditions: {whereStr:'Bran_name=\'校领导\''}, search:'Bran_name',columns: { Bran_number:'ID',Bran_name: '名称' },fields:{Bran_number:'Bran_number',Bran_name:'Bran_name'}});" class="btn btn-default" style="height:35px;">
            <i class="glyphicon glyphicon-chevron-down"></i>
        </a>
    </div>
</div>

 */
function selectTable(mID, mName, tableName, params) {
    if (!params) params = {}
    var tableColumns = [];
    if (params.columns) {
        for (var key in params.columns) {
            tableColumns.push({ field: key, title: params.columns[key], sortable: true });
        }
    }
    var fields = params.fields || { ID: 'ID', NAME: 'NAME' };
    var showID = params.showID || 'N';
    if (showID == 'Y') {
        tableColumns.splice(0, 0, { field: fields.ID, title: fields.IDTITLE || 'ID' });
    }
    var radioOrcheckbox = { radio: true };
    if (params.checkbox) radioOrcheckbox = { checkbox: true };
    tableColumns.splice(0, 0, radioOrcheckbox);
    var conditions = JSON.stringify(params.conditions || { 1: '1' });
    sessionStorage.tableParams = JSON.stringify({
        mID: mID,
        mName: mName,
        search: params.search || 'NAME',
        tableName: tableName,
        fields: fields,
        tableColumns: tableColumns,
        conditions: conditions,
        sql: params.sql,//JSON.stringify( || ''),
        showID: showID,
        sort: params.sort || 'ID',
        order: params.order || 'asc',
        pageSize: params.pageSize || 10,
        rowIndex: params.rowIndex
    });

    var height = params.issm ? ' - 280px' : ' - 80px';
    params.issm = params.issm ? '' : 'modal-dialog-big';
    params.title = params.title || '操作';

    var str = '<div class="modal fade" id="tempSelectTableDiv">   <div class="modal-dialog ' + params.issm + '">       <div class="modal-content">        <div class="modal-header bg-primary" style="padding:10px;">        <button type="button" class="close" data-dismiss="modal" aria-label="Close">        <span aria-hidden="true"style="font-size: 20px;">×</span>    </button>    <h5 class="modal-title"><i class="fa fa-th-large fa-fw"></i> ' + params.title + '</h5>        </div>        <div class="modal-body">        <iframe src="/selectTable/selectTable.html" frameborder="0" style="width:100%; height:calc(100vh' + height + ');">        </div>        </div>        </div>        </div>';
    $('body').append(str);
    $('#tempSelectTableDiv').on('hidden.bs.modal', function () { $(this).remove(); }).modal('show');

}
