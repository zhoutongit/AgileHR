﻿window.console = window.console || (function () {
    var c = {};
    c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () { };
    return c;
})();
var storage, fail, uid; try { uid = new Date; (storage = window.localStorage).setItem(uid, uid); fail = storage.getItem(uid) != uid; storage.removeItem(uid); fail && (storage = false); } catch (e) { }

$(function () {
    $("[data-toggle='tooltip']").tooltip();
    if (storage) {
        var usedSkin = localStorage.getItem('config-skin');
        if (usedSkin != '' && usedSkin != null) {
            document.body.className = usedSkin;
        } else {
            document.body.className = 'theme-whbl';
            localStorage.setItem('config-skin', "theme-whbl");
        }
    } else {
        document.body.className = 'theme-whbl';
    }
    $("#modalBody ul li").each(function () {
        $("#modalBody ul li").click(function () {
            $("#modalBody ul li").css("border", "1px solid #999");
            $(this).css("border", "2px solid #444");
            var color = $(this).children("i").text();
            var layout = $(this).children("e").text();
            if (color == "") color = parent.document.body.className;
            localStorage.setItem("config-skin", color);
            $.cookie('page_layout', layout, { expires:7,path:'/'});
            parent.document.body.className = color;
            document.body.className = color;
        })
    })
});
function refreshView() {
    $.reload();
}
$.reload = function () {
    location.reload();
    return false;
}
$.loading = function (bool, text) {
    var $loadingpage = top.$("#loadingPage");
    var $loadingtext = $loadingpage.find('.loading-content');
    if (bool) {
        $loadingpage.show();
    } else {
        if ($loadingtext.attr('istableloading') == undefined) {
            $loadingpage.hide();
        }
    }
    if (!!text) {
        $loadingtext.html(text);
    } else {
        $loadingtext.html("数据加载中，请稍后…");
    }
    $loadingtext.css("left", (top.$('body').width() - $loadingtext.width()) / 2 - 50);
    $loadingtext.css("top", (top.$('body').height() - $loadingtext.height()) / 2);
}
$.request = function (name) {
    var search = location.search.slice(1);
    var arr = search.split("&");
    for (var i = 0; i < arr.length; i++) {
        var ar = arr[i].split("=");
        if (ar[0] == name) {
            if (unescape(ar[1]) == 'undefined') {
                return "";
            } else {
                return unescape(ar[1]);
            }
        }
    }
    return "";
}
$.currentWindow = function () {
    var iframeId = top.$(".NFine_iframe:visible").attr("id");
    return top.frames[iframeId];
}
$.browser = function () {
    var userAgent = navigator.userAgent;
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (isOpera) {
        return "Opera"
    };
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    }
    if (userAgent.indexOf("Chrome") > -1) {
        if (window.navigator.webkitPersistentStorage.toString().indexOf('DeprecatedStorageQuota') > -1) {
            return "Chrome";
        } else {
            return "360";
        }
    }
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    }
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    };
}
$.download = function (url, data, method) {
    if (url && data) {
        data = typeof data == 'string' ? data : jQuery.param(data);
        var inputs = '';
        $.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        $('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
    };
};
$.modalOpen = function (options) {
    var defaults = {
        id: null,
        title: '系统窗口',
        width: "100px",
        height: "100px",
        url: '',
        shade: 0.3,
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        callBack: null
    };
    var options = $.extend(defaults, options);
    var _width = top.$(window).width() > parseInt(options.width.replace('px', '')) ? options.width : top.$(window).width() + 'px';
    var _height = top.$(window).height() > parseInt(options.height.replace('px', '')) ? options.height : top.$(window).height() + 'px';
    top.layer.open({
        id: options.id,
        type: 2,
        shade: options.shade,
        title: options.title,
        fix: false,
        area: [_width, _height],
        content: options.url,
        btn: options.btn,
        btnclass: options.btnclass,
        yes: function () {
            options.callBack(options.id)
        }, cancel: function () {
            return true;
        }
    });
}
$.modalConfirm = function (content, callBack) {
    var lock = false; //默认未锁定
    var myconfirm = top.layer.confirm(content, {
        icon: "fa-exclamation-circle",
        title: "系统提示",
        btn: ['确认', '取消'],
        btnclass: ['btn btn-primary', 'btn btn-danger']
    }, function () {
        if (!lock) {
            lock = true; // 锁定
            callBack(true);
        }
        layer.close(myconfirm);
    }, function () {
        callBack(false)
    });
}
$.modalAlert = function (content, type) {
    var icon = "";
    if (type == 'success') {
        icon = "fa-check-circle";
    }
    if (type == 'error') {
        icon = "fa-times-circle";
    }
    if (type == 'warning') {
        icon = "fa-exclamation-circle";
    }
    top.layer.alert(content, {
        icon: icon,
        title: "系统提示",
        btn: ['确认'],
        btnclass: ['btn btn-primary'],
    });
}
$.modalMsg = function (content, type) {
    if (type != undefined) {
        var icon = "";
        if (type == 'success' || type == 'true') {
            icon = "fa-check-circle";
        }
        if (type == 'error' || type == 'false') {
            icon = "fa-times-circle";
        }
        if (type == 'warning') {
            icon = "fa-exclamation-circle";
        }
        top.layer.msg(content, { icon: icon, time: 4000, shift: 5 });
        top.$(".layui-layer-msg").find('i.' + icon).parents('.layui-layer-msg').addClass('layui-layer-msg-' + type);
    } else {
        top.layer.msg(content);
    }
}
$.modalClose = function () {
    var index = top.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
    var $IsdialogClose = top.$("#layui-layer" + index).find('.layui-layer-btn').find("#IsdialogClose");
    var IsClose = $IsdialogClose.is(":checked");
    if ($IsdialogClose.length == 0) {
        IsClose = true;
    }
    if (IsClose) {
        top.layer.close(index);
    } else {
        location.reload();
    }
}
$.submitForm = function (options) {
    var defaults = {
        url: "",
        param: [],
        loading: "正在提交数据...",
        success: null,
        close: true
    };
    var options = $.extend(defaults, options);
    $.loading(true, options.loading);
    window.setTimeout(function () {
        if ($('[name=__RequestVerificationToken]').length > 0) {
            options.param["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
        }
        $.ajax({
            url: options.url,
            data: options.param,
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.Success == true) {
                    options.success(data);
                    $.modalMsg(data.Message, 'success');
                    if (options.close == true) {
                        $.modalClose();
                    }
                } else {
                    $.modalAlert(data.Message, 'error');
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.loading(false);
                $.modalMsg(errorThrown, "error");
            },
            beforeSend: function () {
                $.loading(true, options.loading);
            },
            complete: function () {
                $.loading(false);
            }
        });
    }, 500);
}
$.deleteForm = function (options) {
    var defaults = {
        prompt: "注：您确定要删除该项数据吗？",
        loading: "正在删除数据...",
        url: "",
        param: [],
        success: null,
        close: true
    };
    var options = $.extend(defaults, options);
    if ($('[name=__RequestVerificationToken]').length > 0) {
        options.param["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
    }
    $.modalConfirm(options.prompt, function (r) {
        if (r) {
            $.loading(true, options.loading);
            window.setTimeout(function () {
                $.ajax({
                    url: options.url,
                    data: options.param,
                    type: "post",
                    dataType: "json",
                    success: function (data) {
                        if (data.Success == true) {
                            options.success(data);
                            $.modalMsg(data.Message, 'success');
                        } else {
                            $.modalAlert(data.Message, 'error');
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $.loading(false);
                        $.modalMsg(errorThrown, "error");
                    },
                    beforeSend: function () {
                        $.loading(true, options.loading);
                    },
                    complete: function () {
                        $.loading(false);
                    }
                });
            }, 500);
        }
    });

}
$.exportForm = function (options) {
    var defaults = {
        url: "",
        param: [],
        loading: "正在提交数据...",
        success: null,
        close: true
    };
    var options = $.extend(defaults, options);
    $.loading(true, options.loading);
    window.setTimeout(function () {
        if ($('[name=__RequestVerificationToken]').length > 0) {
            options.param["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
        }
        $("<iframe width='0' height='0' id='YuFrame1' name='YuFrame1'  style='display:none' src='" + options.url + "'></iframe>").prependTo('body');
        $.loading(false);
    }, 500);
}
$.jsonWhere = function (data, action) {
    if (action == null) return;
    var reval = new Array();
    $(data).each(function (i, v) {
        if (action(v)) {
            reval.push(v);
        }
    })
    return reval;
}
$.fn.jqGridRowValue = function () {
    var $grid = $(this);
    var selectedRowIds = $grid.jqGrid("getGridParam", "selarrrow");
    if (selectedRowIds != "") {
        var json = [];
        var len = selectedRowIds.length;
        for (var i = 0; i < len; i++) {
            var rowData = $grid.jqGrid('getRowData', selectedRowIds[i]);
            json.push(rowData);
        }
        return json;
    } else {
        return $grid.jqGrid('getRowData', $grid.jqGrid('getGridParam', 'selrow'));
    }
}
$.fn.formValid = function () {
    return $(this).valid({
        errorPlacement: function (error, element) {
            element.parents('.formValue').addClass('has-error');
            element.parents('.has-error').find('i.error').remove();
            element.parents('.has-error').append('<i class="form-control-feedback fa fa-exclamation-circle error" data-placement="left" data-toggle="tooltip" title="' + error + '"></i>');
            $("[data-toggle='tooltip']").tooltip();
            if (element.parents('.input-group').hasClass('input-group')) {
                element.parents('.has-error').find('i.error').css('right', '33px')
            }
        },
        success: function (element) {
            element.parents('.has-error').find('i.error').remove();
            element.parent().removeClass('has-error');
        }
    });
}
$.fn.formSerialize = function (formdate) {
    var element = $(this);
    if (!!formdate) {
        for (var key in formdate) {
            var $id = element.find('#' + key);
            var value = $.trim(formdate[key]).replace(/&nbsp;/g, '');
            var type = $id.attr('type');
            if ($id.hasClass("select2-hidden-accessible")) {
                type = "select";
            }
            switch (type) {
                case "checkbox":
                    if (value == "true") {
                        $id.attr("checked", 'checked');
                    } else {
                        $id.removeAttr("checked");
                    }
                    break;
                case "select":
                    $id.val(value).trigger("change");
                    break;
                default:
                    $id.val(value);
                    break;
            }

        };
        return false;
    }
    var postdata = {};
    element.find('input,select,textarea').each(function (r) {
        var $this = $(this);
        var id = $this.attr('id');
        var type = $this.attr('type');
        switch (type) {
            case "checkbox":
                postdata[id] = $this.is(":checked");
                break;
            default:
                var value = $this.val() == "" ? "&nbsp;" : $this.val();
                if (!$.request("keyValue")) {
                    value = value.replace(/&nbsp;/g, '');
                }
                postdata[id] = value;
                break;
        }
    });
    if ($('[name=__RequestVerificationToken]').length > 0) {
        postdata["__RequestVerificationToken"] = $('[name=__RequestVerificationToken]').val();
    }
    return postdata;
};
$.fn.bindSelect = function (options) {
    var defaults = {
        id: "id",
        text: "text",
        search: true,
        url: "",
        param: [],
        change: null
    };
    var options = $.extend(defaults, options);
    var $element = $(this);
    if (options.url != "") {
        $.ajax({
            url: options.url,
            data: options.param,
            dataType: "json",
            async: false,
            success: function (data) {
                $.each(data, function (i) {
                    $element.append($("<option></option>").val(data[i][options.id]).html(data[i][options.text]));
                });
                $element.select2({
                    minimumResultsForSearch: options.search == true ? 0 : -1
                });
                $element.on("change", function (e) {
                    if (options.change != null) {
                        options.change(data[$(this).find("option:selected").index()]);
                    }
                    $("#select2-" + $element.attr('id') + "-container").html($(this).find("option:selected").text().replace(/　　/g, ''));
                });
            }
        });
    } else {
        $element.select2({
            minimumResultsForSearch: -1
        });
    }
}
$.fn.authorizeButton = function () {
    var moduleId = top.$(".NFine_iframe:visible").attr("id").substr(6);
    var dataJson = top.clients.authorizeButton[moduleId];
    var userId = top.clients.user.AccountId;
    //console.log(userId);
    if (userId != "admin") {
        var $element = $(this);
        $element.find('a[authorize=yes]').attr('authorize', 'no');
        if (dataJson != undefined) {
            $.each(dataJson, function (i) {
                $element.find("#" + dataJson[i].DomId).attr('authorize', 'yes');
            });
        }
        $element.find("[authorize=no]").parents('li').prev('.split').remove();
        $element.find("[authorize=no]").parents('li').remove();
        $element.find('[authorize=no]').remove();
    }
}
$.fn.dataGrid = function (options) {
    var defaults = {
        datatype: "json",
        mtype: "GET",
        colModel: [],
        colNames: [],
        width: $(window).width(),
        height: $(window).height() - 138,
        rowNum: 20,
        rowList: [20, 50, 100],
        pager: '#gridPager',
        sortname: 'ID',
        viewrecords: true,
        sortorder: "asc",
        autowidth: true,
        rownumbers: true,
        shrinkToFit: false,
        gridview: true
    };
    var options = $.extend(defaults, options);
    var $element = $(this);
    options["onSelectRow"] = function (rowid) {
        var length = $(this).jqGrid("getGridParam", "selrow");
        var $operate = $(".operate");
        if (length != null) {
            if (length.length > 0) {
                $operate.animate({ "left": 0 }, 200);
            } else {
                $operate.animate({ "left": '-100.1%' }, 200);
            }
        } else {
            $operate.animate({ "left": '-100.1%' }, 200);
        }
        $operate.find('.close').click(function () {
            $operate.animate({ "left": '-100.1%' }, 200);
        })
    };
    $element.jqGrid(options);
};

//回车事件控件获取焦点
function funKeyCode(vinput) {
    if (event.keyCode == 13)
        $("#" + vinput).focus();
}
var arry = [];
function btn_tc(gtid) {
    if (gtid == null || gtid == undefined) {
        gtid = "gridList";
    }
    $("#" + gtid + "_frozen tr").each(function (i) {
        $(this).on("mouseenter", function () {
            arry.splice(0, arry.length);
            $("#" + gtid + " .active td").each(function () {
                arry.push($(this).html());
            })
        })
        return arry;
    })
    $("#" + gtid + "_frozen tr td").each(function () {
        $(this).on("mouseenter", function (e) {
            var w = this.offsetWidth;
            var h = this.offsetHeight;
            var toTop = this.getBoundingClientRect().top + document.body.scrollTop;  //兼容滚动条
            var x = (e.pageX - this.getBoundingClientRect().left - (w / 2)) * (w > h ? (h / w) : 1);   //获取当前鼠标的x轴位置
            var y = (e.pageY - toTop - h / 2) * (h > w ? (w / h) : 1);
            var direction = Math.round((((Math.atan2(y, x) * 180 / Math.PI) + 180) / 90) + 3) % 4; //direction的值为“0,1,2,3”分别对应着“上，右，下，左”
            var eventType = e.type;
            var dirName = new Array('上方', '右侧', '下方', '左侧');

            if ($(this).find("a").html() == "编辑") {
                trId = $(this).parent().find("td").eq(1).html();

                var xx = e.originalEvent.x || e.originalEvent.layerX || 0;
                var yy = e.originalEvent.y || e.originalEvent.layerY || 0;
                var mouseX = xx;
                var mouseY = yy;//90

                if (window.ActiveXObject || "ActiveXObject" in window) {
                    var e = event || window.event;
                    xx = e.clientX;
                    yy = e.clientY;
                    mouseY = yy;
                    mouseX = xx;
                }

                var tcHtml = '<div class="tc">' + $(".tc").html() + '</div>';

                $(this).append(tcHtml);
                //$(".tc").css({ "top": mouseY });

                var winH = $(window).height();
                var tcH = $(".tc").outerHeight();
                if (eventType == 'mouseenter') {
                    if (dirName[direction] == "上方") {
                        if (winH - mouseY < tcH) {
                            $(".tc").css({ "top": mouseY - (tcH - (winH - mouseY)) });
                            $(".tc").css({ "left": mouseX });
                        } else {
                            $(".tc").css({ "top": mouseY + 20 });
                            $(".tc").css({ "left": mouseX });
                        }
                    } if (dirName[direction] == "下方") {
                        if (winH - mouseY < tcH) {
                            $(".tc").css({ "top": mouseY - (tcH - (winH - mouseY)) });
                            $(".tc").css({ "left": mouseX });
                        }
                        else {
                            $(".tc").css({ "top": mouseY - 8 })
                            $(".tc").css({ "left": mouseX });
                        }
                    } if (dirName[direction] == "右侧" || dirName[direction] == "左侧") {
                        if (winH - mouseY < tcH) {
                            $(".tc").css({ "top": mouseY - (tcH - (winH - mouseY)) });
                            $(".tc").css({ "left": mouseX });
                        } else {
                            $(".tc").css({ "top": mouseY })
                            $(".tc").css({ "left": mouseX });
                        }
                    }
                }
            }
        })

        $(this).on("mouseleave", function () {
            //$(".tc").hide();

            if ($(this).find("a").html() == "编辑") {
                $(this).find(".tc").remove();
            }
        })
    });

    //复选框
    $(".ui-jqgrid-hdiv .ui-jqgrid-htable thead tr th:eq(1) .ckbox input").click(function () {
        if ($(this).prop("checked") == true) {
            $(".frozen-div .ui-jqgrid-htable thead tr #gridList_cb .ckbox input").prop("checked", "true");
        } if ($(this).prop("checked") == false) {
            $(".frozen-div .ui-jqgrid-htable thead tr #gridList_cb .ckbox input").click();
        }
    });
}
function daytime(name, num) {
    $('#' + name + '').datetimepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        startView: 2,
        minView: 2,
        language: 'zh-CN',
        autoclose: true,
        todayBtn: true,//显示今日按钮
        inputMask: true,
        initialDate: new Date(),	//初始化日期.默认new Date()当前日期
        forceParse: false,  	//当输入非格式化日期时，强制格式化。默认true
        bootcssVer: 3	//显示向左向右的箭头
    }).on('changeDate', function (ev) {
        var time = new Date(ev.date.getTime() + (ev.date.getTimezoneOffset() * 60000));
        var date = new Date(time);
        var mon = date.getMonth() + 1;
        var day = date.getDate();
        var mydate = date.getFullYear() + '' + (mon < 10 ? "0" + mon : mon) + '' + (day < 10 ? "0" + day : day);
        $('#' + num + '').val(mydate);
    });
}
function montime(name, num) {
    $('#' + name + '').datetimepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        startView: 3,
        minView: 3,
        language: 'zh-CN',
        autoclose: true,
        todayBtn: true,//显示今日按钮
        inputMask: true,
        initialDate: new Date(),	//初始化日期.默认new Date()当前日期
        forceParse: false,  	//当输入非格式化日期时，强制格式化。默认true
        bootcssVer: 3	//显示向左向右的箭头
    }).on('changeDate', function (ev) {
        var time = new Date(ev.date.getTime() + (ev.date.getTimezoneOffset() * 60000));
        var date = new Date(time);
        var mon = date.getMonth() + 1;
        var day = date.getDate();
        var mydate = date.getFullYear() + '' + (mon < 10 ? "0" + mon : mon);
        $('#' + num + '').val(mydate);
    });
}
function yeartime(name, num) {
    $('#' + name + '').datetimepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        startView: 4,
        minView: 4,
        language: 'zh-CN',
        autoclose: true,
        todayBtn: true,//显示今日按钮
        inputMask: true,
        initialDate: new Date(),	//初始化日期.默认new Date()当前日期
        forceParse: false,  	//当输入非格式化日期时，强制格式化。默认true
        bootcssVer: 3	//显示向左向右的箭头
    }).on('changeDate', function (ev) {
        var time = new Date(ev.date.getTime() + (ev.date.getTimezoneOffset() * 60000));
        var date = new Date(time);
        var mon = date.getMonth() + 1;
        var day = date.getDate();
        var mydate = date.getFullYear();
        $('#' + num + '').val(mydate);
    });
}