﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/onlineHub").build();
//上线通知
connection.on("Online", function (onlineUser) {
    console.log(onlineUser);
    var fid = onlineUser.empUid;
    $(".profile-partner").find("[data-id='" + fid + "']").find(".user-status").removeClass("status-offline").addClass("status-online");
    //添加聊天按钮
    $(".profile-partner").find("[data-id='" + fid + "']").append(`<div class="tools action-buttons">
            <a href="javascript:void(0)" data-chat="`+ fid + `" data-emp="` + onlineUser.empName + `" class="blue">
                <i class="ace-icon fa fa-comments bigger-125"></i>
            </a>
        </div>`);
});
connection.on("Offline", function (onlineUser) {
    var fid = onlineUser.empUid;
    $(".profile-partner").find("[data-id='" + fid + "']").find(".user-status").removeClass("status-online").addClass("status-offline");
    //移除聊天按钮
    $(".profile-partner").find("[data-id='" + fid + "']").find(".tools.action-buttons").remove();
});
//接收消息
connection.on("ReceiveMessage", function (fromUserName, fromUserid, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
   
});
connection.start().then(function () {
    //document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    //return console.error(err.toString());
});