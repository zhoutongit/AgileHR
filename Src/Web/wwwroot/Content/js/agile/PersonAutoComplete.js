﻿//系统人员数据，用于下拉选择框显示
function bindPersonAC(element) {
    $.ajax({
        type: "GET",
        url: "/rbac/User/GetGridJson",
        dataType: "json",
        async: false,
        success: function (personData) {
            //console.log(personData);
            if (personData != null) {
                $(element).autocomplete(personData, {
                    mustMatch: true,
                    selectFirst: true,
                    scroll: true,
                    minChars: 0,
                    cacheLength: 1,
                    max: 40,
                    width: 400,
                    scrollHeight: 250,
                    matchContains: "word",
                    autoFill: false,
                    formatItem: function (row, i, max) {
                        return i + "/" + max + ": \"" + row.Name + "\" " + " [" + row.Account + "] ";
                    },
                    formatMatch: function (row, i, max) {
                        return i + "/" + max + ": \"" + row.Name + "\" " + " [" + row.Account + "] ";
                    },
                    formatResult: function (row) {
                        return row.ID;
                    }
                });
            }
        }
    });
}
//清除自动完成组件
function cleanPersonInfo(element) {
    $(element).unautocomplete();
    $(element).unbind();
}

