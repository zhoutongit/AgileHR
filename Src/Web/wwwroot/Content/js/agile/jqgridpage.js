﻿var gridID = "#gridList";
var baseUrl = "";
//ids Arr
function get_gridId() {
    var myArr = new Array();
    var ret = $(gridID).jqGridRowValue();
    if (ret.length > 0) {
        for (var i = 0; i < ret.length; i++) {
            myArr.push(ret[i].ID);
        }
    } else {
        myArr.push($(gridID).jqGridRowValue().ID);
    }
    console.log(myArr);
    return myArr;
}
function btn_add() {
    $.modalOpen({
        id: "Form",
        title: "新增",
        url: baseUrl + "Form",
        width: "900px",
        height: "800px",
        callBack: function (iframeId) {
            top.frames[iframeId].submitForm();
        }
    });
}
function btn_edit(id) {
    if (id == "" || id == undefined) {
        var ids = get_gridId();
        if (ids == "" || ids == undefined || ids.length == 0) {
            $.modalAlert("请选择一条数据", "warning");
            return;
        }
        if (ids.length > 1) {
            $.modalAlert("不能选择多条数据", "warning");
            return;
        }
        id = ids[0];
    }
    $.modalOpen({
        id: "Form",
        title: "修改",
        url: baseUrl + "Form?keyId=" + id,
        width: "900px",
        height: "800px",
        callBack: function (iframeId) {
            top.frames[iframeId].submitForm();
        }
    });
}
function btn_delete(id) {
    if (id == "" || id == undefined) {
        var ids = get_gridId();
        if (ids == "" || ids == undefined) {
            $.modalAlert("请选择一条数据", "warning");
            return;
        }
        id = ids;
    }
    $.deleteForm({
        url: baseUrl + "DeleteForm",
        param: { keyValue: id.join(",") },
        success: function () {
            $(gridID).resetSelection();
            $(gridID).trigger("reloadGrid");
        }
    })
}
function btn_details(id) {
    if (id == "" || id == undefined) {
        var ids = get_gridId();
        console.log(ids);
        if (ids == "" || ids == undefined) {
            $.modalAlert("请选择一条数据", "warning");
            return;
        }
        if (ids.length > 1) {
            $.modalAlert("不能选择多条数据", "warning");
            return;
        }
        id = ids[0];
    }
    $.modalOpen({
        id: "Details",
        title: "查看",
        url: baseUrl + "Details?keyValue=" + keyValue,
        width: "700px",
        height: "490px",
        btn: null,
    });
}
//关闭有效触发
function Switch(keyId) {
    $.ajax({
        url: baseUrl + "Switch?keyId=" + keyId,
        data: {},
        type: "POST",
        dataType: "json",
        success: function () {
            $.currentWindow().$(gridID).resetSelection();
            $.currentWindow().$(gridID).trigger("reloadGrid");
        }
    });
}
function update_field(keyId, cellvalue) {
    $.ajax({
        url: baseUrl + "UpdateOrderBy?keyId=" + keyId + "&value=" + cellvalue,
        data: {},
        dataType: "json",
        type: "POST",
        success: function () {
            $.currentWindow().$(gridID).resetSelection();
            $.currentWindow().$(gridID).trigger("reloadGrid");
        }
    });
}