﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.OAuth {
    public class UserInfoBase
    {
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}
