﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OAuth;

namespace Web.Models.OAuth {
    public class SupportedOAuth {
        public static List<(string platform, string platformName, Type type, bool available)> List {
            get {
                
                return new List<(string, string, Type, bool)>()
                {
                    //("Baidu", "百度", typeof(OAuth.Baidu.BaiduOAuth), true),
                    //("Wechat", "微信公众号", typeof(OAuth.Wechat.WechatOAuth), true),
                    //("Gitlab", "Gitlab", typeof(OAuth.Gitlab.GitlabOAuth), true),
                    //("Gitee", "Gitee", typeof(OAuth.Gitee.GiteeOAuth), true),
                    //("Github", "Github", typeof(OAuth.Github.GithubOAuth), true),
                    //("Huawei", "华为", typeof(OAuth.Huawei.HuaweiOAuth), true),
                    //("Alipay", "支付宝", typeof(OAuth.Alipay.AlipayOAuth), true),
                    //("QQ", "QQ", typeof(OAuth.QQ.QQOAuth), true),
                    //("OSChina", "OSChina", typeof(OAuth.OSChina.OSChinaOAuth), true),
                    ////("DingTalk", "钉钉", typeof(DingTalk.DingTalkOAuth), true),
                    ////("DingTalkQrcode", "钉钉扫码登录", typeof(DingTalkQrcode.DingTalkQrcodeOAuth), true),
                    //("Microsoft", "Microsoft", typeof(OAuth.Microsoft.MicrosoftOAuth), true),
                    //("Mi", "小米", typeof(OAuth.XiaoMi.MiOAuth), true),
                };
            }
        }
    }
}
