﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service;

namespace Web.Models {
    public class SettingModel {
        public ApplicationSettings AppSettings { get; set; }
        public List<Application> AppSystem { get; set; }
    }
}
