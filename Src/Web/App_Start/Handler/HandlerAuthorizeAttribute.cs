﻿using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Agile.Service.Rbac
{
    public class HandlerAuthorizeAttribute : IActionFilter
    {
        public bool Ignore { get; set; }
        public HandlerAuthorizeAttribute(bool ignore = true)
        {
            Ignore = ignore;
        }
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (Ignore == false)
            {
                return;
            }
            if (!this.ActionAuthorize(context))
            {
                StringBuilder sbScript = new StringBuilder();
                sbScript.Append("<script type='text/javascript'>alert('很抱歉！您的权限不足，访问被拒绝！');</script>");
                context.Result = new ContentResult() { Content = sbScript.ToString() };
                return;
            }
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (Ignore == false)
            {
                return;
            }
        }
        private bool ActionAuthorize(ActionExecutingContext filterContext)
        {
            var loginInfo= EngineContext.Current.Resolve<ILoginInfo>();
            var roleId = loginInfo.AccountId;

            return true;
        }
    }
}