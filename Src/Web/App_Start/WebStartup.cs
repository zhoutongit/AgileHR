using System;
using System.Linq;
using Agile;
using Agile.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Web {
    public class WebStartup : IAgileStartup
    {
        public int Order => 7100;
        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();
            app.UseRouting();
            app.UseSession();
            app.UseCookiePolicy();
            app.UseCors(Utils.AppName);
            app.UseAuthentication();//认证
            app.UseAuthorization(); //鉴权
        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {
            
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            #region 上传文件大小限制
            /** begin xiongze 2021-03-08**************/
            //上传文件大小限制Kestrel设置
            services.Configure<KestrelServerOptions>(options => {
                // Set the limit to 256 MB
                options.Limits.MaxRequestBodySize = 268435456;
            });
            //上传文件大小限制IIS设置
            services.Configure<CookiePolicyOptions>(options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //解决文件上传Multipart body length limit 134217728 exceeded
            services.Configure<FormOptions>(x => {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
                x.MemoryBufferThreshold = int.MaxValue;
            });
            /** end xiongze 2021-03-08**************/

            #endregion

            services.AddSession(option => {
                option.Cookie.SameSite = SameSiteMode.None;
                option.IdleTimeout = TimeSpan.FromSeconds(1800);//session过期时间1800秒  30分钟
                option.Cookie.HttpOnly = true;
                option.Cookie.Name = ".agile.session";
                option.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            });
            services.AddSignalR();
            services.AddHttpClient();
            
            services.AddAuthentication(options => {
                options.DefaultChallengeScheme = Utils.AppName;
                options.DefaultScheme = Utils.AppName;
                options.DefaultSignInScheme = Utils.AppName;
            })
           .AddCookie(Utils.AppName,options => {
               options.AccessDeniedPath = new PathString("/page-not-found");
               options.LoginPath = new PathString("/Login/Index");
               options.LogoutPath = new PathString("/Login/Logout");
               options.Cookie.Name = Utils.AppName;
               options.Cookie.HttpOnly = true;
               options.Cookie.SecurePolicy = false ? CookieSecurePolicy.SameAsRequest : CookieSecurePolicy.None;
           });
            services.AddCors(op => {
                op.AddPolicy(Utils.AppName,set => {
                    set.SetIsOriginAllowed(origin => true).AllowAnyHeader()
                    .AllowAnyMethod().AllowCredentials();
                });
            });

           
        }
    }
}
