﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Components
{
    /// <summary>
    /// 穿梭框组件
    /// </summary>
    public class TransferViewComponent : AgileViewComponent
    {
        private readonly IRoleService _roleService;
        public TransferViewComponent(IRoleService roleService)
        {
            _roleService = roleService;
        }
        public IViewComponentResult Invoke(string keyword)
        {
            var model = _roleService.GetRoles(keyword);
            return View(model);
        }
    }
}
