﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agile.Service;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Components
{
    /// <summary>
    /// 切换系统 switch
    /// </summary>
    public class PlatformSwitchViewComponent: AgileViewComponent
    {
        private readonly IApplicationService _applicationService;
        public PlatformSwitchViewComponent(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }
        public IViewComponentResult Invoke()
        {
            var model = _applicationService.FindAll();
            return View(model);
        }
    }
}
