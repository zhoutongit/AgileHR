using System;
using System.Data;
using System.IO;
using Agile;
using Agile.Service;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace Web {
    public class Startup {
        #region Fields
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public Startup(IConfiguration configuration,IWebHostEnvironment webHostEnvironment) {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }
        #endregion
        public void ConfigureServices(IServiceCollection services) {
            services.AddAgileCore(_configuration,_webHostEnvironment);
            services.AddThemes();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }
        public void Configure(IApplicationBuilder app,IWebHostEnvironment env) {
            if(env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Home/Error");
            }
           
            app.UseThemes(_configuration);
            app.UseAgileCore();
            app.UseEndpoints(endpoints => {
                endpoints.MapHub<OnlineUserHub>("/onlineHub");
                endpoints.MapControllerRoute(
                 name: "areas",
                 pattern: "{area:exists}/{controller}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }

}
