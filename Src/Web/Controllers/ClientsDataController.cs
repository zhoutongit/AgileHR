﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Agile.Service;
using Agile.Service.Rbac;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers {
    public class ClientsDataController:Controller {
        private readonly IMenuService _menuService;
        private readonly IMenuElementService _menuElementService;
        private readonly IRoleService _roleService;
        private readonly IOrganizeService _organizeService;
        private readonly ILoginInfo _loginInfo;
        private readonly IItemsDetailService _itemsDetailService;
        private readonly IItemsService _itemsService;
        private readonly IAuth _auth;
        private readonly AuthStrategyContext _authStrategyContext;
        public ClientsDataController(IAuth auth,IMenuService menuService,
            IMenuElementService menuElementService,
            IRoleService roleService,
            IOrganizeService organizeService,
            IItemsDetailService itemsDetailService,
            IItemsService itemsService,
            ILoginInfo loginInfo) {
            _auth = auth;
            _menuService = menuService;
            _menuElementService = menuElementService;
            _roleService = roleService;
            _organizeService = organizeService;
            _itemsDetailService = itemsDetailService;
            _itemsService = itemsService;
            _loginInfo = loginInfo;
            _authStrategyContext = _auth.GetCurrentUser();
        }
        [HttpGet]
        public ActionResult GetClientsDataJson() {
            var data = new {
                dataItems = this.GetDataItemList(),
                organize = this.GetOrganizeList(),
                role = this.GetRoleList(),
                duty = this.GetDutyList(),
                user = _loginInfo,
                authorizeMenu = this.GetMenuList(),
                authorizeButton = this.GetMenuButtonList(),
            };
            return Content(data.ToJson());
        }
        private object GetMenuButtonList() {
            try {
                var data = _authStrategyContext.ModuleElements;
                var dataModuleId = data.Distinct(new ExtList<MenuElement>("ModuleId"));
                Dictionary<string,object> dictionary = new Dictionary<string,object>();
                foreach(MenuElement item in dataModuleId) {
                    var buttonList = data.Where(t => t.ModuleId.Equals(item.ModuleId));
                    if(!dictionary.ContainsKey(item.ModuleId)) {
                        dictionary.Add(item.ModuleId,buttonList);
                    }
                }
                return dictionary;
            } catch(Exception) {
                return null;
            }
        }
        private object GetMenuList() {
            try {
                var list = _authStrategyContext.Modules;
                return ToMenuJson(list,"00000000-0000-0000-0000-000000000000");
            } catch(Exception) {
                return null;
            }
        }
        private string ToMenuJson(List<Menu> data,string parentId) {
            StringBuilder sbJson = new StringBuilder();
            sbJson.Append("[");
            var entitys = data.FindAll(t => t.ParentID == parentId).OrderBy(p => p.OrderBy);
            if(entitys.Count() > 0) {
                foreach(var item in entitys) {
                    if(string.IsNullOrWhiteSpace(item.Icon)) item.Icon = "fa fa-bars";
                    string strJson = item.ToJson();
                    strJson = strJson.Insert(strJson.Length - 1,",\"ChildNodes\":" + ToMenuJson(data,item.ID) + "");
                    sbJson.Append(strJson + ",");
                }
                sbJson = sbJson.Remove(sbJson.Length - 1,1);
            }
            sbJson.Append("]");
            return sbJson.ToString();
        }
        private object GetDutyList() {
            var data = _roleService.FindAll(" Category=2 ");
            Dictionary<string,object> dictionary = new Dictionary<string,object>();
            foreach(Role item in data) {
                var fieldItem = new {
                    encode = item.ID,
                    fullname = item.Name
                };
                if(!dictionary.ContainsKey(item.ID)) {
                    dictionary.Add(item.ID,fieldItem);
                }
            }
            return dictionary;
        }
        private object GetRoleList() {
            try {
                var data = _authStrategyContext.Roles;
                Dictionary<string,object> dictionary = new Dictionary<string,object>();
                foreach(Role item in data) {
                    var fieldItem = new {
                        encode = item.ID,
                        fullname = item.Name
                    };
                    if(!dictionary.ContainsKey(item.ID)) {
                        dictionary.Add(item.ID,fieldItem);
                    }
                }
                return dictionary;
            } catch(Exception) {
                return null;
            }
        }
        private object GetOrganizeList() {
            try {
                var data = _authStrategyContext.Organize;
                Dictionary<string,object> dictionary = new Dictionary<string,object>();
                foreach(Organize item in data) {
                    var fieldItem = new {
                        encode = item.ID,
                        fullname = item.Name
                    };
                    if(!dictionary.ContainsKey(item.ID)) {
                        dictionary.Add(item.ID,fieldItem);
                    }
                }
                return dictionary;
            } catch(Exception) {
                return null;
            }
        }
        private object GetDataItemList() {
            try {
                var itemdata = _itemsDetailService.FindAll("ItemId is not null and ItemId<>''").ToList();
                var items = _itemsService.FindAll();
                Dictionary<string,object> dictionaryItem = new Dictionary<string,object>();
                foreach(var item in items) {
                    var dataItemList = itemdata.FindAll(t => t.ItemId.Equals(item.ID));
                    Dictionary<string,string> dictionaryItemList = new Dictionary<string,string>();
                    foreach(var itemList in dataItemList) {
                        dictionaryItemList.TryAdd(itemList.ID,itemList.Name);
                    }
                    dictionaryItem.TryAdd(item.EnCode,dictionaryItemList);
                }
                return dictionaryItem;
            } catch(Exception) {
                return null;
            }
        }
    }
}