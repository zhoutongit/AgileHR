﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Web.Controllers {
    /// <summary>
    /// 通用表选择器配合selectTable使用
    /// 
    /// </summary>
    public class TableAPIController:Controller {
        public IDbSession _dbSession;

        public TableAPIController() {
            _dbSession=DbFactory.DbHelper();
        }

        public IActionResult Demo() {
            return View();
        }

        [HttpPost]
        public IActionResult search() {
            PageGridViewModel pageGrid = new PageGridViewModel();
            var pageNumber = Utils.GetObjTranNull<int>(Request.Form["pageNumber"].ToString());
            var offset = string.IsNullOrWhiteSpace(Request.Form["offset"].ToString()) ? "1" : Request.Form["offset"].ToString();
            var limit = string.IsNullOrWhiteSpace(Request.Form["limit"].ToString()) ? 10 : Utils.GetObjTranNull<int>(Request.Form["limit"].ToString());
            var tableName = Request.Form["tableName"].ToString();
            var searchField = string.IsNullOrWhiteSpace(Request.Form["searchField"].ToString()) ? "*" : Request.Form["searchField"].ToString();
            var conditions = Request.Form["conditions"].ToString();
            var sort = string.IsNullOrWhiteSpace(Request.Form["sort"].ToString()) ? "id" : Request.Form["sort"].ToString();
            var order = string.IsNullOrWhiteSpace(Request.Form["order"].ToString()) ? "desc" : Request.Form["order"].ToString();
            var sql = Request.Form["sql"].ToString();
            var search = Request.Form["search"].ToString();

            StringBuilder _whereStr = new StringBuilder();
            if(!string.IsNullOrWhiteSpace(conditions)&&conditions.Contains("whereStr")) {
                JObject _jObject = JObject.Parse(conditions);
                _whereStr.Append(_jObject["whereStr"].ToString());
            } else {
                JObject _jObject = JObject.Parse(conditions);
                List<string> listSql = new List<string>();
                foreach(JProperty item in _jObject.Children()) {
                    listSql.Add($" {item.Name}='{item.Value}' ");
                }
                _whereStr.Append(string.Join(" and ",listSql.ToArray()));
            }
            if(!string.IsNullOrWhiteSpace(sql)) {
                string orderby = sort+" "+order;
                
                var data =_dbSession.ExecuteDataTable(sql);
                pageGrid.rows=data;
                pageGrid.total=data.Rows.Count.ToString();
                pageGrid.other="";

            } else {
                string orderby = sort+" "+order;
                
                //pageGrid.rows=DbHelperSQL.GetListByPage(_whereStr.ToString(),orderby,pageNumber,limit,tableName);
                //pageGrid.total=DbHelperSQL.GetRecordCount(_whereStr.ToString(),tableName).ToString();
                pageGrid.other="";
            }
            return Content(pageGrid.ToJson());

        }
    }

    public class PageGridViewModel {
        public object rows { get; set; }
        public string total { get; set; }
        public string other { get; set; }
    }
}
