﻿using System;
using Agile.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers {
    [Authorize]
    public class HomeController:BaseController {
        private readonly ApplicationSettings _applicationSettings;
        private readonly ISettingService _settingService;
        private readonly IApplicationService _applicationService;
        public HomeController(ApplicationSettings applicationSettings,IApplicationService applicationService,
            ISettingService settingService) {
            _applicationSettings = applicationSettings;
            _settingService = settingService;
            _applicationService=applicationService;
        }
        public override IActionResult Index() {
            ViewBag.AppNameList=_applicationService.FindAll();

            string uitheme = GetCookies("page_layout").ToLower();
            switch(uitheme) {
                case "admintop":
                    return View("AdminTop",_applicationSettings); // 顶部菜单版本
                case "admindefault":
                    return View("AdminDefault",_applicationSettings); // 顶部菜单版本
                default:
                    return View("AdminDefault",_applicationSettings); // 经典版本
            }
        }
        public IActionResult AdminDesktop() {
            return View(); // 桌面
        }
        public IActionResult ClearCache() {
            try {

            } catch(Exception ex) {
                return Error(ex.InnerException?.Message ?? ex.Message);
            }
            return Success("清除缓存成功。");
        }
        public IActionResult PlatformSwitch(string keyValue) {
            ConfigurationHelper.SetValue("current_system",keyValue);
            _applicationSettings.CurrentSystem = keyValue;
            _settingService.SaveSetting(_applicationSettings);
            return Success();
        }
    }
}