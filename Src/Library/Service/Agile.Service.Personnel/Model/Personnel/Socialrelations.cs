﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Personnel
{
    /// <summary>
    /// 家庭成员
    /// </summary>
    [Table("personnel_socialrelations")]
    [Description("家庭成员")]
    public class Socialrelations : Entity
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [Description("姓名")]
        public virtual string Name { get; set; }
        /// <summary>
        /// 与本人关系
        /// </summary>
        [Description("与本人关系")]
        public virtual string Relations { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [Description("单位")]
        public virtual string Unit { get; set; }
        /// <summary>
        /// 文化程度
        /// </summary>
        [Description("文化程度")]
        public string Education { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        [Description("联系方式")]
        public string Contact { get; set; }
    }
}
