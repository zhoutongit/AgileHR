﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Personnel
{
    /// <summary>
    /// 部门调动
    /// </summary>
    [Table("personnel_orgtransfer")]
    [Description("部门调动")]
    public class OrgTransfer : Entity
    {
        /// <summary>
        /// 调动前部门
        /// </summary>
        [Description("调动前部门")]
        public string OldOrgID { get; set; }
        /// <summary>
        /// 调动前部门
        /// </summary>
        [Description("调动前部门")]
        public string OldOrgName { get; set; }
        /// <summary>
        /// 调动前职位
        /// </summary>
        [Description("调动前职位")]
        public string OldPosition { get; set; }
        /// <summary>
        /// 调动后部门
        /// </summary>
        [Description("调动后部门")]
        public string NewOrgID { get; set; }
        /// <summary>
        /// 调动后部门
        /// </summary>
        [Description("调动后部门")]
        public string NewOrgName { get; set; }
        /// <summary>
        /// 调动后职位
        /// </summary>
        [Description("调动后职位")]
        public string NewPosition { get; set; }
        /// <summary>
        /// 调动原因
        /// </summary>
        [Description("调动原因")]
        public string Reason { get; set; }
        /// <summary>
        /// 备注信息
        /// </summary>
        [Description("备注信息")]
        public string Remarks { get; set; }
    }
}
