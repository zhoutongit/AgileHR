﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Personnel
{
    /// <summary>
    /// 工作经验
    /// </summary>
    [Table("personnel_workresume")]
    [Description("工作经验")]
    public class WorkResume : Entity
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        [Description("开始时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Description("结束时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? EndhDate { get; set; }
        /// <summary>
        /// 公司 如：阿里巴巴
        /// </summary>
        [Description("公司")]
        public string Company { get; set; }
        /// <summary>
        /// 职位 如：软件工程师
        /// </summary>
        [Description("职位")]
        public string Position { get; set; }
        /// <summary>
        /// 工作描述
        /// </summary>
        [Description("工作描述")]
        public string JobDescription { get; set; }
        /// <summary>
        /// 证明人
        /// </summary>  
        [Description("证明人")]
        public string Voucher { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        [Description("联系方式")]
        public string Contact { get; set; }
        /// <summary>
        /// 工作类型
        /// </summary>
        [Description("工作类型")]
        public JobType Type { get; set; }
    }
    public enum JobType
    {
        全职,
        兼职
    }
}
