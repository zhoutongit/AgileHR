﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Personnel
{
    /// <summary>
    /// 教育经历
    /// </summary>
    [Table("personnel_educationresume")]
    [Description("教育经历")]
    public class EducationResume : Entity
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        [Description("开始时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Description("结束时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? EndhDate { get; set; }
        /// <summary>
        /// 学校
        /// </summary>
        [Description("学校")]
        public string School { get; set; }
        /// <summary>
        /// 学制
        /// </summary>
        [Description("学制")]
        public string Educational { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        [Description("专业")]
        public string Specialty { get; set; }
        /// <summary>
        /// 学缘
        /// </summary>
        [Description("学缘")]
        public string Content { get; set; }
        /// <summary>
        /// 经历
        /// </summary>
        [Description("经历")]
        public string Experience { get; set; }
        /// <summary>
        /// 学位
        /// </summary>
        [Description("学位")]
        public string Degree { get; set; }
        /// <summary>
        /// 学历形式
        /// </summary>
        [Description("学历形式")]
        public string DegreeForm { get; set; }
        /// <summary>
        /// 学历归属
        /// </summary>
        [Description("学历归属")]
        public string ExperienceBT { get; set; }
        /// <summary>
        /// 隶属学科
        /// </summary>
        [Description("隶属学科")]
        public string TheirDiscipline { get; set; }
        /// <summary>
        /// 指导教师
        /// </summary>
        [Description("指导教师")]
        public string GuideTeacher { get; set; }

    }
}
