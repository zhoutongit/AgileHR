﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Personnel
{
    /// <summary>
    /// 教师详细资料表
    /// </summary>
    [Table("personnel_teacherinfo")]
    [Description("教师详细")]
    public class TeacherInfo : Entity
    {
        /// <summary>
        /// 在岗状态
        /// </summary>
        [Description("在岗状态")]
        public string ZaiGang { get; set; }
        /// <summary>
        /// 在编状态
        /// </summary>
        [Description("在编状态")]
        public string ZaiBian { get; set; }
        /// <summary>
        /// 学历
        /// </summary>
        [Description("学历")]
        public string Education { get; set; }
        /// <summary>
        /// 学位
        /// </summary>
        [Description("学位")]
        public string Degree { get; set; }
        /// <summary>
        /// 是否双肩挑
        /// </summary>
        [Description("是否双肩挑")]
        public string ShuangJianTiao { get; set; }
        /// <summary>
        /// 双职工
        /// </summary>
        [Description("双职工")]
        public string ShuangZhiGong { get; set; }
        /// <summary>
        /// 学缘结构
        /// </summary>
        [Description("学缘结构")]
        public string Structure { get; set; }
        /// <summary>
        /// 是否是学科带头人
        /// </summary>
        [Description("是否是学科带头人")]
        public string AcademicLeader { get; set; }
        /// <summary>
        /// 是否是中青年骨干教师
        /// </summary>
        [Description("是否是中青年骨干教师")]
        public string BackboneTeacher { get; set; }

    }
}
