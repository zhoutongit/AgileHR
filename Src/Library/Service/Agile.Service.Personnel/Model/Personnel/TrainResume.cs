﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Personnel
{
    /// <summary>
    /// 培训经历
    /// </summary>
    [Table("personnel_trainresume")]
    [Description("培训经历")]
    public class TrainResume : Entity
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        [Description("开始时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Description("结束时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? EndhDate { get; set; }
        /// <summary>
        /// 培训机构
        /// </summary>
        [Description("培训机构")]
        public string Organization { get; set; }
        /// <summary>
        /// 培训课程
        /// </summary>
        [Description("培训课程")]
        public string Course { get; set; }
        /// <summary>
        /// 培训描述
        /// </summary>
        [Description("培训描述")]
        public string Description { get; set; }
    }
}
