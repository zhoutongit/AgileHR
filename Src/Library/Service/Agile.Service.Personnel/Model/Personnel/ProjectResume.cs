﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Personnel
{
    /// <summary>
    /// 项目经验
    /// </summary>
    [Table("personnel_projectresume")]
    [Description("项目经验")]
    public class ProjectResume : Entity
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        [Description("项目名称")]
        public string Name { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Description("开始时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Description("结束时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? EndhDate { get; set; }
        /// <summary>
        /// 项目描述
        /// </summary>
        [Description("项目描述")]
        public string ProjectDescription { get; set; }
        /// <summary>
        /// 责任描述
        /// </summary>
        [Description("责任描述")]
        public string DutyDescription { get; set; }

    }
}
