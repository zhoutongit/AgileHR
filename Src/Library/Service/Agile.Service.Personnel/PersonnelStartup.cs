﻿using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Personnel
{
    public class PersonnelStartup : IAgileStartup
    {
        public int Order => 7003;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //仓储模式注入
            //personnel_educationresume教育经历
            services.AddSingleton<IEducationResumeService, EducationResumeService>();
            //personnel_orgtransfer部门调动
            services.AddSingleton<IOrgTransferService, OrgTransferService>();
            //personnel_projectresume项目经验
            services.AddSingleton<IProjectResumeService, ProjectResumeService>();
            //personnel_socialrelations家庭成员
            services.AddSingleton<ISocialrelationsService, SocialrelationsService>();
            //personnel_teacherinfo教师详细
            services.AddSingleton<ITeacherInfoService, TeacherInfoService>();
            //personnel_trainresume培训经历
            services.AddSingleton<ITrainResumeService, TrainResumeService>();
            //personnel_workresume工作经验
            services.AddSingleton<IWorkResumeService, WorkResumeService>();
        }
    }
}
