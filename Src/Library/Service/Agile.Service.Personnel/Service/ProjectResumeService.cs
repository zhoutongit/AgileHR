﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service.Personnel
{
    ///<summary>
    ///项目经验
    ///</summary>
    public interface IProjectResumeService : IRepository<ProjectResume>
    {

    }
    ///<summary>
    ///项目经验
    ///</summary>
    public partial class ProjectResumeService : BaseRepository<ProjectResume>, IProjectResumeService
    {

    }
}