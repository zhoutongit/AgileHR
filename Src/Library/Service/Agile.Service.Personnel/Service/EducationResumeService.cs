﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service.Personnel
{
    ///<summary>
    ///教育经历
    ///</summary>
    public interface IEducationResumeService : IRepository<EducationResume>
    {

    }
    ///<summary>
    ///教育经历
    ///</summary>
    public partial class EducationResumeService : BaseRepository<EducationResume>, IEducationResumeService
    {

    }
}
