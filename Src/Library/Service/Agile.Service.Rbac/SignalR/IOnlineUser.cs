﻿using System.Threading.Tasks;

namespace Agile.Service.Rbac {
    public interface IOnlineUser
    {
        Task Online(object onlineUser);
        Task Offline(object onlineUser);
        Task ReceiveMessage(string userName,string userId, string message);
        Task ReceiveMessage(string message);
    }
}
