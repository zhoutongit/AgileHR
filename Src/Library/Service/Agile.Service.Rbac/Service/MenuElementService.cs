﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace Agile.Service.Rbac
{
    ///<summary>
    ///模块元素表
    ///</summary>
    public interface IMenuElementService : IRepository<MenuElement>
    {
        List<MenuElement> GetButtonList(string roleId);
        List<MenuElement> GetListByModuleId(string moduleId = "");
    }
    ///<summary>
    ///模块元素表
    ///</summary>
    public partial class MenuElementService : BaseRepository<MenuElement>, IMenuElementService
    {
        public List<MenuElement> GetButtonList(string roleId)
        {
            return FindAll("").ToList();
        }
        public List<MenuElement> GetListByModuleId(string moduleId = "")
        {
            string where = "";
            if (!string.IsNullOrEmpty(moduleId))
            {
                where = "ModuleId='" + moduleId + "'";
            }
            return FindAll(where).OrderBy(t => t.OrderBy).ToList();
        }
    }
}

