﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Agile.Service.Rbac {
    ///<summary>
    ///在线用户
    ///</summary>
    public interface IUserOnlineService:IRepository<UserOnline> {
        void OnlineUser(UserOnline online);
        void OfflineUser(string connectionId);
    }
    ///<summary>
    ///在线用户
    ///</summary>
    public partial class UserOnlineService:BaseRepository<UserOnline>, IUserOnlineService {
        public void OfflineUser(string connectionId) {
            if(string.IsNullOrEmpty(connectionId)) return;
            this.DBA.ExecuteNonQuery(@"UPDATE rbac_useronline SET OnlineState='" + UserOnline.CONST_OFFLINE + "' ," +
                "LogoutTime=getdate()" +
                "WHERE ConnectionId='" + connectionId + "'");
        }

        public void OnlineUser(UserOnline online) {
            var model = this.Find(p => p.AccountId == online.AccountId && p.ClientIP == online.ClientIP);
            if(model != null) {
                model.ConnectionId = online.ConnectionId;
                model.AccountId = online.AccountId;
                model.AccountName = online.AccountName;
                model.ClientIP = online.ClientIP;
                model.LoginTime = online.LoginTime;
                model.OnlineState = UserOnline.CONST_ONLINE;
                this.Update(model);
            } else {
                this.AddOrUpdate(online);
            }
        }
    }
}

