﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Agile.Service.Rbac
{
    ///<summary>
    ///模块管理
    ///</summary>
    public interface IMenuService : IRepository<Menu>
    {
        List<Menu> GetMenuList(string roleId);
        void Switch(string menuId);
        void DeleteAll(string menuId);
    }
    ///<summary>
    ///模块管理
    ///</summary>
    public partial class MenuService : BaseRepository<Menu>, IMenuService
    {
        private readonly IMenuElementService _menuElementService;
        public MenuService(IMenuElementService menuElementService)
        {
            _menuElementService = menuElementService;
        }
        public List<Menu> GetMenuList(string roleId)
        {
            return FindAll("1=1 order by OrderBy").ToList();
        }
        public void Switch(string menuId)
        {
            var model = Find(menuId);
            if (model != null)
            {
                if (model.Status > 0)
                {
                    model.Status = 0;
                }
                else
                {
                    model.Status = 1;
                }
                Update(model);
            }
        }

        public void DeleteAll(string menuId)
        {
            this.Delete(menuId);
            _menuElementService.DeleteWhere($"ModuleId='{menuId}'");
        }

    }
}

