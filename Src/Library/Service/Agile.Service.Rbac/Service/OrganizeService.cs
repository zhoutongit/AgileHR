﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Agile.Service.Rbac
{
    ///<summary>
    ///组织表
    ///</summary>
    public interface IOrganizeService : IRepository<Organize>
    {
        void Switch(string Id);
    }
    ///<summary>
    ///组织表
    ///</summary>
    public partial class OrganizeService : BaseRepository<Organize>, IOrganizeService
    {
        public void Switch(string Id)
        {
            var model = Find(Id);
            if (model != null)
            {
                if (model.Status > 0)
                {
                    model.Status = 0;
                }
                else
                {
                    model.Status = 1;
                }
                Update(model);
            }
        }


       
    }
}

