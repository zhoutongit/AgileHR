using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Rbac
{
    public class RbacStartup : IAgileStartup
    {
        public int Order => 7002;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //仓储模式注入
            //Name:Menu Remark:模块管理
            services.AddSingleton<IMenuService, MenuService>();
            //Name:MenuElement Remark:模块元素表
            services.AddSingleton<IMenuElementService, MenuElementService>();
            //Name:Organize Remark:组织表
            services.AddSingleton<IOrganizeService, OrganizeService>();
            //Name:Role Remark:角色表
            services.AddSingleton<IRoleService, RoleService>();
            //Name:RoleAuthorize Remark:角色权限
            //services.AddSingleton<IRoleAuthorizeService, RoleAuthorizeService>();
            //Name:User Remark:用户
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IUserOnlineService,UserOnlineService>();

            services.AddTransient<NormalAuthStrategy>();
            services.AddTransient<SystemAuthStrategy>();

            //services.AddTransient<AuthStrategyContext>();
            services.AddTransient<AuthContextFactory>();

            services.AddScoped<IAuth, LocalAuth>();

        }
    }
}
