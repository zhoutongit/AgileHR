﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Agile.Service.Rbac
{
    /// <summary>
    /// 功能模块表
    /// </summary>
    [Table("rbac_menu")]
    [Description("模块管理")]
    [DebuggerDisplay("Name:{Name}, Url:{Url}")]
    public partial class Menu : TreeEntity, ISoftDelete
    {
        public Menu()
        {
            Icon = "fa fa-file-text-o";
        }
        /// <summary>
        /// 软删除
        /// </summary>
        [Description("软删除")]
        public virtual int IsDelete { get; set; }
        /// <summary>
        /// 软删除用户
        /// </summary>
        [Description("软删除用户")]
        public string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除时间
        /// </summary>
        [Description("软删除时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        [Description("URL")]
        public string Url { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 当前状态，0：正常，-1：隐藏，不在导航列表中显示
        /// </summary>
        [Description("当前状态")]
        public int Status { get; set; }
        /// <summary>
        /// 模块标识
        /// </summary>
        [Description("模块标识")]
        public string Code { get; set; }
        /// <summary>
        /// 是否系统模块
        /// </summary>
        [Description("是否系统模块")]
        public bool IsSys { get; set; }

    }
}