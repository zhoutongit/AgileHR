﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Rbac
{
    /// <summary>
    /// 角色表
    /// </summary>
    [Table("rbac_role")]
    [Description("角色表")]
    public partial class Role : Entity, ISoftDelete
    {
        /// <summary>
        /// 软删除
        /// </summary>
        [Description("软删除")]
        public virtual int IsDelete { get; set; }
        /// <summary>
        /// 软删除用户
        /// </summary>
        [Description("软删除用户")]
        public string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除时间
        /// </summary>
        [Description("软删除时间")]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 归属组织
        /// </summary>
        [Description("归属组织")]
        public string OrganizeId { get; set; }
        /// <summary>
        /// 分类:1-角色2-岗位
        /// </summary>
        [Description("分类")]
        public int Category { get; set; }
        /// <summary>
        /// 角色编号
        /// </summary>
        [Description("角色编号")]
        public string EnCode { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        [Description("角色名称")]
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [Description("角色类型")]
        public string Type { get; set; }
        /// <summary>
        /// 允许编辑
        /// </summary>
        [Description("允许编辑")]
        public bool AllowEdit { get; set; }
        /// <summary>
        /// 允许删除
        /// </summary>
        [Description("允许删除")]
        public bool AllowDelete { get; set; }
        /// <summary>
        /// 有效
        /// </summary>
        [Description("有效")]
        public int Status { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Description("说明")]
        public string Description { get; set; }
    }
}