﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Rbac {
    /// <summary>
    /// 在线用户
    /// </summary>
    [Table("rbac_useronline")]
    [Description("在线用户")]
    public partial class UserOnline:Entity {
        public const string CONST_ONLINE = "Online";
        public const string CONST_OFFLINE = "Offline";
        public const string CONST_LOGEXCEPT = "Exception";
        /// <summary>
        /// 账户编号
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// 账户名称
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// 客户端IP
        /// </summary>
        public string ClientIP { get; set; }
        /// <summary>
        /// 登陆时间
        /// </summary>
        public DateTime? LoginTime { get; set; }
        /// <summary>
        /// 登出时间
        /// </summary>
        public DateTime? LogoutTime { get; set; }
        /// <summary>
        /// 在线状态
        /// </summary>
        public string OnlineState { get; set; }
        /// <summary>
        /// 连接ID
        /// </summary>
        public string ConnectionId { get; set; }
    }
}
