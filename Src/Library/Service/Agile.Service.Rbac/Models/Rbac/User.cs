﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Rbac
{
    /// <summary>
    /// 用户基本信息表
    /// </summary>
    [Table("rbac_user")]
    [Description("用户")]
    public partial class User : Entity, ISoftDelete
    {
        /// <summary>
        /// 所属租户
        /// </summary>
        [Description("所属租户")]
        public virtual string TenantId { get; set; }
        /// <summary>
        /// 软删除
        /// </summary>
        [Description("软删除")]
        public virtual int IsDelete { get; set; }
        /// <summary>
        /// 软删除用户
        /// </summary>
        [Description("软删除用户")]
        public string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除时间
        /// </summary>
        [Description("软删除时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 用户登录帐号
        /// </summary>
        [Description("用户登录帐号")]
        public string Account { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Description("密码")]
        [MaxLength(200)]
        public string Password { get; set; }
        /// <summary>
        /// 用户姓名
        /// </summary>
        [Description("用户姓名")]
        public string Name { get; set; }
        /// <summary>
        /// 性别 0未知,1男,2女
        /// </summary>
        [Description("性别")]
        public int Sex { get; set; }
        /// <summary>
        /// 用户状态
        /// </summary>
        [Description("用户状态")]
        public int Status { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        [Description("手机")]
        public string Mobile { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        [Description("电子邮箱")]
        public string Email { get; set; }
        /// <summary>
        /// 国籍/地区
        /// </summary>
        [Description("国籍/地区")]
        public string Nationality { get; set; }
        /// <summary>
        /// 宗教信仰
        /// </summary>
        [Description("宗教信仰")]
        public string Faith { get; set; }
        /// <summary>
        /// 健康状况
        /// </summary>
        [Description("健康状况")]
        public string Health { get; set; }
        /// <summary>
        /// 证件类型：身份证 护照 军官证等
        /// </summary>
        [Description("证件类型")]
        public string IdcardType { get; set; }
        /// <summary>
        /// 证件号码
        /// </summary>
        [Description("证件号码")]
        public string Idcard { get; set; }
        /// <summary>
        /// 邮寄地址
        /// </summary>
        [Description("邮寄地址")]
        public string Address { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        [Description("邮编")]
        public string Zipcode { get; set; }
        /// <summary>
        /// 生肖(根据生日自动计算)
        /// </summary>
        [Description("生肖")]
        public string Zodiac { get; set; }
        /// <summary>
        /// 星座(根据生日自动计算)
        /// </summary>
        [Description("星座")]
        public string Constellation { get; set; }
        /// <summary>
        /// 出生年月日
        /// </summary>
        [Description("出生年月日")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// 婚姻状态
        /// </summary>
        [Description("婚姻状态")]
        public string Marry { get; set; }
        /// <summary>
        /// 政治面貌
        /// </summary>
        [Description("政治面貌")]
        public string PoliticalID { get; set; }
        /// <summary>
        /// 名族
        /// </summary>
        [Description("名族")]
        public string NationalID { get; set; }
        /// <summary>
        /// 籍贯
        /// </summary>
        [Description("籍贯")]
        public string NativePlace { get; set; }
    }
}