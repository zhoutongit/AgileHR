﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Rbac
{
    /// <summary>
    /// 组织表
    /// </summary>
    [Table("rbac_organize")]
    [Description("组织表")]
    public partial class Organize : TreeEntity, ISoftDelete
    {
        /// <summary>
        /// 所属租户
        /// </summary>
        [Description("所属租户")]
        public virtual string TenantId { get; set; }
        /// <summary>
        /// 软删除
        /// </summary>
        [Description("软删除")]
        public virtual int IsDelete { get; set; }
        /// <summary>
        /// 软删除用户
        /// </summary>
        [Description("软删除用户")]
        public string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除时间
        /// </summary>
        [Description("软删除时间")]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 当前状态
        /// </summary>
        [Description("当前状态")]
        public int Status { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        public string EndCode { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        [Description("简称")]
        public string ShortName { get; set; }
        /// <summary>
        /// 分类
        /// </summary>
        [Description("分类")]
        public string CategoryId { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [Description("负责人")]
        public string ManagerId { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        [Description("手机")]
        public string Mobile { get; set; }
        /// <summary>
        /// 归属区域
        /// </summary>
        [Description("归属区域")]
        public string AreaId { get; set; }

    }
}