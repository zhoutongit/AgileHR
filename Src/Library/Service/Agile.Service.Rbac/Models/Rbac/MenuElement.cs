﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Rbac
{
    /// <summary>
    /// 模块元素表(需要权限控制的按钮)
    /// </summary>
    [Table("rbac_menuElement")]
    [Description("模块元素表")]
    public partial class MenuElement : Entity
    {
        /// <summary>
        /// 功能模块Id
        /// </summary>
        [Description("功能模块Id")]
        public string ModuleId { get; set; }
        /// <summary>
        /// DOM ID
        /// </summary>
        [Description("DOM ID")]
        public string DomId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        [Description("位置")]
        public virtual int Location { get; set; }
        /// <summary>
        /// 元素附加属性
        /// </summary>
        [Description("元素附加属性")]
        public string Attr { get; set; }
        /// <summary>
        /// 元素调用脚本
        /// </summary>
        [Description("元素调用脚本")]
        public string Script { get; set; }
        /// <summary>
        /// 元素样式
        /// </summary>
        [Description("元素样式")]
        public string Class { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string Remark { get; set; }

    }
}