﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service.Rbac {
    /// <summary>
    ///  授权策略上下文，一个典型的策略模式
    /// </summary>
    public class AuthStrategyContext {
        private IAuthStrategy _strategy;
        public AuthStrategyContext(IAuthStrategy strategy) {
            this._strategy = strategy;
        }
        public User User {
            get { return _strategy.User; }
        }
        public List<Menu> Modules {
            get { return _strategy.Modules; }
        }
        public List<MenuElement> ModuleElements {
            get { return _strategy.ModuleElements; }
        }
        public List<Role> Roles {
            get { return _strategy.Roles; }
        }
        public List<Organize> Organize {
            get { return _strategy.Organize; }
        }
        public List<KeyValue> GetProperties(string moduleCode) {
            return _strategy.GetProperties(moduleCode);
        }
    }
}
