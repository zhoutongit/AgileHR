﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Agile.Service.Rbac {
    /// <summary>
    /// 领域服务
    /// <para>超级管理员权限</para>
    /// <para>超级管理员使用guid.empty为ID，可以根据需要修改</para>
    /// </summary>
    public class SystemAuthStrategy:IAuthStrategy {
        private readonly ILoginInfo _loginInfo;
        private readonly IMenuService _menuService;
        private readonly IMenuElementService _menuElementService;
        private readonly IRoleService _roleService;
        private readonly IOrganizeService _organizeService;
        private readonly IUserService _userService;
        private readonly IRelevanceService _relevanceService;
        private readonly ApplicationSettings _applicationSettings;
        public SystemAuthStrategy(ILoginInfo loginInfo,
            IMenuService menuService,
            IMenuElementService menuElementService,
            IRoleService roleService,
            IUserService userService,
            IOrganizeService organizeService,
            IRelevanceService relevanceService,
            ApplicationSettings applicationSettings) {
            _loginInfo = loginInfo;
            _menuService = menuService;
            _menuElementService = menuElementService;
            _roleService = roleService;
            _organizeService = organizeService;
            _userService = userService;
            _relevanceService = relevanceService;
            _applicationSettings = applicationSettings;
        }
        public List<Menu> Modules {
            get {
                var resourceIds = _relevanceService.FindAll(u => u.Key == Define.APPLICATIONMODULE && u.FirstId == _applicationSettings.CurrentSystem).Select(u => u.SecondId).ToList();
                Debug.WriteLine(this.GetType().Name + " Modules :" + resourceIds.ToJson());
                if(resourceIds != null && resourceIds.Count > 0) {
                    var list = _menuService.FindAll("[Status]=0 and ID IN(" + resourceIds.ToSql(false) + ")");
                    return list.ToList();
                } else {
                    var list = _menuService.FindAll("[Status]=0");
                    return list.ToList();
                }
            }
        }
        public List<MenuElement> ModuleElements {
            get {
                var resourceIds = _relevanceService.FindAll(u => u.Key == Define.APPLICATIONMODULE && u.FirstId == _applicationSettings.CurrentSystem).Select(u => u.SecondId).ToList();
                Debug.WriteLine(this.GetType().Name + " Modules :" + resourceIds.ToJson());
                if(resourceIds != null && resourceIds.Count > 0) {
                    var list = _menuElementService.FindAll("[Status]=0 and ID IN(" + resourceIds.ToSql(false) + ")");
                    return list.ToList();
                } else {
                    var list = _menuElementService.FindAll("[Status]=0");
                    return list.ToList();
                }
            }
        }
        public List<Role> Roles {
            get {
                var list = _roleService.FindAll("[Status]=0").ToList();
                return list;
            }
        }
        public List<Organize> Organize {
            get {
                var list = _organizeService.FindAll("[Status]=0").ToList();
                return list;
            }
        }
        public User User {
            get {
                var model = _userService.GetByName(_loginInfo.AccountName);
                return model;
            }
            set//禁止外部设置
            {
                throw new Exception("超级管理员，禁止设置用户");
            }
        }
        public List<KeyValue> GetProperties(string moduleCode) {
            throw new NotImplementedException();
        }
    }
}
