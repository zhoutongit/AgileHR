﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service.Rbac
{
    public interface IAuthStrategy
    {
        List<Menu> Modules { get; }

        List<MenuElement> ModuleElements { get; }

        List<Role> Roles { get; }

        List<Organize> Organize { get; }

        User User { get; set; }

        /// <summary>
        /// 根据模块id获取可访问的模块字段
        /// </summary>
        List<KeyValue> GetProperties(string moduleCode);

    }
}
