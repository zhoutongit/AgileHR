﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agile.Service.Rbac {
    /// <summary>
    /// 普通用户授权策略
    /// </summary>
    public class NormalAuthStrategy:IAuthStrategy {
        protected User _user;
        private List<string> _userRoleIds;    //用户角色GUID
        private readonly ILoginInfo _loginInfo;
        private readonly IMenuService _menuService;
        private readonly IMenuElementService _menuElementService;
        private readonly IRoleService _roleService;
        private readonly IOrganizeService _organizeService;
        private readonly IUserService _userService;
        private readonly IRelevanceService _relevanceService;
        public NormalAuthStrategy(ILoginInfo loginInfo,
            IMenuService menuService,
            IMenuElementService menuElementService,
            IRoleService roleService,
            IUserService userService,
            IOrganizeService organizeService,
            IRelevanceService relevanceService) {
            _loginInfo = loginInfo;
            _menuService = menuService;
            _menuElementService = menuElementService;
            _roleService = roleService;
            _organizeService = organizeService;
            _userService = userService;
            _relevanceService = relevanceService;
        }
        public List<Menu> Modules {
            get {
                var resourceIds = _relevanceService.FindAll(u => u.Key == Define.ROLEMODULE && _userRoleIds.Contains(u.FirstId)).Select(u => u.SecondId).ToList();
                
                var list = _menuService.FindAll("[Status]=0 and ID IN(" + resourceIds.ToSql(false) + ")");
                return list.ToList();
            }
        }
        public List<MenuElement> ModuleElements {
            get {
                var resourceIds = _relevanceService.FindAll(u => u.Key == Define.ROLEELEMENT && _userRoleIds.Contains(u.FirstId)).Select(u => u.SecondId).ToList();
               if(resourceIds!=null&&resourceIds.Count>0) {
                    var list = _menuElementService.FindAll("ID IN(" + resourceIds.ToSql(false) + ")");
                    return list.ToList();
                }
                return new List<MenuElement>();
            }
        }
        public List<Role> Roles {
            get {
                var list = _roleService.FindAll("ID IN(" + _userRoleIds.ToSql(false) + ")");
                return list.ToList();
            }
        }
        public List<Organize> Organize {
            get {
                var resourceIds = _relevanceService.FindAll(u => u.Key == Define.USERORG && _userRoleIds.Contains(u.FirstId)).Select(u => u.SecondId).ToList();
                var list = _organizeService.FindAll("ID IN(" + resourceIds.ToSql(false) + ")");
                return list.ToList();
            }
        }
        public User User {
            get {
                var model = _userService.Find(_loginInfo.AccountId);
                return model;
            }
            set {
                _user = value;
                _userRoleIds = _relevanceService.FindAll(u => u.Key == Define.USERROLE && u.FirstId == _user.ID).Select(u => u.SecondId).ToList();
            }
        }

        public List<KeyValue> GetProperties(string moduleCode) {
            throw new NotImplementedException();
        }
    }
}
