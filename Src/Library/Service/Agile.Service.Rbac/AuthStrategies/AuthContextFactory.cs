﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agile.Service.Rbac {
    /// <summary>
    ///  加载用户所有可访问的资源/机构/模块
    /// </summary>
    public class AuthContextFactory {
        private readonly SystemAuthStrategy _systemAuth;
        private readonly NormalAuthStrategy _normalAuthStrategy;
        private readonly IUserService _userService;
        public AuthContextFactory(IUserService userService,SystemAuthStrategy sysStrategy,NormalAuthStrategy normalAuthStrategy) {
            _userService = userService;
            _systemAuth = sysStrategy;
            _normalAuthStrategy = normalAuthStrategy;
        }
        public AuthStrategyContext GetAuthStrategyContext(string username) {
            if(string.IsNullOrEmpty(username)) return null;
            IAuthStrategy service = null;
            if(username.ToLower() == Define.SYSTEM_USERNAME) {
                service = _systemAuth;
            } else {
                service = _normalAuthStrategy;
                service.User = _userService.FindAll(u => u.Account == username).FirstOrDefault();
            }
            return new AuthStrategyContext(service);
        }
    }
}
