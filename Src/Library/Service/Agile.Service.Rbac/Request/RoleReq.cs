﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service.Rbac {
    public class RoleReq:Role {
        public string keyValue { get; set; }
        public string PermissionMenus { get; set; }
        public string PermissionMenuElements { get; set; }
        public string PermissionOrganizes { get; set; }
    }
}
