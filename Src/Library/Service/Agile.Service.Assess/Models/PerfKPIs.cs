﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess
{
    /// <summary>
    /// 考核指标
    /// </summary>
    [Table("perf_kpis")]
    [Description("考核指标")]
    public class PerfKPIs :Entity {
        /// <summary>
        /// 考核方案
        /// </summary>
        [Description("考核方案")]
        public string PerfProgramID { get; set; }
        /// <summary>
        /// 指标类型
        /// </summary>
        [Description("指标类型")]
        public string PerfKPITypeID { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Description("指标名称")]
        public string KpiName { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("指标备注")]
        public string Remarks { get; set; }
        /// <summary>
        /// 权重
        /// </summary>
        [Description("指标权重")]
        public double Weights { get; set; }
        /// <summary>
        /// 指标性质
        /// </summary>
        [Description("指标性质")]
        public string KpiProperty { get; set; }
        /// <summary>
        /// 评分方式
        /// </summary>
        [Description("评分方式")]
        public string PerfScoreModelID { get; set; }
        /// <summary>
        /// 工作目标
        /// </summary>
        [Description("工作目标")]
        public string Target { get; set; }
        /// <summary>
        /// 完成情况
        /// </summary>
        [Description("完成情况")]
        public string Completeness { get; set; }
    }
}
