﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess {
    /// <summary>
    /// 打分方式
    /// </summary>
    [Table("perf_scoremodel")]
    [Description("打分方式")]
    public class PerfScoreModel :Entity {
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string SmName { get; set; }
        /// <summary>
        /// 是否下拉选择
        /// </summary>
        [Description("是否下拉选择")]
        public int IsChoose { get; set; }
        /// <summary>
        /// 最大分
        /// </summary>
        [Description("最大分")]
        public double MaxScore { get; set; }
        /// <summary>
        /// 最低分
        /// </summary>
        [Description("最低分")]
        public double MinScore { get; set; }
        /// <summary>
        /// 下拉选择项设置
        /// </summary>
        [Description("下拉选择项设置")]
        public string ChooseSource { get; set; }

    }

}
