﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess {
    /// <summary>
    /// 考核打分
    /// </summary>
    [Table("perf_score")]
    [Description("考核打分")]
    public class PerfScore:Entity {
        /// <summary>
        /// 考核人
        /// </summary>
        [Description("考核人")]
        public string ExaminerUid { get; set; }
        /// <summary>
        /// 考核方案
        /// </summary>
        [Description("考核方案")]
        public string PerfProgramID { get; set; }
        /// <summary>
        /// 打分人
        /// </summary>
        [Description("打分人")]
        public string UserID { get; set; }
        /// <summary>
        /// 考核指标
        /// </summary>
        [Description("考核指标")]
        public string KPIUid { get; set; }
        /// <summary>
        /// 考核对象
        /// </summary>
        [Description("考核对象")]
        public string PerfObjectID { get; set; }
        /// <summary>
        /// 评分等级
        /// </summary>
        [Description("评分等级")]
        public string Rating { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        [Description("得分")]
        public double Score { get; set; }
        /// <summary>
        /// 评分说明
        /// </summary>
        [Description("评分说明")]
        public string ScoreNote { get; set; }
        /// <summary>
        /// 权重
        /// </summary>
        [Description("权重")]
        public double Weights { get; set; }
        /// <summary>
        /// 最终分数
        /// </summary>
        [Description("最终分数")]
        public double ScoreResult { get; set; }
        /// <summary>
        /// 完成情况
        /// </summary>
        [Description("完成情况")]
        public string Completeness { get; set; }

    }
}
