﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess{
    /// <summary>
    /// 考核方案分类
    /// </summary>
    [Table("perf_programcategory")]
    [Description("考核方案分类")]
    public class PerfProgramCategory:TreeEntity {
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string Remarks { get; set; }
    }
}
