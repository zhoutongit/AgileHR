﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess
{
    /// <summary>
    /// 考核指标类型
    /// </summary>
    [Table("perf_kpitype")]
    [Description("考核指标类型")]
    public class PerfKPIType :TreeEntity {
        /// <summary>
        /// 考核方案
        /// </summary>
        [Description("考核方案")]
        public string PerfProgramID { get; set; }
        /// <summary>
        /// 权重
        /// </summary>
        [Description("权重")]
        public double Weighting { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string Remarks { get; set; }
    }

}
