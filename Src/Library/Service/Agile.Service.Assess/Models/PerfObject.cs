﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess {
    /// <summary>
    /// 考核对象
    /// </summary>
    [Table("perf_object")]
    [Description("考核对象")]
    public class PerfObject:Entity {
        /// <summary>
        /// 对象编码
        /// </summary>
        [Description("对象编码")]
        public string ObjCode { get; set; }
        /// <summary>
        /// 对象名称
        /// </summary>
        [Description("对象名称")]
        public string ObjName { get; set; }
        /// <summary>
        /// 考核方案
        /// </summary>
        [Description("考核方案")]
        public string PerfProgramID { get; set; }
        /// <summary>
        /// 完成情况
        /// </summary>
        [Description("完成情况")]
        public string Completeness { get; set; }
        /// <summary>
        /// 最终得分
        /// </summary>
        [Description("最终得分")]
        public double Score { get; set; }

    }
}
