﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess {
    /// <summary>
    /// 考核方案
    /// </summary>
    [Table("perf_program")]
    [Description("考核方案")]
    public class PerfProgram:Entity {
        /// <summary>
        /// 考核方案名称
        /// </summary>
        [Description("考核方案名称")]
        public string PrmName { get; set; }
        /// <summary>
        /// 考核年月
        /// </summary>
        [Description("考核年月")]
        public string PrmYm { get; set; }
        /// <summary>
        /// 考核对象
        /// </summary>
        [Description("考核对象")]
        public string ObjectType { get; set; }
        /// <summary>
        /// 方案分类
        /// </summary>
        [Description("方案分类")]
        public string PerfProgramCategoryID { get; set; }
        /// <summary>
        /// 允许自评
        /// </summary>
        [Description("允许自评")]
        public int AllowSelfScore { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string Remarks { get; set; }
        /// <summary>
        /// 最高分
        /// </summary>
        public double MaxScore { get; set; }
        /// <summary>
        /// 最低分
        /// </summary>
        [Description("最低分")]
        public double MinScore { get; set; }
        /// <summary>
        /// 是否360度考核
        /// </summary>
        [Description("是否360度考核")]
        public int Is360 { get; set; }
        /// <summary>
        /// 自助查看
        /// </summary>
        [Description("自助查看")]
        public int EssViewAble { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Description("状态")]
        public int Status { get; set; }
    }
}
