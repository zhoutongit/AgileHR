﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Assess{

	/// <summary>
	/// 考核人
	/// </summary>
	[Table("perf_examiner")]
	[Description("考核人")]
	public class PerfExaminer :Entity
	{
		/// <summary>
		/// 考核对象
		/// </summary>
		[Description("考核对象")]
		public string PerfObjectID { get; set; }
		/// <summary>
		/// 考核方案
		/// </summary>
		[Description("考核方案")]
		public string PerfProgramID { get; set; }
		/// <summary>
		/// 考核方式
		/// </summary>
		[Description("考核方式")]
		public string AssessModel { get; set; }
		/// <summary>
		/// 考核人
		/// </summary>
		[Description("考核人")]
		public string UserID { get; set; }
		/// <summary>
		/// 权重
		/// </summary>
		[Description("权重")]
		public double Weights { get; set; }
		/// <summary>
		/// 分数
		/// </summary>
		[Description("分数")]
		public double Score { get; set; }
		/// <summary>
		/// 评价
		/// </summary>
		[Description("评价")]
		public string Reviews { get; set; }
	}
}
