﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System.Data;

namespace Agile.Service.Assess {
    ///<summary>
    ///考核打分
    ///</summary>
    public interface IPerfScoreService:IRepository<PerfScore> {

    }
    ///<summary>
    ///考核打分
    ///</summary>
    public partial class PerfScoreService:BaseRepository<PerfScore>, IPerfScoreService {

    }
}
