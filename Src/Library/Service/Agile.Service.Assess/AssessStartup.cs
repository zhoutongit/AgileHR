﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Assess {
    public class AssessStartup:IAgileStartup {
        public int Order => 7006;

        public void Configure(IApplicationBuilder app) {

        }

        public void ConfigureMvc(MvcOptions mvcOptions) {

        }

        public void ConfigureServices(IServiceCollection services,IConfiguration configuration) {
            //仓储模式注入

            //perf_examiner考核人
            services.AddSingleton<IPerfExaminerService,PerfExaminerService>();
            //perf_kpis考核指标
            services.AddSingleton<IPerfKPIsService,PerfKPIsService>();
            //perf_kpitype考核指标类型
            services.AddSingleton<IPerfKPITypeService,PerfKPITypeService>();
            //perf_object考核对象
            services.AddSingleton<IPerfObjectService,PerfObjectService>();
            //perf_program考核方案
            services.AddSingleton<IPerfProgramService,PerfProgramService>();
            //perf_programcategory考核方案分类
            services.AddSingleton<IPerfProgramCategoryService,PerfProgramCategoryService>();
            //perf_score考核打分
            services.AddSingleton<IPerfScoreService,PerfScoreService>();
            //perf_scoremodel打分方式
            services.AddSingleton<IPerfScoreModelService,PerfScoreModelService>();
        }
    }
}
