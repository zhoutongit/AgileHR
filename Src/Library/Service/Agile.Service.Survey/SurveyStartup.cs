using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Survey {
    public class SurveyStartup:IAgileStartup {
        public int Order => 7007;

        public void Configure(IApplicationBuilder app) {

        }

        public void ConfigureMvc(MvcOptions mvcOptions) {

        }

        public void ConfigureServices(IServiceCollection services,IConfiguration configuration) {
            //仓储模式注入
            //sur_arraytitle矩阵标题
            services.AddSingleton<ISurArrayTitleService,SurArrayTitleService>();
            //sur_filter收集设置
            services.AddSingleton<ISurFilterService,SurFilterService>();
            //sur_question问卷问题
            services.AddSingleton<ISurQuestionChoiceService,SurQuestionChoiceService>();
            //sur_questionchoice问题选项
            services.AddSingleton<ISurQuestionService,SurQuestionService>();
            //sur_reportfilter报表过滤条件
            services.AddSingleton<ISurReportFilterService,SurReportFilterService>();
            //sur_responselist用户问卷情况
            services.AddSingleton<ISurResponseListService,SurResponseListService>();
            //sur_result调查结果
            services.AddSingleton<ISurResultService,SurResultService>();
            //survey调查问卷
            services.AddSingleton<ISurQuestionNaireService,SurQuestionNaireService>();

        }
    }
}
