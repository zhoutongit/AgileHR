﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System.Data;

namespace Agile.Service.Survey {
    ///<summary>
    ///收集设置
    ///</summary>
    public interface ISurFilterService:IRepository<SurFilter> {

    }
    ///<summary>
    ///收集设置
    ///</summary>
    public partial class SurFilterService:BaseRepository<SurFilter>, ISurFilterService {

    }
}
