﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System.Data;

namespace Agile.Service.Survey {
    ///<summary>
    ///用户问卷情况
    ///</summary>
    public interface ISurResponseListService:IRepository<SurResponseList> {

    }
    ///<summary>
    ///用户问卷情况
    ///</summary>
    public partial class SurResponseListService:BaseRepository<SurResponseList>, ISurResponseListService {

    }
}
