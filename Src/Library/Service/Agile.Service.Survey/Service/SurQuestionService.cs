﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System.Data;

namespace Agile.Service.Survey {
    ///<summary>
    ///问卷问题
    ///</summary>
    public interface ISurQuestionService:IRepository<SurQuestion> {

    }
    ///<summary>
    ///问卷问题
    ///</summary>
    public partial class SurQuestionService:BaseRepository<SurQuestion>, ISurQuestionService {

    }
}
