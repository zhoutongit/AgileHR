﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurArrayTitle]对应的实体类
    /// 内容描述：矩阵标题
    /// </summary>
    [Description("矩阵标题")]
    [Table("sur_arraytitle")]
    public partial class SurArrayTitle:Entity {
        [Description("问题")]
        public string QuestionUid { get; set; }
        [Description("问题ID")]
        public int QuestionID { get; set; }
        [Description("内容")]
        public string Content { get; set; }
        [Description("问卷")]
        public string SurveyUid { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
      
    }
}











