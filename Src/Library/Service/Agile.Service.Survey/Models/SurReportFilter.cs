﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurReportFilter]对应的实体类
    /// 内容描述：报表过滤条件
    /// </summary>
    [Description("报表过滤条件")]
    [Table("sur_reportfilter")]
    public partial class SurReportFilter:Entity {
        [Description("问卷")]
        public string SurveyUid { get; set; }
        [Description("问题")]
        public string QuestionUid { get; set; }
        [Description("选项")]
        public string ChoiceUid { get; set; }
        [Description("矩阵标题")]
        public string TitleUid { get; set; }
        [Description("条件")]
        public string ConditionId { get; set; }
        [Description("类型")]
        public int TypeId { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











