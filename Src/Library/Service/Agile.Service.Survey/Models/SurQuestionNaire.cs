﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[Survey]对应的实体类
    /// 内容描述：调查问卷
    /// </summary>
    [Description("调查问卷")]
    [Table("sur_questionnaire")]
    public partial class SurQuestionNaire:Entity {
        [Description("问卷名称")]
        public string SurName { get; set; }
        [Description("描述")]
        public string SurContent { get; set; }
        [Description("共享")]
        public int IsShare { get; set; }
        [Description("发布方式")]
        public string FilterModel { get; set; }
        [Description("创建时间")]
        public string CreateTime { get; set; }
        [Description("完成情况")]
        public string Completed { get; set; }
        [Description("状态")]
        public string SurStatus { get; set; }
        [Description("收集量")]
        public int CollectionAmount { get; set; }
        [Description("已收集数量")]
        public int Amounted { get; set; }
        [Description("目标用户")]
        public string TargetUser { get; set; }
        [Description("质量控制")]
        public int QualityControl { get; set; }
       
        [Description("调研结果")]
        public string SurResult { get; set; }
        [Description("内容")]
        public string JSONContent { get; set; }
        [Description("预览JSON")]
        public string JSONPreview { get; set; }
        [Description("发布JSON")]
        public string JSONPublish { get; set; }
        [Description("发布时间")]
        public string PublishTime { get; set; }
        [Description("收集开始时间")]
        public string SurStartDate { get; set; }
        [Description("收集结束时间")]
        public string SurEndDate { get; set; }
        [Description("未锁定")]
        public int IsUnlocked { get; set; }
        [Description("在线")]
        public int IsOnline { get; set; }
        [Description("投票类型")]
        public int VoteType { get; set; }
     
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    
    }
}











