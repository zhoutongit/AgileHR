﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurQuestionChoice]对应的实体类
    /// 内容描述：问题选项
    /// </summary>
    [Description("问题选项")]
    [Table("sur_questionchoice")]
    public partial class SurQuestionChoice:Entity {
        [Description("关联问题")]
        public string QuestionUid { get; set; }
        [Description("排序")]
        public int SortIndex { get; set; }
        [Description("选项绝对编号")]
        public int ChoiceAbsoluteId { get; set; }
        [Description("标题")]
        public string Content { get; set; }
        [Description("是否含文本框")]
        public int IsOther { get; set; }
        [Description("必填")]
        public int Required { get; set; }
        [Description("问题ID")]
        public int QuestionID { get; set; }
        [Description("问卷")]
        public string SurveyUid { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











