﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurResponseList]对应的实体类
    /// 内容描述：用户问卷情况
    /// </summary>
    [Description("用户问卷情况")]
    [Table("sur_responselist")]
    public partial class SurResponseList:Entity {
        [Description("问卷")]
        public string SurveyUid { get; set; }
        [Description("用户")]
        public string UserUid { get; set; }
        [Description("员工")]
        public string EmpUid { get; set; }
        [Description("答题时长")]
        public decimal TimeLength { get; set; }
        [Description("答题时长")]
        public string TimeLenDesc { get; set; }
        [Description("提交时间")]
        public string SubmitTime { get; set; }
        [Description("开始答卷时间")]
        public string StartTime { get; set; }
        [Description("排序")]
        public int SortIndex { get; set; }
        [Description("状态")]
        public string ResponseStatus { get; set; }
        [Description("IP地址")]
        public string IPAddress { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











