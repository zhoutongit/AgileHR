﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurFilter]对应的实体类
    /// 内容描述：收集设置
    /// </summary>
    [Description("收集设置")]
    [Table("sur_filter")]
    public partial class SurFilter:Entity {
        [Description("问卷")]
        public string SurveyUid { get; set; }
        [Description("收集目标条件")]
        public string FilterCondition { get; set; }
        [Description("总收集量")]
        public int Amounted { get; set; }
        [Description("开始收集时间")]
        public string SurStartDate { get; set; }
        [Description("结束收集时间")]
        public string SurEndDate { get; set; }
        [Description("发布时间")]
        public string PublishTime { get; set; }
        [Description("收集方式")]
        public string FilterModel { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











