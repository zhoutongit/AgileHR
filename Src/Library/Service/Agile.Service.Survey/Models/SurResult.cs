﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurResult]对应的实体类
    /// 内容描述：调查结果
    /// </summary>
    [Description("调查结果")]
    [Table("sur_result")]
    public partial class SurResult:Entity {
        [Description("调查问卷")]
        public string SurveyUid { get; set; }
        [Description("问题")]
        public string QuestionUid { get; set; }
        [Description("矩阵标题")]
        public string TitleUid { get; set; }
        [Description("答案")]
        public string Answer { get; set; }
        [Description("其它答案")]
        public string AnswerOther { get; set; }
        [Description("填写时间")]
        public string FillDate { get; set; }
        [Description("用户")]
        public string UserUid { get; set; }
        [Description("员工")]
        public string EmpUid { get; set; }
        [Description("多选答案")]
        public string Answers { get; set; }
        [Description("问卷人")]
        public string ResponseUid { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











