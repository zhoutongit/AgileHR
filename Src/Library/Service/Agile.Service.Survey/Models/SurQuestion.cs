﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Survey {
    /// <summary>
    /// 内容摘要：表[SurQuestion]对应的实体类
    /// 内容描述：问卷问题
    /// </summary>
    [Description("问卷问题")]
    [Table("sur_question")]
    public partial class SurQuestion:Entity {
        [Description("关联调查")]
        public string SurveyUid { get; set; }
        [Description("标题")]
        public string Content { get; set; }
        [Description("问题类型")]
        public string TypeId { get; set; }
        [Description("排序")]
        public int SortIndex { get; set; }
        [Description("其它")]
        public int HasOther { get; set; }
        [Description("必填")]
        public int Required { get; set; }
        [Description("页码")]
        public int PageNum { get; set; }
        [Description("索引")]
        public int QIndex { get; set; }
        [Description("绝对编号")]
        public int AbsoluteId { get; set; }
        [Description("末尾绝对编号")]
        public int LastAbsoluteId { get; set; }
        [Description("逻辑隐藏")]
        public int LogicHide { get; set; }
        [Description("互斥选项")]
        public string ExclusiveOptions { get; set; }
        [Description("标题引用")]
        public int TitleQuote { get; set; }
        [Description("最大值")]
        public string MaxValue { get; set; }
        [Description("最小值")]
        public string MinValue { get; set; }
        [Description("JSON内容")]
        public string JSONContent { get; set; }
        [Description("跳转关系")]
        public string RedirectRelation { get; set; }
        [Description("选择项引用")]
        public string ChoiceQuote { get; set; }
        [Description("有效开始时间")]
        public string EnableDate { get; set; }
        [Description("有效截止时间")]
        public string DisableDate { get; set; }
    }
}











