﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace Agile.Service.School
{
    ///<summary>
    ///教室信息
    ///</summary>
    public interface ISchoolClassRoomService : IRepository<ClassRoom>
    {

    }
    ///<summary>
    ///教室信息
    ///</summary>
    public partial class SchoolClassRoomService : BaseRepository<ClassRoom>, ISchoolClassRoomService
    {

    }
}