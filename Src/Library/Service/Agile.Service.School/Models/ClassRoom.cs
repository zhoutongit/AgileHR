﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{

    /// <summary>
    /// 教室信息
    /// </summary>
    [Table("school_ClassRoom")]
    [Description("教室信息")]
    public partial class ClassRoom : Entity
    {
        /// <summary>
        /// 所属校区
        /// </summary>
        [Description("校区")]
        public virtual string SchoolAreaId { get; set; }
        /// <summary>
        /// 所属教学楼
        /// </summary>
        [Description("教学楼")]
        public virtual string TeachingBuildingId { get; set; }
        /// <summary>
        /// 所属教学功能
        /// </summary>
        [Description("功能")]
        public virtual string FunctionAreaId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public virtual string Name { get; set; }
        /// <summary>
        /// 座位数
        /// </summary>
        [Description("座位数")]
        public virtual int Seat { get; set; }
        /// <summary>
        /// 上课座位数
        /// </summary>
        [Description("上课座位数")]
        public virtual int GotoClassSeat { get; set; }
        /// <summary>
        /// 考试座位数
        /// </summary>
        [Description("考试座位数")]
        public virtual int TestSeat { get; set; }
        
    }


}
