﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{
    /// <summary>
    /// 专业信息
    /// </summary>
    [Table("school_major")]
    [Description("专业信息")]
    public partial class Major : TreeEntity
    {
        /// <summary>
        /// 专业简称
        /// </summary>
        [Description("专业简称")]
        public virtual string NameShort{ get; set; }
    }
}
