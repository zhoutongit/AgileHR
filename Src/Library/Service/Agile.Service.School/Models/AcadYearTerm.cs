﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{
    /// <summary>
    /// 学年学期
    /// </summary>
    [Table("school_acadyearterm")]
    [Description("学年学期")]
    public partial class AcadYearTerm : Entity
    {
        /// <summary>
        /// 学年学期号
        /// </summary>
        [Description("学年学期号")]
        public virtual string Name { get; set; }
        /// <summary>
        /// 学期号简称
        /// </summary>
        [Description("学期号简称")]
        public virtual string ShortName { get; set; }
        /// <summary>
        /// 起始上课周
        /// </summary>
        [Description("起始上课周")]
        public virtual int StartWeek { get; set; }
        /// <summary>
        /// 截止上课周
        /// </summary>
        [Description("截止上课周")]
        public virtual int EndWeek { get; set; }
        /// <summary>
        /// 当年学期标志
        /// </summary>
        [Description("当年学期标志")]
        public virtual bool Mark { get; set; }

    }


}
