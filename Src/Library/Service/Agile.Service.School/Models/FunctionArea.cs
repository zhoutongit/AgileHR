﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{
    /// <summary>
    /// 教学功能区
    /// </summary>
    [Table("school_functionarea")]
    [Description("教学功能区")]
    public partial class FunctionArea : Entity
    {
        /// <summary>
        /// 所属校区
        /// </summary>
        [Description("所属校区")]
        public virtual string SchoolAreaId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public virtual string Name { get; set; }
    }
   
    
}
