﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{
    /// <summary>
    /// 班级信息
    /// </summary>
    [Table("school_classes")]
    [Description("班级信息")]
    public partial class Classes : Entity
    {
        /// <summary>
        /// 所属校区
        /// </summary>
        [Description("所属校区")]
        public virtual string SchoolAreaId { get; set; }
        /// <summary>
        /// 所属院系
        /// </summary>
        [Description("所属院系")]
        public virtual string Colleges { get; set; }
        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级名称")]
        public virtual string Name { get; set; }
        /// <summary>
        /// 入学年份
        /// </summary>
        [Description("入学年份")]
        public virtual string EnrollmentYear { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        [Description("专业")]
        public virtual string Major { get; set; }
        /// <summary>
        /// 班级人数
        /// </summary>
        [Description("班级人数")]
        public virtual int NumberPeople { get; set; }
        /// <summary>
        /// 当前班级人数
        /// </summary>
        [Description("当前班级人数")]
        public virtual int CurrNumberPeople { get; set; }
        /// <summary>
        /// 当前班级女生人数
        /// </summary>
        [Description("当前班级女生人数")]
        public virtual int CurrNumberGirlPeople { get; set; }
        /// <summary>
        /// 当前班级男生人数
        /// </summary>
        [Description("当前班级男生人数")]
        public virtual int CurrNumberBoyPeople { get; set; } 
        /// <summary>
        /// 固定教学楼
        /// </summary>
        [Description("固定教学楼")]
        public virtual string TeachingBuilding { get; set; }
        /// <summary>
        /// 固定教室
        /// </summary>
        [Description("固定教室")]
        public virtual string ClassRoom { get; set; }

    }
}
