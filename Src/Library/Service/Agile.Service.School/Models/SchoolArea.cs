﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.School
{
    /// <summary>
    /// 校区信息
    /// </summary>
    [Table("school_schoolarea")]
    [Description("校区信息")]
    public partial class SchoolArea : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public virtual string Name { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [Description("地址")]
        public virtual string Address { get; set; }
    }
}
