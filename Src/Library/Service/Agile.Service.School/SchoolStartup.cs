using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.School
{
    public class SchoolStartup : IAgileStartup
    {
        public int Order => 7004;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //仓储模式注入
         
            //school_acadyearterm学年学期
            services.AddSingleton<ISchoolacadyeartermService, SchoolacadyeartermService>();
            //school_classes班级信息
            services.AddSingleton<ISchoolclassesService, SchoolclassesService>();
            //school_ClassRoom教学功能区
            services.AddSingleton<ISchoolClassRoomService, SchoolClassRoomService>();
            //school_functionarea教学功能区
            services.AddSingleton<ISchoolfunctionareaService, SchoolfunctionareaService>();
            //school_major专业信息
            services.AddSingleton<ISchoolmajorService, SchoolmajorService>();
            //school_schoolarea校区信息
            services.AddSingleton<ISchoolschoolareaService, SchoolschoolareaService>();
            //school_teachingbuilding教学楼
            services.AddSingleton<ISchoolteachingbuildingService, SchoolteachingbuildingService>();

        }
    }
}
