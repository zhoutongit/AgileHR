﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Agile.Service.Flow
{

    ///<summary>
    ///工作流模板信息表
    ///</summary>
    public interface IFlowSchemeService : IRepository<FlowScheme>
    {
        List<FlowScheme> GetAll(QueryFlowSchemeListReq request);
        FlowScheme FindByCode(string code);
    }
    ///<summary>
    ///工作流模板信息表
    ///</summary>
    public partial class FlowSchemeService : BaseRepository<FlowScheme>, IFlowSchemeService
    {
        public List<FlowScheme> GetAll(QueryFlowSchemeListReq request)
        {
            return FindAll("", "AddTime", request.page, request.rows).ToList();
        }
        public FlowScheme FindByCode(string code)
        {
            return FindAll("SchemeCode='" + code + "'").FirstOrDefault();
        }
    }
}

