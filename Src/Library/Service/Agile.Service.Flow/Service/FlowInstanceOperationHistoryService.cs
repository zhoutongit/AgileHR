﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service.Flow
{
    ///<summary>
    ///工作流实例操作记录
    ///</summary>
    public interface IFlowInstanceOperationHistoryService : IRepository<FlowInstanceOperationHistory>
    {

    }
    ///<summary>
    ///工作流实例操作记录
    ///</summary>
    public partial class FlowInstanceOperationHistoryService : BaseRepository<FlowInstanceOperationHistory>, IFlowInstanceOperationHistoryService
    {

    }

}

