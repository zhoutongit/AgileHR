﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Linq;
//using System.Text;

//namespace Agile.Service.Flow
//{
//    ///<summary>
//    ///表单模板表
//    ///</summary>
//    public interface IFormService : IRepository<Form>
//    {
//        FormResp FindSingle(string id);
//        List<Form> GetForms(QueryFormListReq request);
//    }
//    ///<summary>
//    ///表单模板表
//    ///</summary>
//    public partial class FormService : BaseRepository<Form>, IFormService
//    {
//        public FormResp FindSingle(string id)
//        {
//            var form = Find(id);
//            return form.MapTo<FormResp>();
//        }
//        public List<Form> GetForms(QueryFormListReq request)
//        {
//            string whereStr = "";
//            if (!string.IsNullOrEmpty(request.keyword))
//            {
//                whereStr = "Name='" + request.keyword + "'";
//            }
//            var forms = GetByPage(whereStr, "", request.page, request.rows);
//            return forms;
//        }
//        public override string Add(Form obj)
//        {
//            if (!string.IsNullOrEmpty(obj.DbName))
//            {
//                DBA.ExecuteNonQuery(FormUtil.GetSql(obj));
//            }
//            return base.Add(obj);
//        }
//        public override bool Update(Form entity)
//        {
//            if (!string.IsNullOrEmpty(entity.DbName))
//            {
//                DBA.ExecuteNonQuery(FormUtil.GetSql(entity));
//            }
//            return base.Update(entity);
//        }
//    }
//}