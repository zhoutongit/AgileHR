﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Agile.Service.Flow
{
    ///<summary>
    ///表单模型字段
    ///</summary>
    public interface IDivModelfieldService : IRepository<DivModelfield>
    {

    }
    ///<summary>
    ///表单模型字段
    ///</summary>
    public partial class DivModelfieldService : BaseRepository<DivModelfield>, IDivModelfieldService
    {
       
    }
}