﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Agile.Service.Rbac;

namespace Agile.Service.Flow
{
    ///<summary>
    ///表单模型管理
    ///</summary>
    public interface IDivModelService : IRepository<DivModel>
    {
        void DeleteAll(string divmodelId);
    }
    ///<summary>
    ///表单模型管理
    ///</summary>
    public partial class DivModelService : BaseRepository<DivModel>, IDivModelService
    {
        private readonly IMenuService _menuService;
        private readonly IDivModelfieldService _divModelfieldService;
        public DivModelService(IDivModelfieldService divModelfieldService, IMenuService menuService)
        {
            _divModelfieldService = divModelfieldService;
            _menuService = menuService;
        }
        public void DeleteAll(string divmodelId)
        {
            var model = this.Find(divmodelId);
            if (model != null)
            {
                this.Delete(divmodelId);
                _divModelfieldService.DeleteWhere($"DivModelID='{divmodelId}'");
                _menuService.DeleteAll(model.ModuleId);
            }
        }
    }
}