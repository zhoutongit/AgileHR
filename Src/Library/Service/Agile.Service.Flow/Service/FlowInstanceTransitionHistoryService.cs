﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace Agile.Service.Flow
{
    ///<summary>
    ///工作流实例流转历史记录
    ///</summary>
    public interface IFlowInstanceTransitionHistoryService : IRepository<FlowInstanceTransitionHistory>
    {

    }
    ///<summary>
    ///工作流实例流转历史记录
    ///</summary>
    public partial class FlowInstanceTransitionHistoryService : BaseRepository<FlowInstanceTransitionHistory>, IFlowInstanceTransitionHistoryService
    {

    }
}

