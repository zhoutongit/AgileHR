﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agile.Service.Flow
{
	/// <summary>
	/// 模型实例
	/// </summary>
	[Table("flow_divmodel_Instances")]
	[Description("模型字段")]
	public partial class DivModelInstance : Entity
    {
		/// <summary>
		/// 模型字段编号
		/// </summary>	
		[Description("模型字段编号")]
		public string DivModelfieldID { get; set; }
		/// <summary>
		/// 模型字段值
		/// </summary>	
		[Description("模型字段值")]
		public string DivModelfieldValue { get; set; }
	}
}
