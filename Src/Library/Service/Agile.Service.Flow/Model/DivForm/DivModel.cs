﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Flow
{
	/// <summary>
	/// 模型管理
	/// </summary>
	[Table("flow_divmodel")]
	[Description("模型管理")]
	public partial class DivModel : TreeEntity
	{
		/// <summary>
		/// 模板(菜单ID)
		/// </summary>
		public string ModuleId { get; set; }
		/// <summary>
		/// 描述
		/// </summary>	
		[Description("描述")]
		public string Description { get; set; }
		/// <summary>
		/// 允许游客提交
		/// </summary>	
		[Description("允许游客提交")]
		public bool AllowVisitor { get; set; }
		/// <summary>
		/// 文本
		/// </summary>	
		[Description("文本")]
		public string Setting { get; set; }
	}
}

