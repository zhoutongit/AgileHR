﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Flow
{
	/// <summary>
	/// 模型字段
	/// </summary>
	[Table("flow_divmodel_field")]
	[Description("模型字段")]
	public partial class DivModelfield : Entity
	{
		/// <summary>
		/// 模型编号
		/// </summary>	
		[Description("模型编号")]
		public string DivModelID { get; set; }
		/// <summary>
		/// 中文名称
		/// </summary>	
		[Description("中文名称")]
		public string Name { get; set; }
		/// <summary>
		/// 字段英文
		/// </summary>	
		[Description("字段英文")]
		public string Field { get; set; }
		/// <summary>
		/// 字段类型
		/// </summary>	
		[Description("字段类型")]
		public string FieldType { get; set; }
		/// <summary>
		/// 默认值
		/// </summary>	
		[Description("默认值")]
		public string DefaultValue { get; set; }
		/// <summary>
		/// 是否系统
		/// </summary>	
		[Description("是否系统")]
		public int IsSystem { get; set; }
		/// <summary>
		/// 状态
		/// </summary>	
		[Description("状态")]
		public int Status { get; set; }
	}
}

