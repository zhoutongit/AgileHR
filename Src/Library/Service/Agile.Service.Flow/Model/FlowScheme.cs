﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace Agile.Service.Flow
{
    /// <summary>
    /// 工作流模板信息表
    /// </summary>
    [Table("flow_flowScheme")]
    [Description("工作流模板信息表")]
    public partial class FlowScheme : Entity, ISoftDelete, ICreationAudited, IModificationAudited
    {

        /// <summary>
        /// 流程编号
        /// </summary>
        [Description("流程编号")]
        public string SchemeCode { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        [Description("流程名称")]
        public string SchemeName { get; set; }
        /// <summary>
        /// 流程分类
        /// </summary>
        [Description("流程分类")]
        public string SchemeType { get; set; }
        /// <summary>
        /// 流程内容版本
        /// </summary>
        [Description("流程内容版本")]
        public string SchemeVersion { get; set; }
        /// <summary>
        /// 流程模板使用者
        /// </summary>
        [Description("流程模板使用者")]
        public string SchemeCanUser { get; set; }
        /// <summary>
        /// 流程内容
        /// </summary>
        [Description("流程内容")]
        [Max]
        public string SchemeContent { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        [Description("表单ID")]
        public string FrmId { get; set; }
        /// <summary>
        /// 表单类型
        /// </summary>
        [Description("表单类型")]
        public int FrmType { get; set; }
        /// <summary>
        /// 模板权限类型：0完全公开,1指定部门/人员
        /// </summary>
        [Description("模板权限类型：0完全公开,1指定部门/人员")]
        public int AuthorizeType { get; set; }
        /// <summary>
        /// 是否启用（0.未启用，1.启用，2.废弃） 	
        /// </summary>
        [Description("有效")]
        public int Status { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string Description { get; set; }
        /// <summary>
        /// 所属租户
        /// </summary>
        [Description("所属租户")]
        public virtual string TenantId { get; set; }
        /// <summary>
        /// 软删除
        /// </summary>
        [Description("软删除")]
        public virtual int IsDelete { get; set; }
        /// <summary>
        /// 软删除用户
        /// </summary>
        [Description("软删除用户")]
        public string DeleteUserID { get; set; }
        /// <summary>
        /// 软删除时间
        /// </summary>
        [Description("软删除时间")]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        [Description("添加用户")]
        public string AddUserID { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Description("修改用户")]
        public string EditUserID { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        public DateTime? EditTime { get; set; }
    }
}