﻿//using System;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations.Schema;
//namespace Agile.Service.Flow
//{
//    /// <summary>
//    /// 表单模板表
//    /// </summary>
//    [Table("flow_form")]
//    [Description("表单模板表")]
//    public partial class Form : Entity, ITenant, ISoftDelete
//    {
//        /// <summary>
//        /// 所属租户
//        /// </summary>
//        [Description("所属租户")]
//        public virtual string TenantId { get; set; }
//        /// <summary>
//        /// 软删除
//        /// </summary>
//        [Description("软删除")]
//        public virtual int IsDelete { get; set; }
//        /// <summary>
//        /// 软删除用户
//        /// </summary>
//        [Description("软删除用户")]
//        public string DeleteUserID { get; set; }
//        /// <summary>
//        /// 软删除时间
//        /// </summary>
//        [Description("软删除时间")]
//        public DateTime? DeleteTime { get; set; }
//        /// <summary>
//        /// 表单名称
//        /// </summary>
//        [Description("表单名称")]
//        public string Name { get; set; }
//        /// <summary>
//        /// 表单类型，0：默认动态表单；1：Web自定义表单
//        /// </summary>
//        [Description("表单类型")]
//        public int FrmType { get; set; }
//        /// <summary>
//        /// 系统页面标识，当表单类型为用Web自定义的表单时，需要标识加载哪个页面
//        /// </summary>
//        [Description("系统页面标识")]
//        public string WebId { get; set; }
//        /// <summary>
//        /// 字段个数
//        /// </summary>
//        [Description("字段个数")]
//        public int Fields { get; set; }
//        /// <summary>
//        /// 表单中的控件属性描述
//        /// </summary>
//        [Description("表单中的控件属性描述")]
//        public string ContentData { get; set; }
//        /// <summary>
//        /// 表单控件位置模板
//        /// </summary>
//        [Description("表单控件位置模板")]
//        public string ContentParse { get; set; }
//        /// <summary>
//        /// 表单原html模板未经处理的
//        /// </summary>
//        [Description("表单原html模板未经处理的")]
//        public string Content { get; set; }
//        /// <summary>
//        /// 数据库名称
//        /// </summary>
//        [Description("数据库名称")]
//        public string DbName { get; set; }
//        /// <summary>
//        /// 有效
//        /// </summary>
//        [Description("有效")]
//        public int Disabled { get; set; }
//        /// <summary>
//        /// 备注
//        /// </summary>
//        [Description("备注")]
//        public string Description { get; set; }
//    }
//}