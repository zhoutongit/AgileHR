﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service.Flow
{
    /// <summary>
    /// 工作流实例操作记录
    /// </summary>
    [Table("flow_flowInstanceOperationHistory")]
    [Description("工作流实例操作记录")]
    public partial class FlowInstanceOperationHistory : Entity
    {
        /// <summary>
        /// 多租户
        /// </summary>
        [Description("所属租户")]
        public virtual string TenantId { get; set; }
        /// <summary>
        /// 实例进程Id
        /// </summary>
        [Description("实例进程Id")]
        public string InstanceId { get; set; }
        /// <summary>
        /// 操作内容
        /// </summary>
        [Description("操作内容")]
        public string Content { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Description("创建用户主键")]
        public string CreateUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Description("创建用户")]
        public string CreateUserName { get; set; }

    }
}