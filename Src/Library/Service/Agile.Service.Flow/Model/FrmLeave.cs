﻿//using System;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations.Schema;
//namespace Agile.Service.Flow
//{
//    /// <summary>
//    /// 模拟一个自定页面的表单，该数据会关联到流程实例FrmData，可用于复杂页面的设计及后期的数据分析
//    /// </summary>
//    [Table("flow_frmLeave")]
//    [Description("自定义页面表单")]
//    public partial class FrmLeave : Entity, ITenant
//    {
//        /// <summary>
//        /// 所属租户
//        /// </summary>
//        [Description("所属租户")]
//        public virtual string TenantId { get; set; }
//        /// <summary>
//        /// 请假人姓名
//        /// </summary>
//        [Description("请假人姓名")]
//        public string UserName { get; set; }
//        /// <summary>
//        /// 请假分类，病假，事假，公休等
//        /// </summary>
//        [Description("请假分类")]
//        public string RequestType { get; set; }
//        /// <summary>
//        /// 开始日期
//        /// </summary>
//        [Description("开始日期")]
//        public System.DateTime StartDate { get; set; }
//        /// <summary>
//        /// 开始时间
//        /// </summary>
//        [Description("开始时间")]
//        public System.DateTime? StartTime { get; set; }
//        /// <summary>
//        /// 结束日期
//        /// </summary>
//        [Description("结束日期")]
//        public System.DateTime EndDate { get; set; }
//        /// <summary>
//        /// 结束时间
//        /// </summary>
//        [Description("结束时间")]
//        public System.DateTime? EndTime { get; set; }
//        /// <summary>
//        /// 请假说明
//        /// </summary>
//        [Description("请假说明")]
//        public string RequestComment { get; set; }
//        /// <summary>
//        /// 附件，用于提交病假证据等
//        /// </summary>
//        [Description("附件")]
//        public string Attachment { get; set; }
//        /// <summary> 
//        /// 所属流程实例ID
//        /// </summary>
//        [Description("所属流程实例ID")]
//        public string FlowInstanceId { get; set; }

//    }
//}