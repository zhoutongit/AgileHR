using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Flow
{
    public class FlowStartup : IAgileStartup
    {
        public int Order => 7001;

        public void Configure(IApplicationBuilder app)
        {

        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {

        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //仓储模式注入
            ////Name:Form Remark:表单模板表
            //services.AddSingleton<IFormService, FormService>();
            ////Name:FrmLeave Remark:自定义页面表单
            //services.AddSingleton<IFrmLeaveService, FrmLeaveService>();
            //Name:FlowInstance Remark:工作流流程实例表
            services.AddSingleton<IFlowInstanceService, FlowInstanceService>();
            //Name:FlowInstanceOperationHistory Remark:工作流实例操作记录
            services.AddSingleton<IFlowInstanceOperationHistoryService, FlowInstanceOperationHistoryService>();
            //Name:FlowInstanceTransitionHistory Remark:工作流实例流转历史记录
            services.AddSingleton<IFlowInstanceTransitionHistoryService, FlowInstanceTransitionHistoryService>();
            //Name:FlowScheme Remark:工作流模板信息表
            services.AddSingleton<IFlowSchemeService, FlowSchemeService>();

            services.AddSingleton<IDivModelService, DivModelService>();
            services.AddSingleton<IDivModelfieldService, DivModelfieldService>();

        }
    }
}
