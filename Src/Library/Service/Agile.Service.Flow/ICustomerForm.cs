﻿namespace Agile.Service.Flow
{
    public interface ICustomerForm
    {
        void Add(string flowInstanceId, string frmData);
    }
}
