﻿using System;

namespace Agile.Service.Flow
{
    public class QueryFormListReq : Paging
    {
        public string orgId { get; set; }
    }
}
