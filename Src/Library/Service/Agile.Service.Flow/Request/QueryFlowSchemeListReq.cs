﻿using System;

namespace Agile.Service.Flow
{
    public class QueryFlowSchemeListReq : Paging
    {
        public string orgId { get; set; }
    }
}
