﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Agile.Service.Reward
{
    /// <summary>
    /// 工资公式类型
    /// </summary>
    public enum SalaryType
    {
        /// <summary>
        /// 公式调用
        /// </summary>
        [Description("公式调用")]
        Formula = 1,
        /// <summary>
        /// 直接输入
        /// </summary>
        [Description("直接输入")]
        Input = 2,
        /// <summary>
        /// 固定工资
        /// </summary>
        [Description("固定工资")]
        Fixed = 3,
        /// <summary>
        /// 其它
        /// </summary>
        [Description("其它")]
        Other = 0
    }
}
