﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service.Reward
{
    /// <summary>
    /// 工资表
    /// https://www.cnblogs.com/wghao/archive/2009/05/16/1458104.html
    /// </summary>
    public class RewardStartup : IAgileStartup
    {
        public int Order => 7005;

        public void Configure(IApplicationBuilder application)
        {
            
        }

        public void ConfigureMvc(MvcOptions mvcOptions)
        {
            
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //salary_salaryitem工资项
            services.AddSingleton<ISalaryColumnService, SalaryColumnService>();
            //salary_salaryitemtype工资表
            services.AddSingleton<ISalaryService, SalaryService>();
        }
    }
}
