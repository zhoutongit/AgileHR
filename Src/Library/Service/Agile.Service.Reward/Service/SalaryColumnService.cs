﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace Agile.Service.Reward
{
    ///<summary>
    ///工资公式
    ///</summary>
    public interface ISalaryColumnService : IRepository<SalaryColumn>
    {

    }
    ///<summary>
    ///工资公式
    ///</summary>
    public partial class SalaryColumnService : BaseRepository<SalaryColumn>, ISalaryColumnService
    {

    }
}