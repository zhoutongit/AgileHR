﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Reward
{
	/// <summary>
	/// 工资公式
	/// </summary>
	[Table("reward_salarycolumn")]
	[Description("工资公式")]
	public partial class SalaryColumn : TreeEntity
	{
		/// <summary>
		/// 运算公式
		/// </summary>	
		[Description("公式")]
		public string OperateExpression { get; set; }
		/// <summary>
		/// 备注
		/// </summary>	
		[Description("备注")]
		public string Remark { get; set; }
		/// <summary>
		/// 工资列类型  1工资册，0工资列
		/// </summary>
		[Description("工资列类型")]
		public int IfType { get; set; }
        /// <summary>
        /// 1公式调用，2直接输入，3固定工资，4其它导入
        /// </summary>	
        [Description("公式类型")]
		public SalaryType SalaryType { get; set; }
	}
}

