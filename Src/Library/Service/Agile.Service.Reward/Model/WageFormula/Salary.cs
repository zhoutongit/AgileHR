﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service.Reward
{
	/// <summary>
	/// 工资表
	/// </summary>
	[Table("reward_salary")]
	[Description("工资表")]
	public partial class Salary : Entity
    {
		/// <summary>
		/// 用户编号
		/// </summary>	
		[Description("用户编号")]
		public string UserID { get; set; }
		/// <summary>
		/// 工资项编号
		/// </summary>	
		[Description("工资项编号")]
		public string SalaryColumnID { get; set; }
		/// <summary>
		/// 工资月
		/// </summary>	
		[Description("工资月")]
		public DateTime SalaryMonth { get; set; }
		/// <summary>
		/// 金额
		/// </summary>	
		[Description("金额")]
		public double Amount { get; set; }
	}
}
