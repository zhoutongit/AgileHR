﻿namespace Agile.Service
{
    /// <summary>
    /// 部门分配用户
    /// </summary>
    public class AssignOrgUsers
    {
        /// <summary>
        /// 部门id
        /// </summary>
        public string OrgId { get; set; }
        /// <summary>
        /// 用户id列表
        /// </summary>
        public string[] UserIds { get; set; }
        /// <summary>
        /// 为部门分配用户，需要统一提交，会删除以前该部门的所有用户
        /// </summary>
        public bool IsDelete { get; set; }
    }
}