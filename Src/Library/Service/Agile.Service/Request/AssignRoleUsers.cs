﻿namespace Agile.Service
{
    /// <summary>
    /// 角色分配用户
    /// </summary>
    public class AssignRoleUsers
    {
        /// <summary>
        /// 角色id
        /// </summary>
        public string RoleId { get; set; }
        /// <summary>
        /// 用户id列表
        /// </summary>
        public string[] UserIds { get; set; }
        /// <summary>
        /// 为角色分配用户，需要统一提交，会删除以前该角色的所有用户
        /// </summary>
        public bool IsDelete { get; set; }
    }
}