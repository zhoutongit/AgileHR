﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service {
    public class ApplicationReq:Application {
        public string keyValue { get; set; }
        public string PermissionMenus { get; set; }
        public string PermissionMenuElements { get; set; }
        public string PermissionOrganizes { get; set; }
    }
}
