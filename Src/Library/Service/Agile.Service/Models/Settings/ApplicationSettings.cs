﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service {
    public partial class ApplicationSettings:ISettings {
        /// <summary>
        /// 关闭站点
        /// </summary>
        public bool SiteClosed { get; set; }
        /// <summary>
        /// 默认皮肤
        /// </summary>
        public string DefaultStoreTheme { get; set; } = "Default";
        /// <summary>
        /// 创始人账号
        /// </summary>
        public string Admin { get; set; } = "admin";
        /// <summary>
        /// 创始人密码
        /// </summary>
        public string AdminPwd { get; set; } = "admin";
        /// <summary>
        /// 客服QQ1
        /// </summary>
        public string CustomerServiceQQ1 { get; set; } = "1114800402";
        /// <summary>
        /// 客服QQ2
        /// </summary>
        public string CustomerServiceQQ2 { get; set; } = "3025513464";
        /// <summary>
        /// 统计代码
        /// </summary>
        public string Statistics { get; set; }
        /// <summary>
        /// 当前系统
        /// </summary>
        public string CurrentSystem { get; set; }

        #region CompanySettings
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; } = "Agile";
        /// <summary>
        /// 公司地址
        /// </summary>
        public string CompanyAddress { get; set; } = "中国";
        /// <summary>
        /// 公司电话
        /// </summary>
        public string CompanyTelephone { get; set; } = "13320163492";
        /// <summary>
        /// 系统名称
        /// </summary>
        public string SystemFullName { get; set; } = "AgileHR";
        /// <summary>
        /// 系统缩写
        /// </summary>
        public string SystemAbbreviation { get; set; } = "敏捷人事";
        #endregion

        #region SeoSettings
        public string DefaultTitle { get; set; } = "人力资源管理系统";
        public string DefaultMetaKeywords { get; set; } = "人力资源管理系统";
        public string DefaultMetaDescription { get; set; } = "人力资源管理系统";

        #endregion

        #region SmtpSettings
        /// <summary>
        /// 邮件服务器地址
        /// </summary>
        public string MailServer { get; set; } = "smtp.163.com";
        /// <summary>
        /// 用户名
        /// </summary>
        public string MailUserName { get; set; } = "minguiluo@163.com";
        /// <summary>
        /// 密码
        /// </summary>
        public string MailPassword { get; set; } = "TPSDOUOTAVMKSESD";
        /// <summary>
        /// 名称
        /// </summary>
        public string MailName { get; set; } = "minguiluo@163.com";
        #endregion
    }
}
