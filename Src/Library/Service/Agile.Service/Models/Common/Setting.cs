using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// Represents a setting
    /// </summary>
    [Table("base_setting")]
    [Description("ϵͳ����")]
    public partial class Setting : Entity
    {
        public Setting()
        {

        }
        public Setting(string name, string value, int storeId = 0)
        {
            Name = name;
            Value = value;
            StoreId = storeId;
        }
        public string Name { get; set; }
        public string Value { get; set; }
        public int StoreId { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }

    public interface ISettings
    {

    }
}