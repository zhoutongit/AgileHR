﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
	/// <summary>
	/// 评论
	/// </summary>
	[Table("base_comment")]
	[Description("评论")]
	public partial class Comment : TreeEntity
	{
		/// <summary>
		/// 发布人
		/// </summary>	
		[Description("发布人")]
		public string UserID { get; set; }
		/// <summary>
		/// 表ID
		/// </summary>	
		[Description("表ID")]
		public string TableID { get; set; }
		/// <summary>
		/// 表名
		/// </summary>	
		[Description("表名")]
		public string TableName { get; set; }
		/// <summary>
		/// 评论内容
		/// </summary>	
		[Description("评论内容")]
		public string Message { get; set; }
		/// <summary>
		/// 评论IP
		/// </summary>	
		[Description("评论IP")]
		public string IP { get; set; }
		/// <summary>
		/// 评论状态
		/// </summary>	
		[Description("评论状态")]
		public string Status { get; set; }
	}
}

