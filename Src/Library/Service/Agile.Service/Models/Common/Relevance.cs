﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 多对多关系集中映射
    /// </summary>
    [Table("base_relevance")]
    [Description("多对多关系集中映射")]
    public partial class Relevance : Entity
    {
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }
        /// <summary>
	    /// 映射标识
	    /// </summary>
         [Description("映射标识")]
        public string Key { get; set; }
        /// <summary>
	    /// 状态
	    /// </summary>
         [Description("状态")]
        public int Status { get; set; }
        /// <summary>
	    /// 第一个表主键ID
	    /// </summary>
         [Description("第一个表主键ID")]
        public string FirstId { get; set; }
        /// <summary>
	    /// 第二个表主键ID
	    /// </summary>
         [Description("第二个表主键ID")]
        public string SecondId { get; set; }
        /// <summary>
	    /// 第三个主键
	    /// </summary>
         [Description("第三个主键")]
        public string ThirdId { get; set; }
        /// <summary>
	    /// 扩展信息
	    /// </summary>
         [Description("扩展信息")]
        public string ExtendInfo { get; set; }

    }
}