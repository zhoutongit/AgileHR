﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
	/// 友情链接表
	/// </summary>
    [Table("base_friendlink")]
    [Description("应用")]
    public partial class Friendlink : Entity
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string Name { get; set; }
        /// <summary>
        /// url
        /// </summary>
        [Description("Url")]
        public string Url { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Description("图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 是否可用
        /// </summary>
        [Description("是否可用")]
        public string Type { get; set; }

    }
}