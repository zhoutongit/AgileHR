﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 收藏表
    /// </summary>
    [Table("base_favorite")]
    [Description("收藏表")]
    public class Favorite : Entity
    {
        /// <summary>
        /// 会员ID
        /// </summary>
        [Description("会员ID")]
        public virtual string UserID { get; set; }
        /// <summary>
        /// 表ID
        /// </summary>	
        [Description("表ID")]
        public string TableID { get; set; }
        /// <summary>
        /// 表名
        /// </summary>	
        [Description("表名")]
        public string TableName { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Description("标题")]
        public virtual string Title { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }

    }
}
