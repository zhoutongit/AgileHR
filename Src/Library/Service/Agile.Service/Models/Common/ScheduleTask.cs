﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service {
    /// <summary>
    /// 计划任务
    /// </summary>
    [Table("base_scheduletask")]
    [Description("计划任务")]
    public partial class ScheduleTask : Entity {
        /// <summary>
        /// 任务名称
        /// </summary>
        [Description("任务名称")]
        public string Name { get; set; }

        /// <summary>
        /// 获取或设置运行周期（秒）
        /// </summary>
        [Description("运行周期")]
        public int Seconds { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [Description("任务类型")]
        public string Type { get; set; }

        /// <summary>
        /// 启用
        /// </summary>
        [Description("启用")]
        public bool Enabled { get; set; }

        /// <summary>
        /// 获取或设置一个值，该值指示是否应在出现错误时停止任务
        /// </summary>
        [Description("停止任务")]
        public bool StopOnError { get; set; }

        /// <summary>
        /// 上次启动日期
        /// </summary>
        [Description("上次启动日期")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? LastStartUtc { get; set; }

        /// <summary>
        /// 上次完成日期
        /// </summary>
        [Description("上次完成日期")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? LastEndUtc { get; set; }

        /// <summary>
        /// 上次成功完成日期
        /// </summary>
        [Description("上次成功完成日期")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? LastSuccessUtc { get; set; }
    }
}
