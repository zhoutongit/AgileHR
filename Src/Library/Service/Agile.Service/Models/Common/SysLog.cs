﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 系统日志
    /// </summary>
    [Table("base_sysLog")]
    [Description("系统日志")]
    public partial class SysLog : Entity
    {
        /// <summary>
        /// 发布人
        /// </summary>	
        [Description("发布人")]
        public string UserID { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [Description("类型")]
        public virtual string Type { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        [Description("IP地址")]
        public virtual string IPAddress { get; set; }
        /// <summary>
        /// IP所在城市
        /// </summary>
        [Description("IP所在城市")]
        public virtual string IPAddressName { get; set; }
        /// <summary>
        /// 系统模块Id
        /// </summary>
        [Description("系统模块Id")]
        public virtual string ModuleId { get; set; }
        /// <summary>
        /// 结果
        /// </summary>
        [Description("结果")]
        public virtual string Result { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public virtual string Description { get; set; }
    }
}
