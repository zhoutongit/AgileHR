﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 选项明细表
    /// </summary>
    [Table("base_itemsDetail")]
    [Description("选项明细表")]
    public partial class ItemsDetail : TreeEntity
    {
        /// <summary>
        /// 主表主键
        /// </summary>
        [Description("主表主键")]
        public string ItemId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        public string EnCode { get; set; }
        /// <summary>
        /// 简拼
        /// </summary>
        [Description("简拼")]
        public string SimpleSpelling { get; set; }
        /// <summary>
        /// 默认
        /// </summary>
        [Description("默认")]
        public bool IsDefault { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }
        /// <summary>
        /// 有效
        /// </summary>
        [Description("有效")]
        public int Status { get; set; }
    }
}
