﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 选项主表
    /// </summary>
    [Table("base_items")]
    [Description("选项主表")]
    public partial class Items : TreeEntity
    {
        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        public string EnCode { get; set; }
        /// <summary>
        /// 树型
        /// </summary>
        [Description("树型")]
        public bool IsTree { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }
        /// <summary>
        /// 有效
        /// </summary>
        [Description("有效")]
        public int Status { get; set; }
    }
}
