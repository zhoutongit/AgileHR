﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 禁止IP访问表
    /// </summary>
    [Table("base_bannedIp")]
    [Description("禁止IP访问表")]
    public partial class BannedIp : Entity
    {
        /// <summary>
        /// IP分段1
        /// </summary>
        [Description("IP分段1")]
        public string Ip1 { get; set; }
        /// <summary>
        /// IP分段2
        /// </summary>
        [Description("IP分段2")]
        public string Ip2 { get; set; }
        /// <summary>
        /// IP分段3
        /// </summary>
        [Description("IP分段3")]
        public string Ip3 { get; set; }
        /// <summary>
        /// IP分段4
        /// </summary>
        [Description("IP分段4")]
        public string Ip4 { get; set; }
    }
}
