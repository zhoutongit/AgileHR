﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service {

    /// <summary>
    /// 通知公告
    /// </summary>
    [Table("base_noticemod")]
    [Description("通知公告")]
    public class Noticemod:Entity, ICreationAudited, IModificationAudited {
        [Description("公告标题")]
        public string Title { get; set; }
        [Description("公告内容")]
        public string Content { get; set; }
        [Description("公告类型")]
        public string CurryType { get; set; }
        [Description("状态")]
        public int Status { get; set; }
        [Description("添加用户")]
        public string AddUserID { get; set; }
        [Description("修改用户")]
        public string EditUserID { get; set; }
        [Description("修改时间")]
        public DateTime? EditTime { get; set; }



    }
}
