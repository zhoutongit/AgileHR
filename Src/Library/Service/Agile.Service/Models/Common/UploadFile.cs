﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 文件
    /// </summary>
    [Table("base_uploadFile")]
    [Description("文件")]
    public partial class UploadFile : Entity,ICreationAudited
    {
        /// <summary>
        /// 表ID
        /// </summary>	
        [Description("表ID")]
        public string TableID { get; set; }
        /// <summary>
        /// 表名
        /// </summary>	
        [Description("表名")]
        public string TableName { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        [Description("文件名称")]
        public string FileName { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        [Description("文件路径")]
        public string FilePath { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }
        /// <summary>
	    /// 文件类型
	    /// </summary>
        [Description("文件类型")]
        public string FileType { get; set; }
        /// <summary>
	    /// 文件大小
	    /// </summary>
        [Description("文件大小")]
        public long FileSize { get; set; }
        /// <summary>
	    /// 扩展名称
	    /// </summary>
        [Description("扩展名称")]
        public string Extension { get; set; }
        /// <summary>
        /// 是否可用
        /// </summary>
        [Description("是否可用")]
        public bool Enable { get; set; }
        /// <summary>
        /// 缩略图
        /// </summary>
        [Description("缩略图")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 添加用户
        /// </summary>
        [Description("添加用户")]
        public string AddUserID { get; set; }
    }
}