﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
namespace Agile.Service {
    /// <summary>
    /// 查询方案配置
    /// </summary>
    [Table("base_queryprogram")]
    [Description("查询方案配置")]
    public partial class QueryProgram:Entity {
        /// <summary>
        /// 方案名称
        /// </summary>
        public string ProgramName { get; set; }
        /// <summary>
        /// 表名称
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 查询条件
        /// </summary>
        public string QueryCondition { get; set; }
        /// <summary>
        /// 所有人
        /// </summary>
        public string Owner { get; set; }
        /// <summary>
        /// 是否全局
        /// </summary>
        public int IsGlobal { get; set; }
        /// <summary>
        /// 可使用人
        /// </summary>
        public string UseEmployee { get; set; }
    }
}
