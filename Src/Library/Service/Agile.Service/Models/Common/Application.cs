﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 应用
    /// </summary>
    [Table("base_application")]
    [Description("应用")]
    public partial class Application : Entity
    {
        /// <summary>
        /// 应用名称
        /// </summary>
        [Description("应用名称")]
        public string Name { get; set; }
        /// <summary>
        /// 应用密钥
        /// </summary>
        [Description("应用密钥")]
        public string AppSecret { get; set; }
        /// <summary>
        /// 应用描述
        /// </summary>
        [Description("应用描述")]
        public string Description { get; set; }
        /// <summary>
        /// 应用图标
        /// </summary>
        [Description("应用图标")]
        public string Icon { get; set; }
        /// <summary>
        /// 是否可用
        /// </summary>
        [Description("是否可用")]
        public bool Disable { get; set; }

    }
}