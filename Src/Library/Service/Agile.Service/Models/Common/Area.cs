﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agile.Service
{
    /// <summary>
    /// 区域
    /// </summary>
    [Table("base_area")]
    [Description("区域")]
    public class Area : TreeEntity
    {
        /// <summary>
        /// 编码
        /// </summary>
        [Description("编码")]
        public string EnCode { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string FullName { get; set; }
        /// <summary>
        /// 简拼
        /// </summary>
        [Description("简拼")]
        public string SimpleSpelling { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [Description("描述")]
        public string Description { get; set; }

    }
}
