using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agile.Service {
    public class BaseStartup:IAgileStartup {
        public int Order => 7000;

        public void Configure(IApplicationBuilder app) {

        }

        public void ConfigureMvc(MvcOptions mvcOptions) {

        }

        public void ConfigureServices(IServiceCollection services,IConfiguration configuration) {
            //register all settings
            var typeFinder = new WebAppTypeFinder();
            var settings = typeFinder.FindClassesOfType(typeof(ISettings),false).ToList();
            foreach(var setting in settings) {
                services.AddScoped(setting,serviceProvider => {
                    var storeId = 0;
                    return serviceProvider.GetRequiredService<ISettingService>().LoadSetting(setting,storeId);
                });
            }

            services.AddSingleton<IRelevanceService,RelevanceService>();
            services.AddSingleton<ISettingService,SettingService>();

            services.AddSingleton<IItemsDetailService,ItemsDetailService>();
            services.AddSingleton<IItemsService,ItemsService>();

            services.AddSingleton<IApplicationService,ApplicationService>();
            services.AddSingleton<IAreaService,AreaService>();
            services.AddSingleton<ICommentService,CommentService>();
            services.AddSingleton<IFavoriteService,FavoriteService>();
            services.AddSingleton<IFriendlinkService,FriendlinkService>();
            services.AddSingleton<ISysLogService,SysLogService>();
            services.AddSingleton<IUploadFileService,UploadFileService>();
            services.AddSingleton<IScheduleTaskService,ScheduleTaskService>();
            services.AddSingleton<INoticemodService,NoticemodService>();





        }
    }
}
