﻿namespace Agile.Service
{
    public static partial class Define
    {
        /// <summary>
        /// 用户角色关联KEY
        /// </summary>
        public const string USERROLE = "UserRole";
        /// <summary>
        /// 用户机构关联KEY
        /// </summary>
        public const string USERORG = "UserOrg";
        /// <summary>
        /// 角色资源关联KEY
        /// </summary>
        public const string ROLERESOURCE = "RoleResource";
        /// <summary>
        /// 角色菜单关联KEY
        /// </summary>
        public const string ROLEELEMENT = "RoleElement";
        /// <summary>
        /// 角色模块关联KEY
        /// </summary>
        public const string ROLEMODULE = "RoleModule";
        /// <summary>
        /// 角色组织关联KEY
        /// </summary>
        public const string ROLEORG = "RoleOrg";
        /// <summary>
        /// 角色数据字段权限
        /// </summary>
        public const string ROLEDATAPROPERTY = "RoleDataProperty";
        /// <summary>
        /// 应用菜单
        /// </summary>

        public const string APPLICATIONMODULE = "ApplicationModule";
        /// <summary>
        /// 应用菜单按钮
        /// </summary>

        public const string APPLICATIONELEMENT = "ApplicationElement";
        /// <summary>
        /// 应用组织
        /// </summary>
        public const string APPLICATIONORG = "ApplicationOrg";
        public const string DBTYPE_SQLSERVER = "SqlServer";    //sql server
        public const string SYSTEM_USERNAME = "admin";
        public const string SYSTEM_USERPWD = "123456";
    }
}
