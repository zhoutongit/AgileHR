﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service
{
    ///<summary>
    ///评论
    ///</summary>
    public interface ICommentService : IRepository<Comment>
    {

    }
    ///<summary>
    ///评论
    ///</summary>
    public partial class CommentService : BaseRepository<Comment>, ICommentService
    {

    }
}
