﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service
{
    ///<summary>
    ///友情链接表
    ///</summary>
    public interface IFriendlinkService : IRepository<Friendlink>
    {

    }
    ///<summary>
    ///友情链接表
    ///</summary>
    public partial class FriendlinkService : BaseRepository<Friendlink>, IFriendlinkService
    {

    }
}
