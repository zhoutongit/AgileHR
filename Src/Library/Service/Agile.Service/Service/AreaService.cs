﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service
{
    ///<summary>
    ///区域
    ///</summary>
    public interface IAreaService : IRepository<Area>
    {

    }
    ///<summary>
    ///区域
    ///</summary>
    public partial class AreaService : BaseRepository<Area>, IAreaService
    {

    }
}