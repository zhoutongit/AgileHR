﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service
{
    ///<summary>
    ///系统日志
    ///</summary>
    public interface ISysLogService : IRepository<SysLog>
    {
        void ClearLog();
    }
    ///<summary>
    ///系统日志
    ///</summary>
    public partial class SysLogService : BaseRepository<SysLog>, ISysLogService {
        public void ClearLog() {
            this.DBA.ExecuteNonQuery("delete base_sysLog");
           
        }
    }
}
