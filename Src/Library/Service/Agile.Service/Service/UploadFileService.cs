﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

using DapperExtensions;

using Microsoft.AspNetCore.Http;

namespace Agile.Service
{
    ///<summary>
    ///文件
    ///</summary>
    public interface IUploadFileService : IRepository<UploadFile>
    {
        List<UploadFile> BatchAddFile(IFormFileCollection files);
        UploadFile AddFile(IFormFile file);
    }
    ///<summary>
    ///文件
    ///</summary>
    public partial class UploadFileService : BaseRepository<UploadFile>, IUploadFileService
    {
        private string _filePath;    //后面不带/
        private string _dbFilePath;   //数据库中的文件路径
        private string _dbThumbnail;   //数据库中的缩略图路径
        public UploadFileService()
        {
            _filePath = AppContext.BaseDirectory;
        }
        public List<UploadFile> BatchAddFile(IFormFileCollection files)
        {
            var result = new List<UploadFile>();
            foreach (var file in files)
            {
                result.Add(AddFile(file));
            }

            return result;
        }
        public UploadFile AddFile(IFormFile file)
        {
            if (file != null)
            {
                Logger.LogInfo("收到新文件: " + file.FileName);
                Logger.LogInfo("收到新文件: " + file.Length);
            }
            else
            {
                Logger.LogWarn("收到新文件为空");
            }
            if (file != null && file.Length > 0 && file.Length < 10485760)
            {
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var data = binaryReader.ReadBytes((int)file.Length);
                    UploadFile(fileName, data);

                    var filedb = new UploadFile
                    {
                        FilePath = _dbFilePath,
                        Thumbnail = _dbThumbnail,
                        FileName = fileName,
                        FileSize = file.Length,
                        FileType = Path.GetExtension(fileName),
                        Extension = Path.GetExtension(fileName)
                    };
                    Add(filedb);
                    return filedb;
                }
            }
            else
            {
                throw new Exception("文件过大");
            }
        }
        private void UploadFile(string fileName, byte[] fileBuffers)
        {
            string folder = DateTime.Now.ToString("yyyyMMdd");
            //判断文件是否为空
            if (string.IsNullOrEmpty(fileName))
            {
                throw new Exception("文件名不能为空");
            }
            //判断文件是否为空
            if (fileBuffers.Length < 1)
            {
                throw new Exception("文件不能为空");
            }
            var uploadPath = _filePath + "/" + folder + "/";
            Logger.LogInfo("文件写入：" + uploadPath);
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }
            var ext = Path.GetExtension(fileName).ToLower();
            string newName = GenerateId.GenerateOrderNumber() + ext;

            using (var fs = new FileStream(uploadPath + newName, FileMode.Create))
            {
                fs.Write(fileBuffers, 0, fileBuffers.Length);
                fs.Close();
                //生成缩略图
                if (ext.Contains(".jpg") || ext.Contains(".jpeg") || ext.Contains(".png") || ext.Contains(".bmp") || ext.Contains(".gif"))
                {
                    string thumbnailName = GenerateId.GenerateOrderNumber() + ext;
                    ImgHelper.MakeThumbnail(uploadPath + newName, uploadPath + thumbnailName);
                    _dbThumbnail = folder + "/" + thumbnailName;
                }
                _dbFilePath = folder + "/" + newName;
            }
        }
    }
}
