﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service
{
    ///<summary>
    ///选项主表
    ///</summary>
    public interface IItemsService : IRepository<Items>
    {

    }
    ///<summary>
    ///选项主表
    ///</summary>
    public partial class ItemsService : BaseRepository<Items>, IItemsService
    {

    }
}