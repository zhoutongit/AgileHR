﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Agile.Service {
    public partial class ClearLogTask : IScheduleTask {
        #region Fields

        private readonly ISysLogService _logger;

        #endregion

        #region Ctor

        public ClearLogTask(ISysLogService logger) {
            _logger = logger;
        }

        #endregion

        #region Methods
        public virtual void Execute() {
            _logger.ClearLog();
        }

        #endregion
    }
}
