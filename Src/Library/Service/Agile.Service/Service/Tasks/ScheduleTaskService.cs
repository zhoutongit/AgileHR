﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service {
    ///<summary>
    ///计划任务
    ///</summary>
    public interface IScheduleTaskService : IRepository<ScheduleTask> {
        ScheduleTask GetTaskByType(string type);
        IList<ScheduleTask> GetAllTasks(bool showHidden = false);
    }
    ///<summary>
    ///计划任务
    ///</summary>
    public partial class ScheduleTaskService : BaseRepository<ScheduleTask>, IScheduleTaskService {
        public IList<ScheduleTask> GetAllTasks(bool showHidden = false) {
            if (showHidden) {
                this.FindAll("[Enabled]=1");
            }
            return this.FindAll().ToList();
        }

        public ScheduleTask GetTaskByType(string type) {
            return this.FindAll("[Type]='"+ type + "'").FirstOrDefault();
        }
    }
}
