﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agile.Service {
    /// <summary>
    /// 每个任务应该实现的接口
    /// </summary>
    public partial interface IScheduleTask {
        /// <summary>
        ///执行任务
        /// </summary>
        void Execute();
    }
}
