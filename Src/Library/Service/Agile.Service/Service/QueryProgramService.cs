﻿/*******************************************************
Author: 罗敏贵
Explain：
Versions: V 1.0 版
E-mail: minguiluo@163.com
Blogs： http://www.cnblogs.com/luomingui
History:
      CreateDate 2020-08-09 19:22:18
    
*******************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service {
    ///<summary>
    ///查询方案配置
    ///</summary>
    public interface IQueryProgramService:IRepository<QueryProgram> {

    }
    ///<summary>
    ///查询方案配置
    ///</summary>
    public partial class QueryProgramService:BaseRepository<QueryProgram>, IQueryProgramService {

    }
}
