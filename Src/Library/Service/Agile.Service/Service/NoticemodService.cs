﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service {
    ///<summary>
    ///通知公告
    ///</summary>
    public interface INoticemodService:IRepository<Noticemod> {

    }
    ///<summary>
    ///通知公告
    ///</summary>
    public partial class NoticemodService:BaseRepository<Noticemod>, INoticemodService {

    }
}
