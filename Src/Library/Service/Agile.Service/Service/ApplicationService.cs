﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DapperExtensions;

namespace Agile.Service
{
    ///<summary>
    ///应用
    ///</summary>
    public interface IApplicationService : IRepository<Application>
    {
        Result SaveApplication(ApplicationReq applicationReq);
        List<TreeViewModel> GetMenuElementTree(string appId);
        List<TreeViewModel> GetBranTree(string appId);
    }
    ///<summary>
    ///应用
    ///</summary>
    public partial class ApplicationService:BaseRepository<Application>, IApplicationService {
        private readonly IRelevanceService _relevanceService;
        public ApplicationService(IRelevanceService relevanceService) {
            _relevanceService = relevanceService;
        }
        public Result SaveApplication(ApplicationReq applicationReq) {
            Result result = new Result(true);
            string[] permissionIdArr;
            //菜单
            if(!string.IsNullOrWhiteSpace(applicationReq.PermissionMenus)) {
                permissionIdArr = applicationReq.PermissionMenus.Split(',');
                if(permissionIdArr.Length > 0) {
                    List<string> moduleids = new List<string>();
                    List<string> buttionids = new List<string>();
                    _relevanceService.UnAssign(new AssignReq { type = Define.APPLICATIONMODULE,firstId = applicationReq.ID });
                    _relevanceService.UnAssign(new AssignReq { type = Define.APPLICATIONELEMENT,firstId = applicationReq.ID });
                    foreach(var itemId in permissionIdArr) {
                        if(itemId == "undefined") continue;
                        var vitemId = itemId;
                        int hasChildren = vitemId.Split('#').Length > 1 ? 5 : 1;
                        if(hasChildren == 5) {
                            //按钮权限
                            buttionids.Add(itemId.Replace("#",""));
                        } else {
                            //菜单权限
                            moduleids.Add(itemId);
                        }
                    }
                    _relevanceService.Assign(new AssignReq { type = Define.APPLICATIONMODULE,firstId = applicationReq.ID,secIds = moduleids.ToArray() });
                    _relevanceService.Assign(new AssignReq { type = Define.APPLICATIONELEMENT,firstId = applicationReq.ID,secIds = buttionids.ToArray() });
                }
            }
            //组织
            if(!string.IsNullOrWhiteSpace(applicationReq.PermissionOrganizes)) {
                permissionIdArr = applicationReq.PermissionMenus.Split(',');
                if(permissionIdArr.Length > 0) {
                    _relevanceService.UnAssign(new AssignReq { type = Define.APPLICATIONORG,firstId = applicationReq.ID });
                    List<string> powerids = new List<string>();
                    foreach(var itemId in permissionIdArr) {
                        if(itemId == "undefined") continue;
                        powerids.Add(itemId);
                    }
                    _relevanceService.Assign(new AssignReq { type = Define.APPLICATIONORG,firstId = applicationReq.ID,secIds = powerids.ToArray() });
                }
            }
            //数据
            this.AddOrUpdate(applicationReq);
            return result;
        }
        public List<TreeViewModel> GetMenuElementTree(string appId) {
            var table = DBA.ExecuteDataTable(@"SELECT ID,ParentID,Name FROM(
SELECT ID,ParentID,Name,OrderBy FROM [rbac_menu] WHERE [Status]=0 
union all
SELECT ID,[ModuleId] AS ParentID,Name,OrderBy FROM [rbac_menuelement] WHERE 
ModuleId IN(SELECT ID FROM [rbac_menu] WHERE [Status]=0)
)d ORDER BY OrderBy");
            var treeList = new List<TreeViewModel>();
            foreach(DataRow row in table.Rows) {
                string SecondId = DBA.ExecuteScalar<string>("SELECT SecondId FROM [dbo].[base_relevance] WHERE [Key] IN('"+ Define.APPLICATIONMODULE + "','"+ Define.APPLICATIONELEMENT + "') AND FirstId='" + appId + "' AND SecondId='" + row["ID"].ToString() + "'");
                TreeViewModel tree = new TreeViewModel();
                bool hasChildren = table.Select("ParentID='" + row["ID"].ToString() + "'").Length == 0 ? false : true;
                tree.id = row["ID"].ToString();
                tree.text = row["Name"].ToString();
                tree.value = row["ID"].ToString();
                tree.parentId = row["ParentID"].ToString();
                tree.isexpand = false;
                tree.complete = true;
                tree.checkstate = !string.IsNullOrWhiteSpace(SecondId) ? 1 : 0;
                tree.hasChildren = hasChildren;
                treeList.Add(tree);
            }
            return treeList;
        }
        public List<TreeViewModel> GetBranTree(string appId) {
            var table = DBA.ExecuteDataTable(@"SELECT ID,ParentID,Name FROM [dbo].[rbac_organize] WHERE [Status]=0 ORDER BY OrderBy");
            var treeList = new List<TreeViewModel>();
            foreach(DataRow row in table.Rows) {
                string SecondId = DBA.ExecuteScalar<string>("SELECT SecondId FROM [dbo].[base_relevance] WHERE [Key] IN('"+ Define.APPLICATIONORG + "') AND FirstId='" + appId + "'  AND SecondId='" + row["ID"].ToString() + "'");
                TreeViewModel tree = new TreeViewModel();
                bool hasChildren = table.Select("ParentID='" + row["ID"].ToString() + "'").Length == 0 ? false : true;
                tree.id = row["ID"].ToString();
                tree.text = row["Name"].ToString();
                tree.value = row["ID"].ToString();
                tree.parentId = row["ParentID"].ToString();
                tree.isexpand = false;
                tree.complete = true;
                tree.checkstate = !string.IsNullOrWhiteSpace(SecondId) ? 1 : 0;
                tree.hasChildren = hasChildren;
                treeList.Add(tree);
            }
            return treeList;
        }
    }
}

