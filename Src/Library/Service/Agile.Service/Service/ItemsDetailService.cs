﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Dapper;

using DapperExtensions;

namespace Agile.Service
{
    ///<summary>
    ///选项明细表
    ///</summary>
    public interface IItemsDetailService : IRepository<ItemsDetail>
    {
        List<ItemsDetail> GetItemList(string enCode);
    }
    ///<summary>
    ///选项明细表
    ///</summary>
    public partial class ItemsDetailService : BaseRepository<ItemsDetail>, IItemsDetailService
    {
        public List<ItemsDetail> GetItemList(string enCode)
        {
            var sql = "SELECT d.* FROM base_itemsDetail d INNER JOIN base_items i ON i.ID = d.ItemId WHERE 1 = 1 AND d.[Status] = 0 ";
            var dynamicParams = new DynamicParameters();
            if (!string.IsNullOrWhiteSpace(enCode))
            {
                sql += "AND i.EnCode = @enCode ";
                dynamicParams.Add("enCode", enCode);
            }
            sql += "  ORDER BY d.OrderBy DESC ";
            return DBA.Query<ItemsDetail>(sql, dynamicParams).ToList();

        }
    }
}