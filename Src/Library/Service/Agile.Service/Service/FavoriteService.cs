﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Agile.Service
{
    ///<summary>
    ///收藏表
    ///</summary>
    public interface IFavoriteService : IRepository<Favorite>
    {

    }
    ///<summary>
    ///收藏表
    ///</summary>
    public partial class FavoriteService : BaseRepository<Favorite>, IFavoriteService
    {

    }
}
