@for /r . %%I in (bin) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (obj) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (debug) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (.vs) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (clientbin) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (TestResults) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (_packages) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (logs) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (App_Data) do if exist "%%I" rd/s/q "%%I"
@for /r . %%I in (Agile.WebHost/Modules/) do if exist "%%I" rd/s/q "%%I"

del /s/q/f *.user
del /s/q/f *.vspscc
rd /s/q .vs
